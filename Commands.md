Documentation: https://packaging.python.org/tutorials/packaging-projects/


build: python setup.py sdist bdist_wheel


upload to test: python -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

publish: python -m twine upload  dist/*

install : python -m pip install --index-url https://test.pypi.org/simple/ --no-deps numpyprint-Command1991

