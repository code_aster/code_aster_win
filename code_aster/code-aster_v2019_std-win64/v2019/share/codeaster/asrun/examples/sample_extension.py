# -*- coding: utf-8 -*-

# ==============================================================================
# COPYRIGHT (C) 1991 - 2015  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ==============================================================================

"""
Example of an extension.

Usage :
Choose a directory and copy this file as my_action.py and create
in the same directory a file named as_run.extensions containing :

[My extension title]
module=my_action.py

"""

from asrun.i18n      import _

def SetParser(run):
   """Configure the command-line parser, add options name to store to the list,
   set actions informations.
   run : AsterRun object which manages the execution
   """
   acts_descr = {
      'my_action' : {
         'method' : Main,
         'syntax' : _('my_action [args]'),
         'help'   : '?'
      },
   }
   #opts_descr = {}
   title = _('Options for ...')
   run.SetActions(
         actions_descr=acts_descr,
         actions_order=['my_action'],
         group_options=False, group_title=title, actions_group_title=False,
         #options_descr=opts_descr,
   )
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def Main(run, *args):
   """Doc string
   """
#    if len(args) != 1:
#       run.parser.error(_(u"'--%s' takes exactly %d arguments (%d given)") % \
#          (run.current_action, 1, len(args)))

   print(_('hello world'))
