# -*- coding: utf-8 -*-

# ==============================================================================
# COPYRIGHT (C) 1991 - 2015  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ==============================================================================

"""
   Example of a user filter.
   usage :
      as_run --list --all --user_filter=...../user_filter.py
   
   Filter(s) is(are) given through 'user_filter' variable (single value or list).
"""

class FILTER_FONC(FILTER):
   """This is an example of a user filter.
   It should be a subclass of FILTER.
   """
   def check(self, **kwargs):
      """This method must return a boolean : True to keep the testcase,
      False else.
      Arguments :
         test : full pathname with an empty extension
                (for example "/opt/aster/NEW9/astest/zzzz100a.")
         para : directory of the parameter.
         jdc  : content of all ".com?" files of the testcase.
      """
      content = kwargs.get('jdc', '')
      keep = content.find('MACR_SPECTRE') > -1
      return keep

# user_filter can be a list of FILTER objects
user_filter = FILTER_FONC()
