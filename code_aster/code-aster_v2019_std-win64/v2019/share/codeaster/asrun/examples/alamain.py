# -*- coding: utf-8 -*-
# pylint: disable-msg=W0611

# ==============================================================================
# COPYRIGHT (C) 1991 - 2015  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ==============================================================================

"""This file is not used by astk server.
It's an example of using an AsterRun object as a toolbox.
(see `as_....` modules to check syntax)
"""


import os
import sys
from asrun.run          import AsRunFactory
from asrun.installation import aster_root


if __name__ == '__main__':

   VERSION    = 'unstable'
   REPREF     = os.path.join(aster_root, VERSION)
   run = AsRunFactory()

   #-------------------------------------------------------------------------------
   # Methods to build Code_Aster
   from asrun.build     import AsterBuild
   from asrun.config    import build_config_of_version

   # help(AsterBuild)    prints help on available methods

   conf = build_config_of_version(run, VERSION)
   build = AsterBuild(run, conf)

   # Compile all files of a directory
   #build.Compil(typ='C', rep='/opt/aster/NEW8/bibc/supervis', repobj='/tmp/Essai/obj', dbg='nodebug')

   # Compile a Code_Aster version
   #build.CompilAster(REPREF, repdest='/tmp/Essai')

   # Create a library
   #build.Archive(repobj='/tmp/Essai/obj', lib='/tmp/Essai/mylib.a')

   # Build an executable
   #build.Link(exe='/tmp/Essai/aster',
   #           lobj=['/tmp/Essai/mylib.a',],
   #           libaster='/opt/aster/NEW9/lib_obj/libaster.a',
   #           libferm = '/opt/aster/NEW9/lib_obj/libferm.a',
   #           reptrav='/tmp')

   #-------------------------------------------------------------------------------
   # RunAster function
   import asrun.execute
   # asrun.as_exec.RunAster(run, 'my_profile.export')

   #-------------------------------------------------------------------------------
   # Utility functions
   import asrun.maintenance
   # iret, l_v = asrun.maintenance.GetVersion(run)

   #-------------------------------------------------------------------------------
   # ----- module d'information sur les jobs
   import asrun.job

   #-------------------------------------------------------------------------------
   # ----- module interface avec le REX
   import asrun.rex

   #run.Sortie(iret)
