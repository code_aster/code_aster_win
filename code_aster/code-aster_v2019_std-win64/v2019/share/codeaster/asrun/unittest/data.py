# -*- coding: utf-8 -*-

# store the available remote hosts
available_hosts = {}


study_export = """
%(CONFIG_FILE_LINE)s

P actions make_etude
P nomjob study
P mode interactif
P version %(ASTER_VERSION)s
A memjeveux 128
A tpmax 30
P memjob 512000
F comm %(DATA)s/study.comm D 1
"""

study_dvp_export = """
%(CONFIG_FILE_LINE)s

P actions make_etude
P nomjob study_dvp
P mode interactif
P version %(ASTER_VERSION)s
A memjeveux 128
A tpmax 30
P memjob 512000
F comm %(DATA)s/chg_capy.comm D 1
F exec %(TMPDIR)s/aster.exe D 0
R cmde %(TMPDIR)s/commande D 0
"""

study_dvp_full_export = """
%(CONFIG_FILE_LINE)s

P actions make_etude
P actions make_exec
P nomjob study_dvp_full
P mode interactif
P version %(ASTER_VERSION)s
A memjeveux 128
A tpmax 30
P memjob 512000
F comm %(DATA)s/study.comm D 1
F f %(TMPDIR)s/debut.f D 0
F exec %(TMPDIR)s/aster.exe DR 0
"""

meshtool_export = """
%(CONFIG_FILE_LINE)s

P actions make_etude
P nomjob meshtool_cmdline
P origine ASTK VDEV
P mode interactif
P consbtc oui
P soumbtc oui
P version %(ASTER_VERSION)s
P memjob 512000
P tpsjob 1
A memjeveux 128
A tpmax 60

P special meshtool%%NEXT%%F libr %(DATA)s/forma01a.mmed  D%%NEXT%%F libr %(TMPDIR)s/mesh_out  R%%NEXT%%format_in='med'%%NEXT%%format_out='aster'%%NEXT%%info_mail=1%%NEXT%%info_cmd=2%%NEXT%%lang='FRANCAIS'
"""

compilation_export = """
%(CONFIG_FILE_LINE)s

P actions make_exec
P nomjob compilation
P mode interactif
P version %(ASTER_VERSION)s
F f %(TMPDIR)s/debut.f D 0
F exec %(TMPDIR)s/aster.exe R 0
"""

catapy_export = """
%(CONFIG_FILE_LINE)s

P actions make_cmde
P nomjob catapy
P mode interactif
P version %(ASTER_VERSION)s
F capy %(TMPDIR)s/debut.capy D 0
R cmde %(TMPDIR)s/commande R 0
"""

catalo_export = """
%(CONFIG_FILE_LINE)s

P actions make_ele
P nomjob catalo
P mode interactif
P version %(ASTER_VERSION)s
F cata %(TMPDIR)s/catalo.cata D 0
F ele %(TMPDIR)s/elements R 0
"""

server_export ="""
%(CONFIG_FILE_LINE)s

P mclient %(localhost)s
P serveur %(localhost)s
P username %(localuser)s
P protocol_exec asrun.plugins.server.SSHServer
P protocol_copyto asrun.plugins.server.SCPServer
P protocol_copyfrom asrun.plugins.server.SCPServer
P aster_root %(ASTER_ROOT)s
P proxy_dir %(TMPDIR)s/REMOTEDIR

P actions make_etude
P nomjob %(nomjob)s
P origine ASTK VDEV
P mode interactif
P consbtc oui
P soumbtc oui
P version %(ASTER_VERSION)s
P tpsjob 1
A memjeveux 128
A tpmax 10
P memjob 512000
F comm %(DATA)s/sleep.comm D 1
"""

astout_export ="""
%(CONFIG_FILE_LINE)s

P actions astout
P nomjob astout
P mode interactif
P version %(ASTER_VERSION)s
P debug nodebug

P ncpus 1
P nbmaxnook 1
P cpresok RESOK
P facmtps 2.5
P tpsjob 1

F hostfile %(DATA)s/hostfile D 0
F list %(DATA)s/astout.list D 0
R resu_test %(TMPDIR)s/astout.resu_test R 0
R rep_test %(TMPDIR)s/astout.rep_test D 0
R flash %(TMPDIR)s/astout.flash R 0
P diag_pickled %(TMPDIR)s/diag.pick
"""

astout_dvp_export = """
%(CONFIG_FILE_LINE)s

P actions astout
P nomjob astout_dvp
P mode interactif
P version %(ASTER_VERSION)s
P debug nodebug

P ncpus 1
P nbmaxnook 50
P cpresok RESOK
P facmtps 1
P tpsjob 1
F list %(DATA)s/astout_dvp.list D 0
R resu_test %(TMPDIR)s/astout_dvp.resu_test R 0
F exec %(TMPDIR)s/aster.exe D 0
"""

parametric_export = """
%(CONFIG_FILE_LINE)s

P actions distribution
P nomjob parametric
P mode interactif
P version %(ASTER_VERSION)s
A memjeveux 128
A tpmax 30
P memjob 512000
P tpsjob 1
F comm %(DATA)s/study.comm D 1
F distr %(DATA)s/parametric.distr D 0
F libr %(DATA)s/parametric.pre.include D 38
R repe %(TMPDIR)s/parametric.resu R 0
R base %(TMPDIR)s/parametric.base R 0
"""

parametric_poursuite_export = """
%(CONFIG_FILE_LINE)s

P actions distribution
P nomjob parametric_poursuite
P mode interactif
P version %(ASTER_VERSION)s
A memjeveux 128
A tpmax 30
P memjob 512000
P tpsjob 1
F comm %(DATA)s/poursuite.comm D 1
F distr %(DATA)s/parametric.distr D 0
F libr %(DATA)s/parametric.pre.include D 38
R base %(TMPDIR)s/parametric.resu D 0
R repe %(TMPDIR)s/parametric_poursuite.resu R 0
"""
