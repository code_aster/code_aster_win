#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
import unittest

from common import dict_conf, execcmd, tmpdir
from data   import server_export

from asrun.calcul import parse_submission_result
from asrun.job    import parse_actu_result, parse_tail_result
from asrun.run    import AsRunFactory
from asrun.profil import AsterProfil
from asrun.calcul import AsterCalcul



class TestServerFunctions(unittest.TestCase):

    def setUp(self):
        self.export = os.path.join(tmpdir, "server.export")
        dico = dict_conf.copy()
        dico['nomjob'] = 'server_cmdline'
        with open(self.export, "w") as f:
            f.write(server_export % dico)


    def test01_from_cmdline(self):
        # the same test from the client side is 200_client.test02_proxy
        cmd = dict_conf["as_run"] + ["--serv", self.export]
        iret, out = execcmd(cmd, "server.1", return_output=True)
        nomjob = "server_cmdline"
        assert iret == 0
        jobid, queue, studyid = parse_submission_result(out)
        assert jobid.strip() != ""
        assert studyid.strip() != ""

        time.sleep(0.2)
        cmd = dict_conf["as_run"] + ["--actu", jobid, nomjob, "interactif"]
        iret, out = execcmd(cmd, "server.2", return_output=True)
        assert iret == 0
        etat, diag, node, tcpu, wrk, queue = parse_actu_result(out)
        assert etat == "RUN", etat

        ftail = os.path.join(tmpdir, "server.3.tail")
        cmd = dict_conf["as_run"] + ["--tail", jobid, nomjob, "interactif", ftail, "10"]
        iret, out = execcmd(cmd, "server.3", return_output=True)
        assert iret == 0
        job, jobid_, etat, diag = parse_tail_result(out)
        assert etat == "RUN", etat

        etat = None
        while etat not in ('ENDED', '_'):
            time.sleep(4)
            cmd = dict_conf["as_run"] + ["--actu", jobid, nomjob, "interactif"]
            iret, out = execcmd(cmd, "server.4", return_output=True)
            assert iret == 0
            etat, diag, node, tcpu, wrk, queue = parse_actu_result(out)
        assert etat == "ENDED", etat
        assert diag == "<A>_ALARM", diag

        cmd = dict_conf["as_run"] + ["--del", jobid, nomjob, "interactif"]
        iret = execcmd(cmd, "server.5")
        assert iret == 0


    def test02_using_api(self):
        run = AsRunFactory()
        prof = AsterProfil(run=run, filename=self.export)
        # change job name and job number
        prof['nomjob'] = "server_api"
        calc = AsterCalcul(run, prof=prof, pid=run.get_pid())
        jret, out = calc.start()
        assert jret == 0, "submission problem : %s" % out

        time.sleep(0.2)
        etat, diag, node, tcpu, wrk, queue = calc.get_state()
        assert etat == "RUN", etat

        etat, diag, s_out = calc.tail(expression='CODE_ASTER')
        assert etat == "RUN", etat

        calc.wait()

        etat, diag, node, tcpu, wrk, queue = calc.get_state()
        assert etat == "ENDED", etat
        assert diag == "<A>_ALARM", diag
        assert run.GetGrav(calc.diag) in (0, 1), calc.diag
        assert os.path.isfile(calc.flash('output'))
        calc.kill()



if __name__ == "__main__":
    unittest.main()
