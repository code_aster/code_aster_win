#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import unittest
import shutil
from glob   import glob

from common import tmpdir


class TestClean(unittest.TestCase):

    def test01_rm_tmpdir(self):
        ldirs = glob(os.path.splitext(tmpdir)[0] + ".*")
        for dd in ldirs:
            shutil.rmtree(dd)


if __name__ == "__main__":
    unittest.main()
