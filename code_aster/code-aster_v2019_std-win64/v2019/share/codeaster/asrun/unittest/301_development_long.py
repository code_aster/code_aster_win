#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as osp
import re
import shutil
import unittest

from common import dict_conf, execcmd, tmpdir, aster_version
from data   import catalo_export


class TestDevelopLong(unittest.TestCase):

    def test03_catalo(self):
        from asrun.run import AsRunFactory
        run = AsRunFactory()
        orig_cata = osp.join(run.get_version_path(aster_version),
                             "catalo", "compelem", "grandeur_simple__.cata")
        export = osp.join(tmpdir, "catalo.export")
        with open(export, "w") as f:
            f.write(catalo_export % dict_conf)
        shutil.copy(orig_cata, osp.join(tmpdir, "catalo.cata"))

        cmd = dict_conf["as_run"] + [export]
        iret, out = execcmd(cmd, "dvp.3", return_output=True)
        assert iret == 0
        assert re.search("DIAGNOSTIC JOB : OK", out) \
          or re.search("DIAGNOSTIC JOB :.*ALARM", out)



if __name__ == "__main__":
    unittest.main()
