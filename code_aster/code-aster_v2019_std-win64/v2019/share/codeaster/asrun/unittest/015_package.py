#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path as osp
import unittest
from distutils.sysconfig import get_python_lib

from common import confdir


class TestPackage(unittest.TestCase):

    def test01_prefix(self):
        aster_root_ini = os.environ.get("ASTER_ROOT")
        assert aster_root_ini, "ASTER_ROOT environment variable required!"
        inst = osp.join(get_python_lib(prefix=aster_root_ini), "asrun", "installation.py")
        cmd = "%s run_test.py [args]" % os.getenv('PYTHONEXECUTABLE')
        assert osp.exists(inst), "%s not found. You should probably run %s" % (inst, cmd)

        def run_inst(aster_root):
            previous = os.environ.get("ASTER_ROOT")
            os.environ["ASTER_ROOT"] = aster_root
            ctxt = {}
            with open(inst) as f:
                exec(compile(f.read(), inst, 'exec'), ctxt)
            os.environ["ASTER_ROOT"] = previous
            return tuple([ctxt[var] for var in \
                        ("aster_root", "prefix", "confdir", "datadir", "localedir")])

        #XXX adjustement needed to be check on windows
        res = run_inst("/opt/aster")
        assert res == ("/opt/aster", "/opt/aster", "/opt/aster/etc/codeaster",
                       "/opt/aster/share/codeaster/asrun/data", "/opt/aster/share/locale"), res
        res = run_inst("/usr")
        assert res == ("/usr", "/", "/etc/codeaster", "/usr/share/codeaster/asrun/data",
                       "/usr/share/locale"), res


if __name__ == "__main__":
   unittest.main()
