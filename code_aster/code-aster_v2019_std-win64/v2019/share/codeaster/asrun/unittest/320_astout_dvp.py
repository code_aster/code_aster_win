#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import unittest

from common import dict_conf, execcmd, tmpdir
from data   import astout_dvp_export

import asrun


class TestUsingDvpt(unittest.TestCase):

    def test01_using_api(self):
        assert os.path.exists(os.path.join(tmpdir, "aster.exe")), \
                "testcase 'compilation@development' must have been run before!"
        export = os.path.join(tmpdir, "astout_dvp.export")
        with open(export, "w") as f:
            f.write(astout_dvp_export % dict_conf)
        cmd = dict_conf["as_run"] + ["--only_nook", export]
        iret = execcmd(cmd, "astout_dvp.1")
        assert iret == 0
        mess = os.path.join(tmpdir, "astout_dvp.resu_test", "adlv100a.mess")
        with open(mess, "r") as f:
            content = f.read()
        assert len(re.findall("MCTEST_EXE_OK", content)) == 1
        mess = os.path.join(tmpdir, "astout_dvp.resu_test", "forma01a.mess")
        with open(mess, "r") as f:
            content = f.read()
        assert len(re.findall("MCTEST_EXE_OK", content)) == 1


if __name__ == "__main__":
    unittest.main()
