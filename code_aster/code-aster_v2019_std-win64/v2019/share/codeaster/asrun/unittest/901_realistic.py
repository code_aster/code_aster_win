#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import os.path as osp
import unittest

from common import dict_conf, execcmd, tmpdir

from asrun.job    import (parse_actu_result, print_actu_result,
                          parse_tail_result, print_tail_result)
from asrun.profil import AsterProfil
from asrun.calcul import parse_submission_result


_inum = 0
def ftrace(nomjob):
    global _inum
    _inum += 1
    return "%s.%s" % (nomjob, _inum)


class TestClientFunctions(unittest.TestCase):

    def test01_ovh(self):
        ntrace = "ovh"
        nomjob = "ssnp125a"
        print()

        cmd = dict_conf["as_run"] + ["--get_export", nomjob]
        iret, out = execcmd(cmd, ftrace(ntrace), return_output=True)
        export = osp.join(tmpdir, nomjob + "_ovh.export")
        prof = AsterProfil()
        # add new parameters (will be added by the client)
        dico = dict_conf.copy()
        dico['nomjob'] = nomjob
        # open informations of the remote server
        fname = osp.join(dict_conf['DATA'], "testserv.export")
        assert osp.exists(fname), "a file containing server connection parameters is needed"
        # template :
        #P serveur FULL_SERVER_NAME_OR_IP_ADDRESS
        #P aster_root ASTER_ROOT
        #P platform LINUX
        #P noeud NODE_NAME
        #P username REMOTE_USERNAME
        #P protocol_exec asrun.plugins.server.SSHServer
        #P protocol_copyto asrun.plugins.server.SCPServer
        #P protocol_copyfrom asrun.plugins.server.SCPServer
        #P proxy_dir PROXY_DIR_NAME
        with open(fname, 'r') as f:
            params = f.read()
        out += params
        out += """
P nomjob %(nomjob)s
F resu %(TMPDIR)s/%(nomjob)s_ovh.resu R 8
P mclient %(localhost)s
""" % dico
        prof.parse(out)
        prof['version'] = "STA10.1"
        prof.WriteExportTo(export)

        cmd = dict_conf["as_run"] + ["--proxy", "--serv", export]
        print(' '.join(cmd))
        iret, out = execcmd(cmd, ftrace(ntrace), return_output=True)
        assert iret == 0
        jobid, queue, studyid = parse_submission_result(out)
        print("submission:", jobid, queue, studyid)
        assert jobid.strip() != ""

        etat = "RUN"
        prof['jobid'] = jobid
        prof['studyid'] = studyid
        prof.WriteExportTo(export)
        while etat == "RUN":
            time.sleep(1.)
            cmd = dict_conf["as_run"] + ["--proxy", "--actu", export]
            print(' '.join(cmd))
            iret, out = execcmd(cmd, ftrace(ntrace), return_output=True)
            etat, diag, node, tcpu, wrk, queue = parse_actu_result(out)
            print_actu_result(etat, diag, node, tcpu, wrk, queue)

        cmd = dict_conf["as_run"] + ["--proxy", "--get_results", export]
        print(' '.join(cmd))
        iret, out = execcmd(cmd, ftrace(ntrace), return_output=True)

        cmd = dict_conf["as_run"] + ["--proxy", "--del", export]
        print(' '.join(cmd))
        iret = execcmd(cmd, ftrace(ntrace))
        assert iret == 0


if __name__ == "__main__":
    unittest.main()
