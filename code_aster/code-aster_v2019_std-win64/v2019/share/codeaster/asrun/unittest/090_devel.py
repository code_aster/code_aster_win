#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path as osp
import unittest

from common import aster_version

import asrun
from asrun.run          import AsRunFactory
from asrun.dev.routines import get_available_te, get_available_op, get_available_lc



class TestDevel(unittest.TestCase):

    def test01_routines(self):
        run = AsRunFactory()
        bibfor = osp.join(run.get_version_path(aster_version), 'bibfor')
        lte = get_available_te(bibfor)
        lop = get_available_te(bibfor)
        llc = get_available_te(bibfor)
        assert len(lte) > 0
        assert len(lop) > 0
        assert len(llc) > 0


if __name__ == "__main__":
    unittest.main()
