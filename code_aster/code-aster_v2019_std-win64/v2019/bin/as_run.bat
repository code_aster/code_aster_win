echo OFF
setlocal
chcp 65001
set ASTER_ROOT_DIR=%~dp0..
set OUTILS=%ASTER_ROOT_DIR%\outils
set PYTHONHOME=%ASTER_ROOT_DIR%\Python37
set PYTHONPATH=%ASTER_ROOT_DIR%\lib\python3.7\site-packages
set PATH=%PYTHONHOME%;%OUTILS%;%ASTER_ROOT_DIR%\MinGW\bin;%PATH%
set LIBRARY_PATH=%ASTER_ROOT_DIR%\MinGW\lib64
set PYTHONEXECUTABLE=python.exe
set ASTER_TMPDIR=%TEMP%
%PYTHONEXECUTABLE% -c "import sys, asrun ; from asrun.main import main,start; del sys.argv[0]; main()" %0 %*
