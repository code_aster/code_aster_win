This product intend to provide a Code_Aster version for Windows users.
Updates and support can be found here : https://bitbucket.org/siavelis/codeaster-windows-src/

The version of Code_Aster provided in this package has been build using mingw gnu compiler https://mingw-w64.org/
It intend to be compatible with x86_64 architecture on Windows platform.


INFORMATIONS
============

This package contains versions 14.4 of Code_Aster as stable and testing

The Code_Aster executable, aster.exe, provided in this package have been statically linked to the following prerequisites :

- hdf5
- med
- metis
- scotch
- mumps

Theses prerequisites have been build using the tarball sources package available at https://code-aster.org.

Some other tools/prerequisites are available on demand in premium version:

- homard
- miss3d
- mfront
- petsc

Focus was done on stabillity: most part of the official test cases have been run succesfully using a windows 10 64 bits (build 1903).

Modified source code is available on bitbucket https://bitbucket.org/siavelis/codeaster-windows-src/src,
known issues are available at https://bitbucket.org/siavelis/codeaster-windows-src/


INSTALATION
===========

This version was made as portable as possible. Please report any installation issue.

For MPI version, you should install msmpi (latest version is best). Version v9.0.1 is provided here:

https://download.microsoft.com/download/4/A/6/4A6AAED8-200C-457C-AB86-37505DE4C90D/msmpisetup.exe

USING
=====

To execute a simple test case provided in the testing/test directory, simply run (in cmd.exe or powershell):

> bin\as_run --test forma01a

To use testing vertion

> bin\as_run.bat --vers testing --test forma01a

To execute a customized export file, simply run :

> bin\as_run.bat example\forma01a.export

To execute a sample of test cases provided in the testing/test directory :

> bin\as_run.bat astout\run_test.export

For export files previously generated on different platforms (if you use both Linux and Windows for example),
it is stongly encouraged to use unix2dos command (or dos2unix in the other way) to convert your export files before switching.
Absolute path should also be adjusted.

SUPPORT
=======

support@simulease.com

