��    %     D	  �  l      �  7   �  F  �  7      '   X     �  ]   �  �   �  (   �     �     �     �           ,     M     h     �  %   �     �     �     �          )     G  y   c  `   �  k   >  R   �  *   �     (  %   B  '   h     �  "   �  F   �           (      :      N   	   j   	   t      ~      �   @   �   @   �   @   !  @   Z!  @   �!  @   �!  Q   "  Q   o"  Q   �"  Q   #  Q   e#  Q   �#  6   	$     @$     T$     \$  v   `$     �$     �$     �$     %     -%     B%  #   V%     z%  4   �%     �%  @   �%     &     +&     I&  *   f&      �&     �&     �&     �&     �&     �&  	   �&     �&     '     '  %   .'  %   T'     z'     �'     �'     �'     �'     �'     �'     �'     �'     �'     �'     �'      (     (     (     &(     .(     6(     =(     D(  7   K(  $   �(     �(     �(     �(     �(     �(     �(     )  3   )     G)     _)  -   x)     �)     �)     �)     �)  "   �)  $   *     ?*     B*  S   U*  2   �*  >   �*  $   +     @+     O+  
   W+     b+  !   w+  	   �+  
   �+  ,   �+  ,   �+  ,   ,  ,   5,  ,   b,  ,   �,  ,   �,      �,  !   
-  >   ,-     k-     s-     |-     �-     �-     �-  (   �-  (   �-  (   �-  (   .  (   A.  (   j.  (   �.     �.  	   �.  :   �.  ;   /  ;   K/  ;   �/  ;   �/  ;   �/  ;   ;0  ;   w0  ;   �0  ;   �0  ;   +1  :   g1  ;   �1  ;   �1  ;   2  ;   V2  ;   �2  ;   �2  ;   
3  ;   F3  ;   �3  ;   �3  :   �3  ;   54  ;   q4  ;   �4  ;   �4  ;   %5  ;   a5  ;   �5  ;   �5  ;   6  ;   Q6  :   �6  ;   �6  ;   7  ;   @7  ;   |7     �7     �7     �7     �7     �7     8  $   %8  $   J8  $   o8  $   �8  $   �8  $   �8  %   9     )9     19     C9     U9     g9     |9     �9     �9  
   �9     �9     �9     :     :  3   $:     X:  )   a:  ;   �:  	   �:     �:     �:     �:     ;     ;     .;     :;     L;     R;     g;     v;     �;     �;     �;     �;     �;     �;  #   �;  #   
<  #   .<  #   R<  #   v<  #   �<  #   �<  #   �<  #   =  #   *=  #   N=  #   r=     �=     �=      �=     �=     �=     �=     >     ,>     9>  '   Q>  (   y>  (   �>  (   �>  (   �>  (   ?  (   F?  (   o?  (   �?  (   �?  '   �?  '   @  '   :@  '   b@  '   �@  '   �@  '   �@  '   A     *A  8   <A  !   uA     �A  �  �A  7   �C  Q  �C  "   E  '   ;E     cE  Q   sE  �   �E  +   lF     �F     �F     �F     �F  %   �F     $G     <G     OG     `G     �G     �G     �G     �G     �G     �G  y   H  `   �H  k   �H  R   aI  *   �I     �I  %   �I  '   J     GJ  $   ]J  F   �J     �J     �J     �J     K  
   K  	   'K  	   1K     ;K  >   NK  >   �K  >   �K  >   L  >   JL  >   �L  G   �L  G   M  G   XM  G   �M  G   �M  G   0N  4   xN     �N     �N     �N  m   �N     ;O     PO     cO     uO     �O     �O     �O     �O  0   �O     P  >   (P     gP     �P     �P  %   �P      �P     Q     Q     Q     $Q     )Q  	   AQ     KQ     YQ     hQ  &   �Q  &   �Q     �Q     �Q     �Q     �Q     �Q     R     R     R      R     (R     7R     FR     UR     dR     sR     {R     �R     �R     �R     �R  8   �R  #   �R     �R     S     "S     /S     >S     VS  
   ZS  6   eS     �S     �S  5   �S     T     !T     9T     LT     bT  &   �T     �T     �T  A   �T  2   �T  =   2U  %   pU     �U     �U     �U     �U      �U  	   �U  
   �U  0   V  0   3V  0   dV  0   �V  0   �V  0   �V  0   (W     YW     yW  ?   �W     �W     �W     �W     �W     �W     �W  (   X  (   9X  (   bX  (   �X  (   �X  (   �X  (   Y     /Y     =Y  >   FY  ?   �Y  ?   �Y  ?   Z  ?   EZ  ?   �Z  ?   �Z  ?   [  ?   E[  ?   �[  ?   �[  >   \  ?   D\  ?   �\  ?   �\  ?   ]  ?   D]  ?   �]  ?   �]  ?   ^  ?   D^  ?   �^  >   �^  ?   _  ?   C_  ?   �_  ?   �_  ?   `  ?   C`  ?   �`  ?   �`  ?   a  ?   Ca  >   �a  ?   �a  ?   b  ?   Bb  ?   �b     �b     �b     �b     �b     �b     c      &c      Gc      hc      �c      �c      �c      �c     d     d     %d     7d     Id     ^d     sd     �d  	   �d     �d     �d     �d     �d  :   �d     .e  (   7e  6   `e  
   �e     �e     �e     �e     �e     �e     �e     f     f     f     1f     @f     Of     \f     lf     sf     �f     �f     �f     �f     �f     g     /g     Og     og     �g     �g     �g     �g     
h     )h     8h     Uh     th     �h     �h     �h     �h     �h  &   �h  (   i  (   5i  (   ^i  (   �i  (   �i  (   �i  (   j  (   +j  (   Tj  '   }j  '   �j  '   �j  '   �j  '   k  '   Ek  '   mk  '   �k     �k  <   �k  "   l     .l                )   �         �       $      �   o                   #   	           %      �   �            �   �           �   �   M   n   �   �   �   �   �   �   !       �   �   �      �   �   �   �   `   �   8   �   �   �           A   +   �   ,   -   .   /   0   1       �                �       �          Z   [   �   D   %   �   �   �   :       
   	  �   �     �   f   g   h   j      �                 �   �         �     &   R     X   s             �   �       w     �             �   J                 �           �                              U   �       t        �                   !            �       �   L   q   �   T   ~   v   �   �   �   �   N   �     �         �                              �   K   �      $   �   �   �   �   V   W       =           a   b   c   d   e              �   �   (   F   �      |   �       ^   _           �         �   �       �       I       �   �   5          �   z       �               @   {   �   �   �   E   �   �   �       �     <   �   }       �   �   �   �   �   �   �   �   �   �               �         �   �   P   �   >       �   ;       �   �          p   �         �       r   �   u      �       Q       l           �         �   �   i   9   k   �       m         2   3   4      6   7   �                          �          y       �   �       "  S   �   #  �       '   x   Y   C   \   ]   �      �   �      �                 G   �   �       �      ?      �   �       H                    �   "         *   �       �   O   
  B    
                                               %(k1)s  
    Vous utilisez une vieille version de Code_Aster.

    En mettant à jour votre version, vous bénéficierez des dernières améliorations
    apportées au code depuis 15 mois.
    Si vous avez des développements privés, vous risquez d'avoir un travail
    important de portage si vous ne suivez pas les mises à jour.
 
   la nature de l'excitation est             : %(k1)s  
  DP     =  %(r1)f
  F(DP)  =  %(r2)f
 
  F(XMIN) > 0
 
  Manque de mémoire :
     Mémoire disponible = %(i1)d
     Mémoire nécessaire = %(i2)d
 
  Valeur initiale du temps CPU maximum =   %(i1)d secondes
  Valeur du temps CPU maximum passé aux commandes =   %(i2)d secondes
  Réserve CPU prévue = %(i3)d secondes
 
  dimension max du problème :  %(i1)d
 
  incohérence détectée
 
 Erreur : LMAT est nul
 
 Mode non compatible.
 
 Structure non tubulaire
 
 aucun axe de rotation défini
 
 données incompatibles.
 
 données incorrectes.
 
 erreur DPVP_2
 
 erreur de répétitivité cyclique
 
 erreur(s) dans les données
 
 format  %(k1)s  inconnu.
 
 il manque des masses.
 
 il manque des rigidités.
 
 manque les accélérations
   %(i1)4d       %(r1)12.5E
  %(i1)4d      %(r1)12.5E       %(r2)12.5E     %(i2)4d       %(i3)4d    %(r3)12.5E    %(i4)4d    %(r4)12.5E    %(r5)12.5E
  %(i1)4d      %(r1)12.5E       %(r2)12.5E     %(i2)4d       %(i3)4d    %(r3)12.5E    %(r4)12.5E
  %(i1)4d      %(r1)12.5E       %(r2)12.5E     %(i2)4d    %(r3)12.5E    %(i3)4d    %(r4)12.5E    %(r5)12.5E
  %(i1)4d      %(r1)12.5E       %(r2)12.5E     %(i2)4d    %(r3)12.5E    %(r4)12.5E
  %(i1)4d      %(r2)12.5E       %(r1)12.5E
  %(i1)5d        %(k1)24s
  %(i1)5d        %(k1)24s    %(k2)24s
  ALPHA = %(r1)12.5E
 BETA = %(r2)12.5E
  LAMBDA = %(r1)12.5E
  La bande de fréquence est vide.
 %(i1)4d        %(i2)4d       %(r1)10.3E      ( %(r2)9.2E, %(r3)9.2E )
 --- Fin de l'exécution > %d commandes... A propos de Stanley Adresse du poste de travail Affichage Animation Annuler Arret sur erreur BETON_RAG : Endommagement intrinsèque de traction, composante 1 BETON_RAG : Endommagement intrinsèque de traction, composante 2 BETON_RAG : Endommagement intrinsèque de traction, composante 3 BETON_RAG : Endommagement intrinsèque de traction, composante 4 BETON_RAG : Endommagement intrinsèque de traction, composante 5 BETON_RAG : Endommagement intrinsèque de traction, composante 6 BETON_RAG : Endommagement macroscopique (indicateur de fissuration)  composante 1 BETON_RAG : Endommagement macroscopique (indicateur de fissuration)  composante 2 BETON_RAG : Endommagement macroscopique (indicateur de fissuration)  composante 3 BETON_RAG : Endommagement macroscopique (indicateur de fissuration)  composante 4 BETON_RAG : Endommagement macroscopique (indicateur de fissuration)  composante 5 BETON_RAG : Endommagement macroscopique (indicateur de fissuration)  composante 6 BETON_RAG : Endommagement macroscopique en compression Beton_umlv_fp : v21 C-A-S-H CSH Ceci est une alarme. Si vous ne comprenez pas le sens de cette
alarme, vous pouvez obtenir des résultats inattendus ! Changement des fontes Command output : Commandes utilisées Concepts jamais utilisés Creation d'un chemin Creation d'un point Déformation sphérique réversible Dépendances ERREUR A LA VERIFICATION DU CATALOGUE - INTERRUPTION Erreur VISU Salome Erreur construction table de valeur pour visualisation 2D SALOME Erreur dans la macro %s
%s Erreur exécution commande :  Erreur lecture fichier MED : Erreur type de visualisation non supporté Erreur visualisation dans SALOME Exit code : %d Force GAZ : v1 Gmsh Gmsh : Version du fichier HYDR : v1 HYDRENDO : v1 HYDR_UTIL : v1 Hayhurst : variable PHI Hayhurst : variable d'écrouissage H1 Hayhurst : variable d'écrouissage H2 Incrément de temps LIQADGV1 LIQADGV2 LIQADGV3 LIQGATM1 LIQGATM2 LIQGAZ1 LIQGAZ2 LIQSAT1 LIQU_AD_GAZ v1 LIQU_AD_GAZ v2 LIQU_AD_GAZ v3 LIQU_AD_GAZ v4 LIQU_AD_GAZ v5 LIQVAP1 LIQVAP2 LIQVAP3 LIQVG1 LIQVG2 LIQVG3 Le nom de concept %s est trop long (8 caractères maxi) Les types entrés ne sont pas permis Login Machine Windows/Samba Machine de Gmsh Machine de Salome Machine de visualisation Mode Mode graphique Mot-clé %s invalide : %s
Critère de validité: %s Mot-clé simple : %s %s Mots clés inconnus : %s Nom de concept invalide ('SD_' est réservé) Nombre de commandes Nombre de dépendances Nombre de noeuds Nombre de résultats Nombre de résultats non utilisés None n'est pas une valeur autorisée OK Options graphiques Parallélisme OpenMP : actif
                Nombre de processus utilisés : %(i1)d Parametre pour Gmsh : version du fichier resultat. Parametre pour Salome : visualisation dans POSTPRO ou PARAVIZ. Pas de nom pour le concept retourné Port de Salome Quitter Résultats Salome : Visualiseur Serveur de calcul Aster / Stanley Smbclient Total time VISCOCHAB, voir R5.03.12, variable interne 3 VISCOCHAB, voir R5.03.12, variable interne 4 VISCOCHAB, voir R5.03.12, variable interne 5 VISCOCHAB, voir R5.03.12, variable interne 6 VISCOCHAB, voir R5.03.12, variable interne 7 VISCOCHAB, voir R5.03.12, variable interne 8 VISCOCHAB, voir R5.03.12, variable interne 9 Valider les parametres et sortir Vitesse de dissipation mécanique Voulez-vous quitter ? Les paramètres modifiés seront perdus. Xmgrace [ SKIP ] [FAILED] afm aft after %d timers assemblage cornière, variable interne 1 assemblage cornière, variable interne 2 assemblage cornière, variable interne 3 assemblage cornière, variable interne 4 assemblage cornière, variable interne 5 assemblage cornière, variable interne 6 assemblage cornière, variable interne 7 calcium libre cohésion comportement GRANGER_FP, voir R7.01.01, variable interne 1 comportement GRANGER_FP, voir R7.01.01, variable interne 10 comportement GRANGER_FP, voir R7.01.01, variable interne 11 comportement GRANGER_FP, voir R7.01.01, variable interne 12 comportement GRANGER_FP, voir R7.01.01, variable interne 13 comportement GRANGER_FP, voir R7.01.01, variable interne 14 comportement GRANGER_FP, voir R7.01.01, variable interne 15 comportement GRANGER_FP, voir R7.01.01, variable interne 16 comportement GRANGER_FP, voir R7.01.01, variable interne 17 comportement GRANGER_FP, voir R7.01.01, variable interne 18 comportement GRANGER_FP, voir R7.01.01, variable interne 19 comportement GRANGER_FP, voir R7.01.01, variable interne 2 comportement GRANGER_FP, voir R7.01.01, variable interne 20 comportement GRANGER_FP, voir R7.01.01, variable interne 21 comportement GRANGER_FP, voir R7.01.01, variable interne 22 comportement GRANGER_FP, voir R7.01.01, variable interne 23 comportement GRANGER_FP, voir R7.01.01, variable interne 24 comportement GRANGER_FP, voir R7.01.01, variable interne 25 comportement GRANGER_FP, voir R7.01.01, variable interne 26 comportement GRANGER_FP, voir R7.01.01, variable interne 27 comportement GRANGER_FP, voir R7.01.01, variable interne 28 comportement GRANGER_FP, voir R7.01.01, variable interne 29 comportement GRANGER_FP, voir R7.01.01, variable interne 3 comportement GRANGER_FP, voir R7.01.01, variable interne 30 comportement GRANGER_FP, voir R7.01.01, variable interne 31 comportement GRANGER_FP, voir R7.01.01, variable interne 32 comportement GRANGER_FP, voir R7.01.01, variable interne 33 comportement GRANGER_FP, voir R7.01.01, variable interne 34 comportement GRANGER_FP, voir R7.01.01, variable interne 35 comportement GRANGER_FP, voir R7.01.01, variable interne 36 comportement GRANGER_FP, voir R7.01.01, variable interne 37 comportement GRANGER_FP, voir R7.01.01, variable interne 38 comportement GRANGER_FP, voir R7.01.01, variable interne 39 comportement GRANGER_FP, voir R7.01.01, variable interne 4 comportement GRANGER_FP, voir R7.01.01, variable interne 40 comportement GRANGER_FP, voir R7.01.01, variable interne 41 comportement GRANGER_FP, voir R7.01.01, variable interne 42 comportement GRANGER_FP, voir R7.01.01, variable interne 43 cpu cpu+sys degre d hydratation degres d hydratation moyen dissipation Thermodynamique dissipation plastique déformation plastique composante XX déformation plastique composante XY déformation plastique composante XZ déformation plastique composante YY déformation plastique composante YZ déformation plastique composante ZZ déformation sphérique irréversible elapsed elas_poutre_GR v1 elas_poutre_GR v2 elas_poutre_GR v3 endommagement normal endommagement scalaire endommagement tangentiel endommagement thermique gonflement indicateur de flambement indicateur de viscoplasticité indice des vides irradiation length of command shell greater than %d characters. liste %s numéro de l'élément pointé numéro 1, numéro de l'élément pointé numéro 2 (quand amorçage), porosité pression critique pression de fluide pression de gel pression hydrique rigidité résiduelle saut normal saut tangentiel 1 seuil seuil de compression seuil hydrique seuil isotrope sulfate libre sulfates fixes system temps de chargement cumule température température maximum tenseur cinématique, composante XX tenseur cinématique, composante XY tenseur cinématique, composante XZ tenseur cinématique, composante YY tenseur cinématique, composante YZ tenseur cinématique, composante ZZ tenseur endommagement, direction XX tenseur endommagement, direction XY tenseur endommagement, direction XZ tenseur endommagement, direction YY tenseur endommagement, direction YZ tenseur endommagement, direction ZZ type non supporté valeur %s obligatoire valeur dans l'intervalle %s , %s valeur de %s valeur valide variation de volume cumulée variation volumique volume d eau volume d ett secondaire éléments discrets, variable interne 1 éléments discrets, variable interne 10 éléments discrets, variable interne 11 éléments discrets, variable interne 12 éléments discrets, variable interne 13 éléments discrets, variable interne 14 éléments discrets, variable interne 15 éléments discrets, variable interne 16 éléments discrets, variable interne 17 éléments discrets, variable interne 18 éléments discrets, variable interne 2 éléments discrets, variable interne 3 éléments discrets, variable interne 4 éléments discrets, variable interne 5 éléments discrets, variable interne 6 éléments discrets, variable interne 7 éléments discrets, variable interne 8 éléments discrets, variable interne 9 énergie bloquée énergie dissipée cumulée à chaque pas pour CRIT_RUPT énergie dissipée pour CRIT_RUPT énergie résiduelle Project-Id-Version: code-aster
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-13 05:29-0400
Last-Translator: ottovonvictor <ottovonvictor@yahoo.com>
Language-Team: Romanian
Language: ro_RO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100>0 && n%100<20)) ? 1 : 2);
X-Generator: crowdin.com
X-Crowdin-Project: code-aster
X-Crowdin-Language: ro
X-Crowdin-File: /v13/code_aster.po
 
                                               %(k1)s  
 Utilizați o versiune mai veche a programului Code_Aster.

 Dacă actualizați la o versiune mai nouă, veți beneficia de ultimele îmbunătățiri
 aduse programului în 15 luni.
 Dacă lucrați la un cod de dezvoltare privat,  este posibil să aveți alte lucruri 
 mai importante de efectuat, dacă nu urmați aceste actualizări.
 
 natura excitaţie este : %(k1)s  
  DP     =  %(r1)f
  F(DP)  =  %(r2)f
 
  F(XMIN) > 0
 
 Lipsă de memorie :
 Memorie disponibilă = %(i1)d
 Memorie necesară = %(i2)d
 
 Valoarea inițială maximă de timp  CPU = %(i1)d secunde
 Valoarea de timp CPU maximă pentru comenzi  = %(i2)d secunde
 Rezerva de așteptat CPU = %(i3)d secunde
 
 dimensiunea maximă a problemei : %(i1)d
 
  incoerență detectată
 
 Eroare : LMAT este nul
 
 Mod incompatibil.
 
 Structură ne-tubulară
 
 nu este definită axa de rotație 
 
 date incompatibile. 
 
 date incorecte.
 
 eroare DPVP_2
 
 eroare ciclică de repetare 
 
 eroare(i) în date 
 
 formatul %(k1)s necunoscut.
 
 lipsesc masele.
 
 lipsesc rigiditățile.
 
 lipsesc accelerațiile
   %(i1)4d       %(r1)12.5E
  %(i1)4d      %(r1)12.5E       %(r2)12.5E     %(i2)4d       %(i3)4d    %(r3)12.5E    %(i4)4d    %(r4)12.5E    %(r5)12.5E
  %(i1)4d      %(r1)12.5E       %(r2)12.5E     %(i2)4d       %(i3)4d    %(r3)12.5E    %(r4)12.5E
  %(i1)4d      %(r1)12.5E       %(r2)12.5E     %(i2)4d    %(r3)12.5E    %(i3)4d    %(r4)12.5E    %(r5)12.5E
  %(i1)4d      %(r1)12.5E       %(r2)12.5E     %(i2)4d    %(r3)12.5E    %(r4)12.5E
  %(i1)4d      %(r2)12.5E       %(r1)12.5E
  %(i1)5d        %(k1)24s
  %(i1)5d        %(k1)24s    %(k2)24s
  ALPHA = %(r1)12.5E
 BETA = %(r2)12.5E
  LAMBDA = %(r1)12.5E
  Banda de frecvență este goală. 
 %(i1)4d        %(i2)4d       %(r1)10.3E      ( %(r2)9.2E, %(r3)9.2E )
 --- Sfârșitul execuției > %d comenzi... Despre Stanley Adresa postului de lucru Afișează Animație Anulează Oprește la eroare BETON_RAG : Deteriorare  de tensiune intrinsecă, componenta 1 BETON_RAG : Deteriorare  de tensiune intrinsecă, componenta 2 BETON_RAG : Deteriorare  de tensiune intrinsecă, componenta 3 BETON_RAG : Deteriorare  de tensiune intrinsecă, componenta 4 BETON_RAG : Deteriorare  de tensiune intrinsecă, componenta 5 BETON_RAG : Deteriorare  de tensiune intrinsecă, componenta 6 BETON_RAG : Deteriorare macroscopică (indicare fisurare)  componenta 1 BETON_RAG : Deteriorare macroscopică (indicare fisurare)  componenta 2 BETON_RAG : Deteriorare macroscopică (indicare fisurare)  componenta 3 BETON_RAG : Deteriorare macroscopică (indicare fisurare)  componenta 4 BETON_RAG : Deteriorare macroscopică (indicare fisurare)  componenta 5 BETON_RAG : Deteriorare macroscopică (indicare fisurare)  componenta 6 BETON_RAG : Deteriorare macroscopică în comprimare Beton_umlv_fp : v21 C-A-S-H CSH Aceasta este o alarmă. Dacă nu înțelegeți sensul acestei
alarme, puteți obține rezultate neașteptate! Schimbare de fonturi Ieșire comandă : Comenzi utilizate Concepte niciodată utilizate Crearea unei căi Crearea unui punct Deformare sferică reversibilă Dependențe EROARE LA VERIFICAREA CATALOGULUI - ÎNTRERUPERE Eroare VISU Salome Eroare construire tabel de valori pentru vizualizare 2D SALOME Eroare în macrocomanda %s
%s Eroare execuție comandă:  Eroare citire fișier MED: Eroare: tip de vizualizare nesuportat Eroare de vizualizare în SALOME Cod ieșire : %d Forța GAZ : v1 Gmsh Gmsh : Versiune fișier HYDR : v1 HYDRENDO : v1 HYDR_UTIL : v1 Hayhurst: variabila PHI Hayhurst : variabilă de întărire H1 Hayhurst : variabilă de întărire H2 Incrementare de timp LIQADGV1 LIQADGV2 LIQADGV3 LIQGATM1 LIQGATM2 LIQGAZ1 LIQGAZ2 LIQSAT1 LIQU_AD_GAZ v1 LIQU_AD_GAZ v2 LIQU_AD_GAZ v3 LIQU_AD_GAZ v4 LIQU_AD_GAZ v5 LIQVAP1 LIQVAP2 LIQVAP3 LIQVG1 LIQVG2 LIQVG3 Numele conceptului %s este prea lung (maxim 8 caractere) Tipurile de intrare nu sunt permise Autentificare Maşină Windows/Samba Mașina Gmsh Mașina Salome Maşină de vizualizare Mod Mod grafic Cuvânt cheie %s nevalid : %s
Criteriu de validare: %s Cuvânt cheie simplu : %s %s Cuvânt cheie necunoscut : %s Numele conceptului este nevalid ('SD_' este rezervat) Numărul de comenzi Numărul de dependenţe Numărul de noduri Numărul de rezultate Număr de rezultate neutilizate Nici una nu este o valoare autorizată OK Opțiuni grafice Paralelism OpenMP : activ
 Numărul de procese utilizate : %(i1)d Parametru pentru Gmsh : versiune fișier rezultat. Parametru pentru Salome: vizualizare în POSTPRO sau PARAVIZ. Nici un nume pentru conceptul întors Port Salome Ieșire Rezultatele Salome : Vizualizator Server de calcul Aster / Stanley Smbclient Timp total VISCOCHAB, arată R5.03.12, variabila internă 3 VISCOCHAB, arată R5.03.12, variabila internă 4 VISCOCHAB, arată R5.03.12, variabila internă 5 VISCOCHAB, arată R5.03.12, variabila internă 6 VISCOCHAB, arată R5.03.12, variabila internă 7 VISCOCHAB, arată R5.03.12, variabila internă 8 VISCOCHAB, arată R5.03.12, variabila internă 9 Validează parametrii și ieși Viteza de disipație mecanică Vreți să ieșiți ? Modificarea parametrilor va fi pierdută. Xmgrace [ OMIS ] [EȘUAT] afm aft după %d temporizări unghi de asamblare, variabila internă 1 unghi de asamblare, variabila internă 2 unghi de asamblare, variabila internă 3 unghi de asamblare, variabila internă 4 unghi de asamblare, variabila internă 5 unghi de asamblare, variabila internă 6 unghi de asamblare, variabila internă 7 fără calciu coeziune comportament GRANGER_FP, arată R7.01.01, variabila internă 1 comportament GRANGER_FP, arată R7.01.01, variabila internă 10 comportament GRANGER_FP, arată R7.01.01, variabila internă 11 comportament GRANGER_FP, arată R7.01.01, variabila internă 12 comportament GRANGER_FP, arată R7.01.01, variabila internă 13 comportament GRANGER_FP, arată R7.01.01, variabila internă 14 comportament GRANGER_FP, arată R7.01.01, variabila internă 15 comportament GRANGER_FP, arată R7.01.01, variabila internă 16 comportament GRANGER_FP, arată R7.01.01, variabila internă 17 comportament GRANGER_FP, arată R7.01.01, variabila internă 18 comportament GRANGER_FP, arată R7.01.01, variabila internă 19 comportament GRANGER_FP, arată R7.01.01, variabila internă 2 comportament GRANGER_FP, arată R7.01.01, variabila internă 20 comportament GRANGER_FP, arată R7.01.01, variabila internă 22 comportament GRANGER_FP, arată R7.01.01, variabila internă 22 comportament GRANGER_FP, arată R7.01.01, variabila internă 23 comportament GRANGER_FP, arată R7.01.01, variabila internă 24 comportament GRANGER_FP, arată R7.01.01, variabila internă 25 comportament GRANGER_FP, arată R7.01.01, variabila internă 26 comportament GRANGER_FP, arată R7.01.01, variabila internă 27 comportament GRANGER_FP, arată R7.01.01, variabila internă 28 comportament GRANGER_FP, arată R7.01.01, variabila internă 29 comportament GRANGER_FP, arată R7.01.01, variabila internă 3 comportament GRANGER_FP, arată R7.01.01, variabila internă 30 comportament GRANGER_FP, arată R7.01.01, variabila internă 31 comportament GRANGER_FP, arată R7.01.01, variabila internă 32 comportament GRANGER_FP, arată R7.01.01, variabila internă 33 comportament GRANGER_FP, arată R7.01.01, variabila internă 34 comportament GRANGER_FP, arată R7.01.01, variabila internă 35 comportament GRANGER_FP, arată R7.01.01, variabila internă 36 comportament GRANGER_FP, arată R7.01.01, variabila internă 37 comportament GRANGER_FP, arată R7.01.01, variabila internă 38 comportament GRANGER_FP, arată R7.01.01, variabila internă 39 comportament GRANGER_FP, arată R7.01.01, variabila internă 4 comportament GRANGER_FP, arată R7.01.01, variabila internă 40 comportament GRANGER_FP, arată R7.01.01, variabila internă 41 comportament GRANGER_FP, arată R7.01.01, variabila internă 42 comportament GRANGER_FP, arată R7.01.01, variabila internă 43 cpu cpu+sys gradul de hidratare grade medii de hidratare disipare termodinamică disipare plastică deformare plastică component XX deformare plastică component XY deformare plastică component XZ deformare plastică component YY deformare plastică component YZ deformare plastică component ZZ Deformare sferică ireversibilă scurs elas_poutre_GR v1 elas_poutre_GR v2 elas_poutre_GR v3 defecțiune normală defecțiune scalară defecțiune tangentă deteriorare termică gonflabil indicator de flambaj indicator de viscoplasticitate indice de vid iradiere lungimea liniei de comandă este mai mare de %d caractere. lista %s numărul elementului indicat numărul 1, numărul elementului indicat numărul 2 (la amorsare), porozitate presiunea critică presiunea fluidului presiune gel presiunea apei rigiditate reziduală salt normal salt tangențial 1 prag prag de compresie prag hidratare prag izotropic sulfat liber sulfați ficși sistem timp de încărcare cumulat temperatura temperatura maximă tensor cinematic, componenta XX tensor cinematic, componenta XY tensor cinematic, componenta ZZ tensor cinematic, componenta YY tensor cinematic, componenta YZ tensor cinematic, componenta ZZ deformare tensor, direcția XX deformare tensor, direcția XY deformare tensor, direcția XZ deformare tensor, direcția YY deformare tensor, direcția YZ deformare tensor, direcția ZZ tip nesuportat valoarea %s este obligatorie valoare în intervalele %s, %s valoare de %s valoare validă variație de volum cumulat variaţie de volum volumul de apă volumul secundar ett elemente discrete,variabila internă 1 elemente discrete, variabila internă 10 elemente discrete, variabila internă 11 elemente discrete, variabila internă 12 elemente discrete, variabila internă 13 elemente discrete, variabila internă 14 elemente discrete, variabila internă 15 elemente discrete, variabila internă 16 elemente discrete, variabila internă 17 elemente discrete, variabila internă 18 elemente discrete, variabila internă 2 elemente discrete, variabila internă 3 elemente discrete, variabila internă 4 elemente discrete, variabila internă 5 elemente discrete, variabila internă 6 elemente discrete, variabila internă 7 elemente discrete, variabila internă 8 elemente discrete, variabila internă 9 energie blocată energie disipată acumulată la fiecare pas pentru CRIT_RUPT energie disipată pentru CRIT_RUPT energie reziduală 