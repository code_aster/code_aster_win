:: created by waf using data/wscript

set PYTHONPATH=%ASTER_ROOT%\14.4\lib\aster;%ASTER_ROOT%\lib\python3.7\site-packages;%PYTHONPATH%

set ASTER_DATADIR=%ASTER_ROOT%\14.4\share\aster
set ASTER_LIBDIR=%ASTER_ROOT%\14.4\lib\aster
set ASTER_LOCALEDIR=%ASTER_ROOT%\14.4\share\locale\aster
set ASTER_ELEMENTSDIR=%ASTER_ROOT%\14.4\lib\aster

set TFELHOME=%ASTER_ROOT%\outils\tfel-3.1.1
set PATH=%TFELHOME%\bin;%PATH%
