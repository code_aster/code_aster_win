#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_agla.tcl 2883 2007-03-20 17:00:02Z courtois $

# liste des versions gérées en conf
#################################################################
proc liste_vers_agla { parent } {
   set pastrouve 1
   catch { destroy $parent.vers }
   set MenuVers [tk_optionMenu $parent.vers astk::profil(version) $astk::agla(verdev,0)]
   $MenuVers configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $MenuVers entryconfigure 0 -font $astk::ihm(font,lablst)
   for {set j 1} {$j < $astk::agla(nb_verdev)} {incr j} {
      $MenuVers add radiobutton
      $MenuVers entryconfigure $j -label $astk::agla(verdev,$j) -font $astk::ihm(font,lablst) -variable astk::profil(version)
      if { $astk::profil(version) == $astk::agla(verdev,$j) } {
         set pastrouve 0
        }
   }
   $parent.vers configure -font $astk::ihm(font,lablst)
   pack $parent.vers -side right
   if { $pastrouve } {
      set astk::profil(version) $astk::agla(verdev,0)
   }
}

# raffraichit le menu/onglet AGLA
#################################################################
proc raffr_ong_agla { } {
   global ongsel
   if { $astk::profil(agla) == "oui" } {
      adapt_satellite 1
      for {set j 2} {$j <= $astk::ihm(nbong,$astk::ihm(nongl_agla)) } {incr j} {
         if { $astk::profil($astk::ihm(ong,$astk::ihm(nongl_agla),$j)) == "oui" } {
            set ongsel($astk::ihm(nongl_agla)) $astk::ihm(tit,$astk::ihm(nongl_agla),$j)
         }
      }
   }
   coche_onglet_agla
}

# fenêtre pour renseigner les modules à denoter
# retourne 0 si ok, 1 si annuler
#################################################################
proc asdeno_ihm { } {
   global iret_asdeno
   set iret_asdeno 0

   set fen .f_asdeno
   catch {destroy $fen}
   set tit [ashare::mess ihm 144]
   toplevel $fen
   wm title $fen $tit

#  type de module, version
   pack [frame $fen.f0 -bd 0] -fill x -anchor nw
      pack [frame $fen.f0.typ -bd 0] -side left
      pack [frame $fen.f0.vers -bd 0] -side left -padx 20
   pack [frame $fen.f1 -bd 0] -anchor nw

   label $fen.f0.typ.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 14] :"
   pack $fen.f0.typ.lbl -padx 5 -pady 3 -side left

   set ltype {f/f90/c py cata capy test}
   set i 0
   set lab [lindex $ltype $i]
   set Menu [tk_optionMenu $fen.f0.typ.liste typ_asdeno $lab]
   $Menu configure \
     -foreground $astk::ihm(couleur,menu_foreground) \
     -background $astk::ihm(couleur,menu_background)
   for {set i 1} {$i < [llength $ltype]} {incr i} {
      $Menu add radiobutton
      $Menu entryconfigure $i -label [lindex $ltype $i] -font $astk::ihm(font,labmenu) -variable typ_asdeno
   }
   pack $fen.f0.typ.liste -padx 5 -pady 3 -side left

   label $fen.f0.vers.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 46] : $astk::profil(version)"
   pack $fen.f0.vers.lbl -padx 5 -pady 3

#  info
   label $fen.f1.lbl  -font $astk::ihm(font,lab) -justify left -text [ashare::mess ihm 126]
   pack $fen.f1.lbl -padx 5 -pady 3

#  texte
   pack [frame $fen.txt -relief solid -bd 0] -anchor c -fill both -expand 1
   text $fen.txt.tx -xscrollcommand "$fen.txt.scrollx set" -yscrollcommand "$fen.txt.scrolly set" \
      -height 32 -width 80 -font $astk::ihm(font,zonfix) -bg "$astk::ihm(couleur,entry_background)" -wrap word
   scrollbar $fen.txt.scrolly -command "$fen.txt.tx yview"
   scrollbar $fen.txt.scrollx -command "$fen.txt.tx xview" -orient h
   pack $fen.txt.scrolly -side right  -fill y
   pack $fen.txt.scrollx -side bottom -fill x
   pack $fen.txt.tx -expand 1 -fill both

#  fermer
   pack [frame $fen.valid -relief solid -bd 0]
   button $fen.valid.ok -font $astk::ihm(font,labbout) -text "Ok" \
      -background $astk::ihm(couleur,valid) \
      -command "asdeno_prof $fen"
   button $fen.valid.can -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
      -background $astk::ihm(couleur,annul) \
      -command "set iret_asdeno 1 ; destroy $fen"
   pack $fen.valid.ok $fen.valid.can -side left -padx 10 -pady 3
   focus $fen.txt.tx

   tkwait window $fen
   return $iret_asdeno
}

# remplit le profil pour asdeno
#################################################################
proc asdeno_prof { fen } {
   global iret_asdeno typ_asdeno
    
#   set ltype {f/c py cata/capy test}
   switch -exact -- $typ_asdeno {
      f/f90/c {
         set var "sources"
         regexp {([-a-z_0-9]+)/} $typ_asdeno mat1 type
      }
      py -
      cata -
      capy {
         set var "sources"
         set type $typ_asdeno
      }
      test {
         set var "tests"
         set type "rep_test"
      }
   }
   set lmod [$fen.txt.tx get 1.0 end]
   set n [llength $lmod]
   for {set i 0} {$i < $n} {incr i} {
      set astk::profil($var,fich,$i,nom)      [lindex $lmod $i]
      set astk::profil($var,fich,$i,serv)     $astk::local_server
      set astk::profil($var,fich,$i,type)     $type
      set astk::profil($var,fich,$i,UL)       0
      set astk::profil($var,fich,$i,donnee)   1
      set astk::profil($var,fich,$i,resultat) 0
      set astk::profil($var,fich,$i,compress) 0
      set astk::profil($var,fich,$i,FR)       "N"
   }
   set astk::profil($var,nbfic) $n
   set iret_asdeno 0
   destroy $fen
}

# active/desactive la partie produits de surcharge si agla
#################################################################
proc raffr_agla_surch { } {
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> raffr_agla_surch : onglet_actif=$astk::profil(onglet_actif)  agla_actif=$astk::profil(agla)"
   }
   if { $astk::profil(agla) == "non" } {
      set etat normal
   } else {
      set etat disabled
   }
#
   set liste $astk::ihm(fenetre).active.prod.lst.f.frame
   set var surcharge
   if { $astk::profil(onglet_actif) == "$var" } {
      liste_fich $liste 0 $var
      for { set i 0 } { $i < $astk::profil($var,nbfic) } { incr i } {
         if { $astk::profil($var,fich,$i,type) == "exec"
           || $astk::profil($var,fich,$i,type) == "cmde"
           || $astk::profil($var,fich,$i,type) == "ele" } {
            $liste.lig($i,nom) configure -state $etat
            $liste.lig($i,type) configure -state $etat
            $liste.lig($i,serv) configure -state $etat
            $liste.lig($i,donnee) configure -state $etat
            $liste.lig($i,resultat) configure -state $etat
         }
      }
   }
}
