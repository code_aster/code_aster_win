#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: init_share.tcl 1195 2006-07-27 16:10:40Z mcourtoi $

# namespace partagé
#################################################################
namespace eval ashare {
   # nom de l'appli : astk, bsf, asjob
   variable appli
   # chemins prefix, root="prefix"/lib/astk
   variable root
   variable prefix
   variable astkrc_ref
   # origine
   variable origine
   # debug
   variable dbg
   # DISPLAY
   variable DISPLAY
   # liste de mots-clés des fichiers (tableau)
   variable mots
   # version des fichiers astkrc
   variable astkrc_version
   # pour aide contextuelle
   variable context
   # langue courante
   variable lang
   # liste des langues disponibles llang(lang,i) et nbre llang(nb_lang)
   variable llang
   # messages (tableau, cf. sdd_msg.txt)
   variable msg
   # fichier des messages
   variable fic_msg
   # fenetre des messages
   variable fen_log
   # fichier des messages
   variable fic_log
   variable pt_log
   # fichier de suivi de la progression
   variable fic_progress
   # permet de supprimer certaines boites de dialogue pendant la phase d'initialisation
   variable show_dialog
   variable noinitwarn
   # splash screen
   variable splash_screen
   variable splash_info
   # attente des process
   variable fini
   variable cycle
   namespace export ashare
}

# valeurs par défaut
set ashare::dbg 0
set ashare::astkrc_version 1.1
set ashare::lang "ENG"
set ashare::fic_msg "locales.txt"
set ashare::origine "stand-alone"

set ashare::show_dialog 1
set ashare::noinitwarn 0

set ashare::splash_screen 1

# enrobage de l'exit
#################################################################
proc ashare::my_exit { iret { ignore_run 0 } } {
   set end [clock format [clock seconds] -format "%d/%m/%Y - %H:%M:%S"]
   ashare::log "<INFO> Session ended : $end"
#
   ashare::log "EXIT CODE : $iret"
# fermeture du fichier log
   if { $ashare::fen_log } {
      close $ashare::pt_log
   }
   exit $iret
}
