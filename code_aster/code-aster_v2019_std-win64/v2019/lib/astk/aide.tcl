#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: aide.tcl 3255 2008-04-10 17:13:17Z courtois $

# reaction a Aide/xxx (n : indice ihm de l'item)
# exception pour RELNOTES : indice=241
#################################################################
proc aff_aide { parent n } {
   set except 241
   set fen .f_aide_$n
   if { $n != $except } {
      set tit "[ashare::mess ihm 105] - [ashare::mess ihm $n]"
      set fich [file join $ashare::root HELP $ashare::lang item_$n.txt]
   } else {
      set tit "[ashare::mess ihm $n]"
      set fich [file join $ashare::root RELNOTES.$ashare::lang]
   }

   if { $n == $except && $astk::config(-1,browser) != "" } {
      set uri [file join $ashare::root $fich.html]
      ashare::file_open_url $uri
      set msg [ashare::mess ihm 425]
      tk_messageBox -message $msg -type ok -icon info
      return
   }
   catch {destroy $fen}
   toplevel $fen
   wm title $fen $tit

#  titre
   pack [frame $fen.titre -relief solid -bd 0] -anchor c
   label $fen.titre.lbl -font $astk::ihm(font,tit) -text $tit -anchor w
   pack $fen.titre.lbl -padx 20 -pady 10 -side left

#  texte
   pack [frame $fen.txt -relief solid -bd 0] -anchor c -fill both -expand 1
   text $fen.txt.tx -xscrollcommand "$fen.txt.scrollx set" -yscrollcommand "$fen.txt.scrolly set" \
      -height 32 -width 80 -font $astk::ihm(font,txt) -bg $astk::ihm(couleur,entry_background) -wrap word
   scrollbar $fen.txt.scrolly -command "$fen.txt.tx yview"
   scrollbar $fen.txt.scrollx -command "$fen.txt.tx xview" -orient h
   pack $fen.txt.scrolly -side right  -fill y
   pack $fen.txt.scrollx -side bottom -fill x
   pack $fen.txt.tx -expand 1 -fill both

# remplissage du texte
   #$fen.txt.tx delete 1 end
   if { [file exists $fich] } {
      set id [open $fich r]
      $fen.txt.tx insert end [read $id [file size $fich]]
      close $id
   } else {
      $fen.txt.tx insert end [ashare::mess ihm 1]
      $fen.txt.tx insert end "\n"
      $fen.txt.tx insert end [ashare::mess info 4 $fich]
   }
   $fen.txt.tx configure -state disabled

#  fermer
   pack [frame $fen.valid -relief solid -bd 0]
   button $fen.valid.ok -font $astk::ihm(font,labbout) -text "Ok" \
      -background $astk::ihm(couleur,valid) \
      -command "destroy $fen"
   pack $fen.valid.ok -padx 10 -pady 3
}

# envoi d'un mail à l'assistance téléphonique
#################################################################
proc contact_ata { } {
# verif
   if { $astk::agla(num_serv) < 0 || $astk::agla(mail_ata) == "" } {
      set msg [ashare::mess ihm 426]
      tk_messageBox -message $msg -type ok -icon info
      return
   }

# fenetre texte
   set fen .fen_about
   set parent .
   catch {destroy $fen}
   toplevel $fen
   wm title $fen "[ashare::mess ihm 105] - [ashare::mess ihm 269]"
   wm transient $fen $parent
   grab set $fen

   # entete
   pack [frame $fen.f1 -relief flat -bd 0] -fill both -expand yes -padx 5 -pady 5
   label $fen.f1.em -font $astk::ihm(font,labbout) -text "Envoi d'un message à l'assistance technique" -anchor w
   pack $fen.f1.em -fill x -expand yes

   label $fen.f1.co1 -font $astk::ihm(font,lab) -text "Email de l'assistance : $astk::agla(mail_ata)" -anchor w
   pack $fen.f1.co1 -fill x -expand yes

   label $fen.f1.tx -font $astk::ihm(font,labpt) -text "N'oubliez pas de préciser vos coordonnées dans le message." -anchor c
   pack $fen.f1.tx -fill x -expand yes

   # texte
   pack [frame $fen.f2 -relief flat -bd 0] -fill both -expand yes -padx 5 -pady 5

   text $fen.f2.tx -xscrollcommand "$fen.f2.scrollx set" -yscrollcommand "$fen.f2.scrolly set" \
   -height 20 -width 80 -font $astk::ihm(font,zonfix) -bg "$astk::ihm(couleur,entry_background)" -wrap word
   scrollbar $fen.f2.scrolly -command "$fen.f2.tx yview"
   scrollbar $fen.f2.scrollx -command "$fen.f2.tx xview" -orient h
   pack $fen.f2.scrolly -side right  -fill y
   pack $fen.f2.scrollx -side bottom -fill x
   pack $fen.f2.tx -expand 1 -fill both

   # ok / annuler
   pack [frame $fen.valid -relief solid -bd 0] -pady 15
      button $fen.valid.annuler -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
         -command "destroy $fen" -bg $astk::ihm(couleur,annul)
      button $fen.valid.ok -font $astk::ihm(font,labbout) -text [ashare::mess ihm 215] \
         -command "ata_envoi $fen" -bg $astk::ihm(couleur,valid)
      pack $fen.valid.ok $fen.valid.annuler -side left -padx 10 -pady 5
}

# envoi du mail à l'assistance
#################################################################
proc ata_envoi { parent } {
   set fmail [get_tmpname .file_mail_ata]
   set id [open $fmail w]
   puts $id [$parent.f2.tx get 1.0 end]
   close $id

# préparation de la ligne de commande à exécuter
   set lcmd ""
   set argu ""
   set serv $astk::agla(num_serv)
   set ftmp [get_tmpname .sendmail_export]
   set idexp [open $ftmp w]
   write_server_infos $idexp $serv
   close $idexp
   append lcmd [file join $ashare::prefix "bin" as_run]
   append lcmd " --proxy --sendmail --schema=[get_schema $serv sendmail]"
   append [ashare::get_glob_args]
   append argu "--report_to=$astk::agla(mail_ata) "
   append argu $ftmp
   append argu $fmail
#  execution
   set iret [ ashare::rexec_cmd -1 astk::config $lcmd $argu 0 out $parent progress]
   if { $iret == 0 } {
      set msg [ashare::mess ihm 270]
      change_status $msg
   } else {
      ashare::mess "erreur" 3 "as_mail" $iret $out
      set msg [ashare::mess ihm 60 $iret]
      change_status $msg
      tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info -parent $parent
   }

   # ferme la fenetre d'emission
   destroy $parent
}

# reaction a Aide/A propos
#################################################################
proc a_propos { parent {msg ""}} {
   set fen .fen_about
   catch {destroy $fen}
   toplevel $fen
   if { $msg == "" } {
      wm title $fen "[ashare::mess ihm 105] - [ashare::mess ihm 106]"
    wm transient $fen $parent
      grab set $fen
    } else {
      wm title $fen $msg
   }
   wm geometry $fen "+300+200"

#  titre
   pack [frame $fen.titre -relief raised -bd 1] -anchor c -fill x -expand 1
   label $fen.titre.lbl -font $astk::ihm(font,tit) -text [ashare::mess ihm 107 $astk::astk_version] -anchor w
   label $fen.titre.mod -font $astk::ihm(font,txt) -text "(mode $ashare::origine)" -anchor w
   pack $fen.titre.lbl $fen.titre.mod -padx 20 -pady 10

#  texte
   pack [frame $fen.txt -relief raised -bd 1] -anchor c -fill x -expand 1
   label $fen.txt.bla -font $astk::ihm(font,txt) -text [ashare::mess ihm 108] -anchor w
   pack $fen.txt.bla -padx 20 -pady 10

#  fermer
   pack [frame $fen.valid -relief solid -bd 0]
   if { $msg == "" } {
      button $fen.valid.ok -font $astk::ihm(font,labbout) -text "Ok" \
         -background $astk::ihm(couleur,valid) \
         -command "destroy $fen ; grab release $fen"
   } else {
      set ashare::splash_info $msg
      label $fen.valid.ok -font $astk::ihm(font,labbout) -textvariable ashare::splash_info
   }
   pack $fen.valid.ok -padx 10 -pady 3
    wm deiconify $fen
}

# affiche une info dans la fenetre a_propos (aussi splash screen)
#################################################################
proc ShowSplashInfo { msg } {
   set ashare::splash_info $msg
   update idletasks
}
