#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: options.tcl 3530 2008-09-26 08:45:55Z courtois $

# reaction a Option/Configuration
#################################################################
proc Opt_conf { } {
   global serv_conf last_CS_serv
   global old_conf old_inv

# sauvegarde le tableau config avant modif
   copie_tabl astk::config old_conf 1
   copie_tabl astk::inv old_inv 1
   set fen .fen_conf
   set serv_conf $astk::profil(serveur)
   if { $ashare::dbg >= 3 } {
      ashare::log "<DEBUG> Opt_conf : serv_conf = $serv_conf"
   }

   catch {destroy $fen}
   toplevel $fen
   wm withdraw $fen
   wm title $fen "[ashare::mess ihm 33] - [ashare::mess ihm 35]"
   wm transient $fen .
   grab set $fen

   Opt_conf_list $fen

   wm deiconify $fen
}

# recréer la liste des serveurs
#################################################################
proc Opt_conf_list { fen } {
   global serv_conf last_CS_serv

   catch {destroy $fen.haut}
   catch {destroy $fen.liste}
   catch {destroy $fen.bas}

   # le 1er coup ET quand on veut recréer la liste, il faut raffraichir
   set last_CS_serv -1

   pack [frame $fen.haut -relief solid -bd 0] -fill x -expand yes
   pack [frame $fen.liste -relief solid -bd 1]
   pack [frame $fen.bas -relief solid -bd 0] -fill both -expand yes

   pack [frame $fen.haut.choix_serv -relief solid -bd 0] -side left -fill x -expand yes
   if { $astk::config(nb_serv) > 0 } {
      set MenuOptNoeud [tk_optionMenu $fen.haut.choix_serv.noeud serv_conf $astk::config(0,nom)]
      $MenuOptNoeud entryconfigure 0 -font $astk::ihm(font,labmenu) -command "Opt_conf_serv $fen.liste $astk::config(0,nom)"
      for {set j 1} {$j < $astk::config(nb_serv)} {incr j} {
         $MenuOptNoeud add radiobutton
         $MenuOptNoeud entryconfigure $j -label $astk::config($j,nom) -font $astk::ihm(font,labmenu) -variable serv_conf -command "Opt_conf_serv $fen.liste $astk::config($j,nom)"
      }
      $MenuOptNoeud add radiobutton
      $MenuOptNoeud entryconfigure $j -label [ashare::mess ihm 70] -font $astk::ihm(font,labmenu) -variable serv_conf -command "add_serv $fen $MenuOptNoeud"
   } else {
      set MenuOptNoeud [tk_optionMenu $fen.haut.choix_serv.noeud serv_conf [ashare::mess ihm 70]]
      $MenuOptNoeud entryconfigure 0 -font $astk::ihm(font,labmenu) -command "add_serv $fen $MenuOptNoeud"
   }
   $MenuOptNoeud configure \
      -foreground $astk::ihm(couleur,menu_foreground) \
      -background $astk::ihm(couleur,menu_background)
#
   $fen.haut.choix_serv.noeud configure -font $astk::ihm(font,labmenu) -bg $astk::ihm(couleur,liste)
   pack $fen.haut.choix_serv.noeud
   Opt_conf_serv $fen.liste $serv_conf
# aide
   button $fen.haut.help -image [image create photo -file $astk::icon(help)] \
      -bg $astk::ihm(couleur,background) -bd 0 \
      -command "grab release $fen ; aff_aide . 190"
   pack $fen.haut.help -anchor ne -side right

   pack [frame $fen.bas.valid -relief solid -bd 0] -side left -fill both -expand yes
      pack [frame $fen.bas.valid.m -relief solid -bd 0]
      button $fen.bas.valid.m.annuler -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
         -command "annule_mod_serv $fen" -bg $astk::ihm(couleur,annul)
      button $fen.bas.valid.m.ok  -font $astk::ihm(font,labbout) -text "Ok" \
         -bg $astk::ihm(couleur,valid) \
         -command "maj_sat ; raffr_agla $fen ; ashare::save_options; destroy $fen ; grab release $fen"
      pack $fen.bas.valid.m.ok $fen.bas.valid.m.annuler -side left -padx 10 -pady 5

   pack [frame $fen.bas.del -relief solid -bd 0] -side right
   button $fen.bas.del.bout -font $astk::ihm(font,labbout) -text [ashare::mess ihm 34] \
      -command "del_serv $fen" -bg $astk::ihm(couleur,suppr)
   pack $fen.bas.del.bout -side left -padx 10 -pady 5
}

#################################################################
proc Opt_conf_serv {fen nserv} {
   global serv_conf last_CS_serv rec_bid

# si pas de chgt, on sort
   set mod 0
   catch { if { $nserv == $last_CS_serv } { set mod 1 } }
   if { $mod } { return }
   set last_CS_serv $nserv

   catch {destroy $fen.f}
   pack [frame $fen.f -relief raised -bd 0]

   # indices du serveur
   set serv $astk::inv(serv,$nserv)

   # blindage : on ne doit pas arriver avec serv=-1 (Local)
   if { $serv < 0 } {
      return
      set serv 0
      set serv_conf $astk::config($serv,nom)
      set last_CS_serv $serv_conf
   }

# données modifiables par l'utilisateur
# nom_complet
   pack [frame $fen.f.nom -relief solid -bd 0] -anchor w
   label $fen.f.nom.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 79] -width 35 -anchor w
   entry $fen.f.nom.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config($serv,nom_complet)
   pack $fen.f.nom.lbl $fen.f.nom.path -pady 3 -side left
# interaction avec l'AGLA
   if { $serv == $astk::agla(num_serv) } {
      label $fen.f.lblagla -font $astk::ihm(font,labpt) -text [ashare::mess ihm 128]
      pack $fen.f.lblagla -anchor w
   }
# local ?
   pack [frame $fen.f.local -relief solid -bd 0] -anchor w -padx 20
   checkbutton $fen.f.local.cb -variable astk::config($serv,islocal) -onvalue oui -offvalue non -anchor w
   label  $fen.f.local.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 364]"
   pack $fen.f.local.cb $fen.f.local.lbl -pady 3 -side left

# etat du serveur
   pack [frame $fen.f.etat -relief solid -bd 0] -anchor w
   label $fen.f.etat.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 135] -width 35 -anchor w
   radiobutton $fen.f.etat.active -font $astk::ihm(font,lab) -text on -value on -variable astk::config($serv,etat)
   radiobutton $fen.f.etat.inactive -font $astk::ihm(font,lab) -text off -value off -variable astk::config($serv,etat)
   pack $fen.f.etat.lbl $fen.f.etat.active $fen.f.etat.inactive -pady 3 -side left

# login
   pack [frame $fen.f.login -relief solid -bd 0] -anchor w
   label $fen.f.login.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 80] -width 35 -anchor w
   entry $fen.f.login.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config($serv,login)
   pack $fen.f.login.lbl $fen.f.login.path -pady 3 -side left

# home
   pack [frame $fen.f.home -relief solid -bd 0] -anchor w
   label $fen.f.home.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 84] -width 35 -anchor w
   entry $fen.f.home.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config($serv,home)
   pack $fen.f.home.lbl $fen.f.home.path -pady 3 -side left

# rep_serv
   pack [frame $fen.f.trav -relief solid -bd 0] -anchor w
   label $fen.f.trav.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 81] -width 35 -anchor w
   entry $fen.f.trav.exe -width 30 -font $astk::ihm(font,val) -textvariable astk::config($serv,rep_serv)
   pack $fen.f.trav.lbl $fen.f.trav.exe -pady 3 -side left
   button $fen.f.trav.ouvrir -image [image create photo -file $astk::icon(openR)] \
      -command "get_rep_serv $serv" -bg $astk::ihm(couleur,background) -bd 0
   pack $fen.f.trav.ouvrir -pady 3 -padx 5 -side right

# recup - last_recup
   pack [frame $fen.f.recup -relief solid -bd 0] -anchor w
   label $fen.f.recup.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 71] -width 35 -anchor w
   set rval(0) "auto"
   set rval(1) "manu"
   set rval(2) "none"
   set rlist(0) [ashare::mess ihm 73]
   set rlist(1) [ashare::mess ihm 74]
   set rlist(2) [ashare::mess ihm 75]
   for {set j 0} {$j < 3} {incr j} {
      if { $astk::config($serv,recup) == $rval($j) } {
         set rec_bid($serv) $rlist($j)
         break
      }
   }
   set MenuRec [tk_optionMenu $fen.f.recup.ch rec_bid($serv) $rlist(0)]
   $MenuRec configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $MenuRec entryconfigure 0 -font $astk::ihm(font,labmenu) -command "set astk::config($serv,recup) $rval(0)"
   for {set j 1} {$j < 3} {incr j} {
      $MenuRec add radiobutton
      $MenuRec entryconfigure $j -label $rlist($j) -font $astk::ihm(font,labmenu) -variable rec_bid($serv) -command "set astk::config($serv,recup) $rval($j)"
   }
   $fen.f.recup.ch configure -font $astk::ihm(font,labmenu) -bg $astk::ihm(couleur,liste)
   pack $fen.f.recup.lbl $fen.f.recup.ch -pady 3 -side left

   pack [frame $fen.f.last_recup -relief solid -bd 0] -anchor w
   set vlrec $astk::config($serv,last_recup)
   set iret [regexp {([0-9]+)/([0-9]+)/([0-9]+)} $vlrec mat1 mm jj YY]
   set vlrec [ashare::mess ihm 72]
   append vlrec " : $jj/$mm/$YY"
   label $fen.f.last_recup.lbl -font $astk::ihm(font,lab) -text $vlrec -width 35
   button $fen.f.last_recup.but -font $astk::ihm(font,labbout) -text [ashare::mess ihm 78] \
      -command "recup_action $serv $fen" -bg $astk::ihm(couleur,lancement)
   pack $fen.f.last_recup.lbl $fen.f.last_recup.but -pady 1 -side left

# xterm
   pack [frame $fen.f.xterm -relief solid -bd 0] -anchor w
   label $fen.f.xterm.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 82] -width 35 -anchor w
   entry $fen.f.xterm.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config($serv,xterm)
   pack $fen.f.xterm.lbl $fen.f.xterm.path -pady 3 -side left

# editeur
   pack [frame $fen.f.editeur -relief solid -bd 0] -anchor w
   label $fen.f.editeur.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 83] -width 35 -anchor w
   entry $fen.f.editeur.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config($serv,editeur)
   pack $fen.f.editeur.lbl $fen.f.editeur.path -pady 3 -side left

# valeurs récupérées
# plate-forme
   pack [frame $fen.f.pltf -relief solid -bd 0] -anchor w
   label $fen.f.pltf.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 130] -width 35 -anchor w
   set lab [ashare::mess ihm 62]
   if { $astk::config($serv,plate-forme) != "" } {
      catch {set lab  $astk::ihm(pltf,$astk::config($serv,plate-forme))}
   }
   label $fen.f.pltf.path -font $astk::ihm(font,lab) -text $lab
   pack $fen.f.pltf.lbl $fen.f.pltf.path -pady 3 -side left

# liste des versions
   set lvers ""
   set n1 [expr $astk::config($serv,nb_vers) - 1]
   if { $n1 >= 0 } {
      set len 0
      for {set i 0} {$i < $astk::config($serv,nb_vers)} {incr i} {
         append lvers $astk::config($serv,vers,$i)
         incr len [string length $astk::config($serv,vers,$i)]
         if { $i != $n1 } {
            append lvers ", "
         }
         if { $len > 50 } {
             append lvers "\n"
             set len 0
         }
      }
      pack [frame $fen.f.vers -relief solid -bd 0] -anchor w
      label $fen.f.vers.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 76] -width 35 -anchor w
      label $fen.f.vers.lv -font $astk::ihm(font,lab) -text $lvers -justify left
      pack $fen.f.vers.lbl $fen.f.vers.lv -pady 3 -side left
   }

# liste des noeuds
   set lnod ""
   set n1 [expr $astk::config($serv,nb_noeud) - 1]
   if { $n1 >= 0 } {
      for {set i 0} {$i < $astk::config($serv,nb_noeud)} {incr i} {
         append lnod $astk::config($serv,noeud,$i)
         if { $i != $n1 } {
            append lnod ", "
         }
      }
      pack [frame $fen.f.nod -relief solid -bd 0] -anchor w
      label $fen.f.nod.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 77] -width 35 -anchor w
      label $fen.f.nod.lv -font $astk::ihm(font,lab) -text $lnod
      pack $fen.f.nod.lbl $fen.f.nod.lv -pady 3 -side left
   }

# batch
   pack [frame $fen.f.batch -relief solid -bd 0] -anchor w
   label $fen.f.batch.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 51] -width 35
   radiobutton $fen.f.batch.act -font $astk::ihm(font,lab) -text on -value oui -variable astk::config($serv,batch) -state disabled
   radiobutton $fen.f.batch.inact -font $astk::ihm(font,lab) -text off -value non -variable astk::config($serv,batch) -state disabled
   pack $fen.f.batch.lbl $fen.f.batch.act $fen.f.batch.inact -pady 3 -side left

   if { $astk::config($serv,batch) == "oui" } {
      if { $astk::config($serv,batch_queue_group) != "" } {
         pack [frame $fen.f.bagrp -relief solid -bd 0] -anchor w
         label $fen.f.bagrp.lb0 -text " "  -width 35 -anchor w
         label $fen.f.bagrp.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 374 $astk::config($serv,batch_queue_group)]
         pack $fen.f.bagrp.lb0 $fen.f.bagrp.lbl -pady 3 -side left
      }
      pack [frame $fen.f.batps -relief solid -bd 0] -anchor w
      label $fen.f.batps.lb0 -text " "  -width 35 -anchor w
      label $fen.f.batps.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 57 $astk::config($serv,batch_tpsmax)]
      pack $fen.f.batps.lb0 $fen.f.batps.lbl -pady 3 -side left
      pack [frame $fen.f.bamem -relief solid -bd 0] -anchor w
      label $fen.f.bamem.lb0 -text " "  -width 35 -anchor w
      label $fen.f.bamem.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 59 $astk::config($serv,batch_memmax)]
      pack $fen.f.bamem.lb0 $fen.f.bamem.lbl -pady 3 -side left
      pack [frame $fen.f.banbp -relief solid -bd 0] -anchor w
      label $fen.f.banbp.lb0 -text " "  -width 35 -anchor w
      label $fen.f.banbp.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 55 $astk::config($serv,batch_nbpmax)]
      pack $fen.f.banbp.lb0 $fen.f.banbp.lbl -pady 3 -side left
   }
# interactif
   pack [frame $fen.f.interactif -relief solid -bd 0] -anchor w
   label $fen.f.interactif.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 52] -width 35
   radiobutton $fen.f.interactif.act -font $astk::ihm(font,lab) -text on -value oui -variable astk::config($serv,interactif) -state disabled
   radiobutton $fen.f.interactif.inact -font $astk::ihm(font,lab) -text off -value non -variable astk::config($serv,interactif) -state disabled
   pack $fen.f.interactif.lbl $fen.f.interactif.act $fen.f.interactif.inact -pady 3 -side left

   if { $astk::config($serv,interactif) == "oui" } {
      pack [frame $fen.f.intps -relief solid -bd 0] -anchor w
      label $fen.f.intps.lb0 -text " "  -width 35 -anchor w
      label $fen.f.intps.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 57 $astk::config($serv,interactif_tpsmax)]
      pack $fen.f.intps.lb0 $fen.f.intps.lbl -pady 3 -side left
      pack [frame $fen.f.inmem -relief solid -bd 0] -anchor w
      label $fen.f.inmem.lb0 -text " "  -width 35 -anchor w
      label $fen.f.inmem.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 59 $astk::config($serv,interactif_memmax)]
      pack $fen.f.inmem.lb0 $fen.f.inmem.lbl -pady 3 -side left
      pack [frame $fen.f.innbp -relief solid -bd 0] -anchor w
      label $fen.f.innbp.lb0 -text " "  -width 35 -anchor w
      label $fen.f.innbp.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 55 $astk::config($serv,interactif_nbpmax)]
      pack $fen.f.innbp.lb0 $fen.f.innbp.lbl -pady 3 -side left
   }
}

# annule les modifs serveur
#################################################################
proc annule_mod_serv { fen } {
   global old_conf old_inv
   set mod 0
   # vérifie que l'on n'a pas ajouté de serveur
   if { $old_conf(nb_serv) != $astk::config(nb_serv) } {
      set mod 1
   } else {
      set lmv [array get old_conf]
      set nbl [expr [llength $lmv] / 2]
      set lm00 [array names astk::config]
      for {set i 0} {$i < $nbl} {incr i} {
         set k  [expr $i * 2]
         set k1 [expr $k + 1]
         if { [lsearch $lm00 [lindex $lmv $k]] > -1
           && $astk::config([lindex $lmv $k]) != [lindex $lmv $k1] } {
            set mod 1
            if { $ashare::dbg >= 5 } {
               ashare::log "<DEBUG> (annule_mod_serv) [lindex $lmv $k] old=[lindex $lmv $k1] ; new=$astk::config([lindex $lmv $k])"
            }
            break
         }
      }
   }
   # est-ce qu'il y a eu des modifs
   if { $mod } {
   # demande confirmation
      set iret [ tk_messageBox -message [ashare::mess ihm 88] \
         -title [ashare::mess ihm 143] -type yesno -icon question ]
      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (annule_mod_serv) choix : $iret"
      }
      if { $iret == "yes" } {
         set mod 0
      }
   }
   # on sort
   if { $mod == 0 } {
      copie_tabl old_conf astk::config 1
      copie_tabl old_inv astk::inv 1
      destroy $fen
      grab release $fen
   } else {
      raise $fen
   }
}

# selection du rep_serv
#################################################################
proc get_rep_serv { serv } {
   set iret [ashare::selecteur nserv dir type $serv $astk::config($serv,home) "R"]
   if { $iret == 0 } {
      set astk::config($serv,rep_serv) $dir
   }
}

# récupération les infos d'un serveur
#################################################################
proc recup_action { serv fen } {
   global last_CS_serv
   if { $astk::config($serv,etat) == "on" } {
      recup_info $fen $serv
      if { $serv != $astk::agla(num_serv) } {
         raffr_agla
      }
      cree_liste_machines
      set last_CS_serv -1
      Opt_conf_serv $fen $astk::config($serv,nom)
   }
}

# ajoute un serveur
# fen : fenetre parent
#################################################################
proc add_serv { fen men } {
   global serv_conf last_CS_serv
   global getValue_val
   # retire le focus au parent
   grab release $fen

   set new [expr $astk::config(nb_serv)]
   list valint
   for {set i 0} {$i<$new} {incr i} {
      lappend valint $astk::config($i,nom)
   }
   lappend valint $astk::local_server
   getValue_fen $fen 70 95 $valint "" non_vide
   tkwait window .fen_getv
   grab set $fen
# valide le nom du serveur choisi
   set valid 1
   if { $getValue_val == "_VIDE" || $getValue_val == "" } {
      set valid 0
   }
   if { $valid } {
      set astk::config($new,nom) $getValue_val
      incr astk::config(nb_serv)
      # initialisation
      # valeurs obligatoires
      set nbm [llength $ashare::mots(MCS_serv)]
      for {set i 0} {$i<$nbm} {incr i} {
         set mcle [lindex $ashare::mots(MCS_serv) $i]
         set val ""
         # quelques valeurs particulières
         if { $mcle == "etat" } {
            set val "on"
         } elseif { $mcle == "recup" } {
            set val "manu"
         } elseif { $mcle == "last_recup" } {
            set val "01/01/00"
         }
         set astk::config($new,$mcle) $val
      }
      # paramètres optionnels
      set astk::config($new,nb_vers)  0
      set astk::config($new,nb_noeud) 0
      for { set im 0 } { $im < [ llength $ashare::mots(MCS_serv_opt) ] } { incr im } {
         set mcs [lindex $ashare::mots(MCS_serv_opt) $im]
         if { $mcs != "vers" & $mcs != "noeud" } {
            set astk::config($new,$mcs) ""
         }
      }
      # en attendant l'appel à cree_liste_machines
      set astk::inv(serv,$astk::config($new,nom)) $new
      # nom dans le menu
      set serv_conf $astk::config($new,nom)
      Opt_conf_list $fen
      raise $fen
   } else {
      set serv_conf $last_CS_serv
      Opt_conf_serv $fen $serv_conf
      raise $fen
   }
}

# supprime un serveur
#################################################################
proc del_serv { fen } {
   global serv_conf
   set serv $astk::inv(serv,$serv_conf)
# demande confirmation
   set iret [tk_messageBox -message [ashare::mess ihm 98] \
      -title [ashare::mess ihm 99] -type yesno -icon question ]
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (del_serv) choix : $iret"
   }
   if { $iret != "yes" } {
      raise $fen
      return 1
   }
# interaction avec l'agla
   if { $astk::agla(status) == "on" } {
      set agla_off 0
      # destruction de la machine de développement
      if { $serv == $astk::agla(num_serv) } {
         set iret [ tk_messageBox -message [ashare::mess ihm 100] \
            -title [ashare::mess ihm 99] -type yesno -icon question ]
         if { $iret != "yes" } {
            raise $fen
            return 1
         }
         set agla_off 1
      }
      # si on détruit un serveur avant la machine de développement, il faut décaler
      if { $serv < $astk::agla(num_serv) } {
         incr astk::agla(num_serv) -1
         if { $ashare::dbg >= 5 } {
            ashare::log "<DEBUG> (del_serv) Suppr serv avant mach_dev"
         }
      }
# faut-il desactiver l'AGLA
      if { $agla_off } {
         set astk::agla(status) off
         init_onglet "DETR"
      }
   }
# liste des mots-clés + nom (<-->serveur)
   set mots_cles "nom"
   append mots_cles $ashare::mots(MCS_serv)
   append mots_cles $ashare::mots(MCS_serv_opt)
   set nbm [llength $mots_cles]
# suppression du serveur
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (del_serv) Suppr $astk::config($serv,nom) (numero $serv)"
   }
   unset astk::inv(serv,$astk::config($serv,nom))
# décalage
   incr astk::config(nb_serv) -1
   for { set j $serv } { $j < $astk::config(nb_serv) } {incr j} {
      set j1 [expr $j + 1]
      if { $ashare::dbg >= 5 } {
         ashare::log "<DEBUG> (del_serv) Copie $astk::config($j1,nom) de $j1 en $j"
      }
      # tabl inverse
      set astk::inv(serv,$astk::config($j1,nom)) $j
      for { set k 0 } { $k < $nbm } { incr k } {
         set mcs [lindex $mots_cles $k]
         if { $mcs == "vers" || $mcs == "noeud" } {
            for { set l 0 } { $l < $astk::config($j1,nb_$mcs) } { incr l } {
               set val ""
               # catch pour les mcles facultatifs
               catch {set val $astk::config($j1,$mcs,$l)}
               set astk::config($j,$mcs,$l) $val
            }
            set astk::config($j,nb_$mcs) $astk::config($j1,nb_$mcs)
         } else {
            set val ""
            # catch pour les mcles facultatifs
            catch {set val $astk::config($j1,$mcs)}
            set astk::config($j,$mcs) $val
         }
      }
   }
   # suppression du dernier serveur
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (del_serv) Suppr serveur $j"
   }
# ATTENTION : astk::config contient encore des traces de "$j,..." après !?
#    array unset astk::config "$j,*"
   for { set k 0 } { $k < $nbm } { incr k } {
      set mcs [lindex $mots_cles $k]
      if { $mcs == "vers" || $mcs == "noeud" } {
         for { set l 0 } { $l < $astk::config($j,nb_$mcs) } { incr l } {
            # catch pour les mcles facultatifs
            catch {unset astk::config($j,$mcs,$l)}
         }
         unset astk::config($j,nb_$mcs)
      } else {
         # catch pour les mcles facultatifs
         catch { unset astk::config($j,$mcs)}
      }
   }
   set serv_conf $astk::config(0,nom)
   Opt_conf_list $fen
   raise $fen
   return 0
}
