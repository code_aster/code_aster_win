#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_execution.tcl 653 2004-09-24 14:44:45Z mcourtoi $

# Onglet EXECUTION
#################################################################
proc affiche_surcharge {} {
   global tk_version
   set use_panedwindow 0
   if { $tk_version < 8.4 } {
      set use_panedwindow 0
   }

   set act $astk::ihm(fenetre).active

   if { $use_panedwindow } {
      pack [panedwindow $act -orient vertical] -anchor nw -fill both -expand 1
      $act add [frame $act.haut] -sticky nsew

   # frame bouton et chemin de base
      grid [frame $act.haut.base -relief raised -bd 1] -row 0 -column 0 -sticky new -padx 3 -pady 3
         fenEEC2 $act.haut.base [ashare::mess ihm 40]
         pack [frame $act.haut.base.basepath -bd 0] -padx 3 -pady 0 -fill x

   # frame liste des fichiers et icones
    grid [frame $act.haut.src -relief raised -bd 1] -row 1 -column 0 -sticky nsew -padx 3 -pady 3

         set sw [ScrolledWindow $act.haut.src.lst]
         set sf [ScrollableFrame $sw.f]
         $sw setwidget $sf
         set liste [$sf getframe]
         $sf configure -constrainedwidth 1
         # liste = ....active.src.lst.f.frame
         grid $sw -row 0 -column 0 -sticky nsew
         grid [frame $act.haut.src.icone] -row 0 -column 1 -sticky nw
         grid rowconfigure    $act.haut.src 0 -weight 1
         grid columnconfigure $act.haut.src 0 -weight 1

      grid rowconfigure    $act.haut 1 -weight 1
      grid columnconfigure $act.haut 0 -weight 1

      chemin_base $act.haut.base.basepath $liste sources
      liste_fich  $liste -1 sources
      liste_icone $act.haut.src.icone $liste sources

      $act add [frame $act.bas] -sticky nsew
   # frame bouton
      grid [frame $act.bas.base2 -relief raised -bd 1] -row 0 -column 0 -sticky new -padx 3 -pady 3
         fenEEC2 $act.bas.base2 [ashare::mess ihm 41]

   # frame liste des fichiers et icones
    grid [frame $act.bas.prod -relief raised -bd 1] -row 1 -column 0 -sticky nsew -padx 3 -pady 3

         set sw2 [ScrolledWindow $act.bas.prod.lst]
         set sf2 [ScrollableFrame $sw2.f]
         $sw2 setwidget $sf2
         set liste2 [$sf2 getframe]
         $sf2 configure -constrainedwidth 1
         # liste = ....active.prod.lst.f.frame
         grid $sw2 -row 0 -column 0 -sticky nsew
         grid [frame $act.bas.prod.icone] -row 0 -column 1 -sticky nw
         grid rowconfigure    $act.bas.prod 0 -weight 1
         grid columnconfigure $act.bas.prod 0 -weight 1

      grid rowconfigure    $act.bas 1 -weight 1
      grid columnconfigure $act.bas 0 -weight 1

      liste_fich  $liste2 -1 surcharge
      liste_icone $act.bas.prod.icone $liste2 surcharge

   } else {

      pack [frame $act -relief raised -bd 1] -anchor nw -fill both -expand 1

   # frame bouton et chemin de base
      grid [frame $act.base -relief raised -bd 1] -row 0 -column 0 -sticky new -padx 3 -pady 3
         fenEEC2 $act.base [ashare::mess ihm 40]
         pack [frame $act.base.basepath -bd 0] -padx 3 -pady 0 -fill x

   # frame liste des fichiers et icones
    grid [frame $act.src -relief raised -bd 1] -row 1 -column 0 -sticky nsew -padx 3 -pady 3

         set sw [ScrolledWindow $act.src.lst]
         set sf [ScrollableFrame $sw.f]
         $sw setwidget $sf
         set liste [$sf getframe]
         $sf configure -constrainedwidth 1
         # liste = ....active.src.lst.f.frame
         grid $sw -row 0 -column 0 -sticky nsew
         grid [frame $act.src.icone] -row 0 -column 1 -sticky nw
         grid rowconfigure    $act.src 0 -weight 1
         grid columnconfigure $act.src 0 -weight 1

   # frame bouton
      grid [frame $act.base2 -relief raised -bd 1] -row 2 -column 0 -sticky new -padx 3 -pady 3
         fenEEC2 $act.base2 [ashare::mess ihm 41]

   # frame liste des fichiers et icones
    grid [frame $act.prod -relief raised -bd 1] -row 3 -column 0 -sticky nsew -padx 3 -pady 3

         set sw2 [ScrolledWindow $act.prod.lst]
         set sf2 [ScrollableFrame $sw2.f]
         $sw2 setwidget $sf2
         set liste2 [$sf2 getframe]
         $sf2 configure -constrainedwidth 1
         # liste = ....active.prod.lst.f.frame
         grid $sw2 -row 0 -column 0 -sticky nsew
         grid [frame $act.prod.icone] -row 0 -column 1 -sticky nw
         grid rowconfigure    $act.prod 0 -weight 1
         grid columnconfigure $act.prod 0 -weight 1

      grid rowconfigure    $act 1 -weight 1
      grid rowconfigure    $act 3 -weight 1
      grid columnconfigure $act 0 -weight 1

      chemin_base $act.base.basepath $liste sources
      liste_fich  $liste -1 sources
      liste_icone $act.src.icone $liste sources

      liste_fich  $liste2 -1 surcharge
      liste_icone $act.prod.icone $liste2 surcharge

   }
}

# Fenetre de selection des serveurs pour une execution multiple
# Retourne yes/no
#################################################################
proc select_server_list { } {
    global srv_list_on
    global srv_list_value
    global srv_list_resoncli
    set srv_list_on "no"
    set srv_list_resoncli 0
    catch { set srv_list_resoncli [expr { $astk::profil(multiple_result_on_client) == "yes" }] }
    set fen .fen_srv_list
    catch {destroy $fen}
    toplevel $fen
    wm withdraw $fen
    wm title $fen "[ashare::mess ihm 420]"
    wm transient $fen .
    #wm protocol $fen WM_DELETE_WINDOW "grab release $fen; destroy $fen"
    grab set $fen

    pack [frame $fen.haut -relief solid -bd 0] -fill x -expand yes
    pack [frame $fen.liste -relief solid -bd 1] -fill both -expand yes
    pack [frame $fen.comm -relief solid -bd 0] -fill x -expand yes
    pack [frame $fen.copy -relief solid -bd 0] -padx 20 -fill x -expand yes
    pack [frame $fen.bas -relief solid -bd 0] -fill x -expand yes

    label $fen.haut.title -font $astk::ihm(font,lab) -text [ashare::mess ihm 422]
    pack $fen.haut.title -side left -padx 10 -pady 5

    for {set j 0} {$j < $astk::config(nb_serv)} {incr j} {
        if { $astk::config($j,etat) != "off" } {
            set srvfr $fen.liste.srv$j
            pack [frame $srvfr -relief solid -bd 0] -anchor w -padx 20
            checkbutton $srvfr.cb -variable srv_list_value($j) -anchor w
            label $srvfr.lbl -font $astk::ihm(font,lab) -text $astk::config($j,nom)
            pack $srvfr.cb $srvfr.lbl -pady 3 -side left
        } else {
            set srv_list_value($j) 0
        }
    }
    set serv $astk::inv(serv,$astk::profil(serveur))
    if { [ashare::meme_machine $astk::config(-1,nom_complet) $astk::config($serv,nom_complet)] } {
        label $fen.comm.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 424]
    } else {
        label $fen.comm.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 423]
    }
    pack $fen.comm.lbl -pady 3 -side left

    set wid $fen.copy
    checkbutton $wid.cb -variable srv_list_resoncli -anchor w
    label $wid.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 427]
    pack $wid.cb $wid.lbl -pady 3 -side left

    pack [frame $fen.bas.valid -relief solid -bd 0] -side left -fill both -expand yes
      pack [frame $fen.bas.valid.m -relief solid -bd 0]
      button $fen.bas.valid.m.annuler -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
         -bg $astk::ihm(couleur,annul) \
         -command "grab release $fen; destroy $fen"
      button $fen.bas.valid.m.ok  -font $astk::ihm(font,labbout) -text "Ok" \
         -bg $astk::ihm(couleur,valid) \
         -command "valid_srv_list $fen"
      pack $fen.bas.valid.m.ok $fen.bas.valid.m.annuler -side left -padx 10 -pady 5

    wm deiconify $fen

    # position
    ashare::centre_fen $fen .
    wm deiconify $fen
    update
}

# validation de la liste des serveurs
#################################################################
proc valid_srv_list { fen } {
    global srv_list_on
    global srv_list_value
    global srv_list_resoncli
    # liste des serveurs cochés
    set astk::profil(multiple_server_list) {}
    for {set j 0} {$j < $astk::config(nb_serv)} {incr j} {
        if { $srv_list_value($j) == 1 } {
            lappend astk::profil(multiple_server_list) $astk::config($j,nom_complet)
        }
    }
    # result_on_client
    set astk::profil(multiple_result_on_client) "no"
    if { $srv_list_resoncli == 1 } {
        set astk::profil(multiple_result_on_client) "yes"
    }
    set srv_list_on "yes"
    grab release $fen ; destroy $fen
}
