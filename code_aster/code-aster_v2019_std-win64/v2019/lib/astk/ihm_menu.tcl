#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_menu.tcl 3577 2008-10-24 12:03:28Z courtois $

# créé les menus, si REINI="DETR", on détruit le menu Fichier
# et on le recrée
#################################################################
proc create_menu { fenetre_parent } {
   create_menu_fichier $fenetre_parent
   create_menu_configuration $fenetre_parent
   create_menu_outils $fenetre_parent
   create_menu_options $fenetre_parent
   create_menu_aide $fenetre_parent
}

# Menu Fichier
#################################################################
proc create_menu_fichier { fenetre_parent {REINI "INI"} } {
   if { $REINI != "DETR" } {
      menubutton $fenetre_parent.fic -font $astk::ihm(font,labmenu) -text [ashare::mess ihm 26] \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background) \
        -underline 0 -menu $fenetre_parent.fic.mnu
      pack $fenetre_parent.fic -side left
   } else {
      destroy $fenetre_parent.fic.mnu
   }
   menu $fenetre_parent.fic.mnu -tearoff 0 -bd 1 \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $fenetre_parent.fic.mnu add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 27]      -accelerator "Ctrl+N" -command "nouveau"
   $fenetre_parent.fic.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 28]..." -accelerator "Ctrl+O" -command "ouvrir -1 _VIDE"
   $fenetre_parent.fic.mnu add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 29]      -accelerator "Ctrl+S" -command {enregistrer $astk::profil(serv_profil) $astk::profil(nom_profil)}
   $fenetre_parent.fic.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 30]..." -command "enregistrer -1 _VIDE"
   $fenetre_parent.fic.mnu add separator
   $fenetre_parent.fic.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 275]..." -command "copie_prof"
   $fenetre_parent.fic.mnu add separator
   $fenetre_parent.fic.mnu add cascade -menu $fenetre_parent.fic.mnu.import -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 102]..."
   $fenetre_parent.fic.mnu add cascade -menu $fenetre_parent.fic.mnu.export -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 31]..."
   if { $astk::agla(num_serv) != -1 } {
      $fenetre_parent.fic.mnu add separator
      $fenetre_parent.fic.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 357]..." -command "insert_in_db"
   }
   $fenetre_parent.fic.mnu add separator
   #$fenetre_parent.fic.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 247]..." -command "print_conf"
   #$fenetre_parent.fic.mnu add separator
   $fenetre_parent.fic.mnu add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 366]      -accelerator "Ctrl+W" -command "nouveau"
   $fenetre_parent.fic.mnu add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 32]       -accelerator "Ctrl+Q" -command "quitter"
   dern_prof $fenetre_parent.fic.mnu
   set i 0
   set ashare::context($fenetre_parent.fic.mnu,$i) 115
   incr i
   set ashare::context($fenetre_parent.fic.mnu,$i) 116
   incr i
   set ashare::context($fenetre_parent.fic.mnu,$i) 117
   incr i
   set ashare::context($fenetre_parent.fic.mnu,$i) 118
   incr i
   incr i
   set ashare::context($fenetre_parent.fic.mnu,$i) 276
   incr i
   incr i
   set ashare::context($fenetre_parent.fic.mnu,$i) 119
   incr i
   set ashare::context($fenetre_parent.fic.mnu,$i) 123
   if { $astk::agla(num_serv) != -1 } {
      incr i
      incr i
      set ashare::context($fenetre_parent.fic.mnu,$i) 373
   }
   incr i
   incr i
   #set ashare::context($fenetre_parent.fic.mnu,$i) 247
   #incr i
   #incr i
   set id_close $i
   set ashare::context($fenetre_parent.fic.mnu,$i) 365
   incr i
   set id_quit $i
   set ashare::context($fenetre_parent.fic.mnu,$i) 127

# raccourcis clavier
   bind . <Control-Key-N> "$fenetre_parent.fic.mnu invoke  0"
   bind . <Control-Key-n> "$fenetre_parent.fic.mnu invoke  0"
   bind . <Control-Key-O> "$fenetre_parent.fic.mnu invoke  1"
   bind . <Control-Key-o> "$fenetre_parent.fic.mnu invoke  1"
   bind . <Control-Key-S> "$fenetre_parent.fic.mnu invoke  2"
   bind . <Control-Key-s> "$fenetre_parent.fic.mnu invoke  2"
   bind . <Control-Key-W> "$fenetre_parent.fic.mnu invoke $id_close"
   bind . <Control-Key-w> "$fenetre_parent.fic.mnu invoke $id_close"
   bind . <Control-Key-Q> "$fenetre_parent.fic.mnu invoke $id_quit"
   bind . <Control-Key-q> "$fenetre_parent.fic.mnu invoke $id_quit"


# cascade menu import/export
   menu $fenetre_parent.fic.mnu.import -tearoff 0 -bd 1 \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $fenetre_parent.fic.mnu.import add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 112] -command "importer astk_serv"
   $fenetre_parent.fic.mnu.import add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 122] -command "importer astk_serv append"
   $fenetre_parent.fic.mnu.import add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 151] -command "importer rex"
   $fenetre_parent.fic.mnu.import add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 379] -command "importer test"
   $fenetre_parent.fic.mnu.import add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 383] -command "importer test append"
   set ashare::context($fenetre_parent.fic.mnu.import,0) 120
   set ashare::context($fenetre_parent.fic.mnu.import,1) 120
   set ashare::context($fenetre_parent.fic.mnu.import,2) 121
   set ashare::context($fenetre_parent.fic.mnu.import,2) 121
   set ashare::context($fenetre_parent.fic.mnu.import,2) 121
   menu $fenetre_parent.fic.mnu.export -tearoff 0 -bd 1 \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $fenetre_parent.fic.mnu.export add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 112] -command "exporter_sous astk_serv"
   set ashare::context($fenetre_parent.fic.mnu.export,0) 124
# aide contextuelle
   bind $fenetre_parent.fic.mnu <<MenuSelect>> "ShowStatus menu $fenetre_parent.fic.mnu astk"
   bind $fenetre_parent.fic.mnu.import <<MenuSelect>> "ShowStatus menu $fenetre_parent.fic.mnu.import astk"
   bind $fenetre_parent.fic.mnu.export <<MenuSelect>> "ShowStatus menu $fenetre_parent.fic.mnu.export astk"
}

# Menu Configuration
#################################################################
proc create_menu_configuration { fenetre_parent } {
   menubutton $fenetre_parent.cfg -font $astk::ihm(font,labmenu) -text [ashare::mess ihm 33] \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background) \
        -underline 0 -menu $fenetre_parent.cfg.mnu
   pack $fenetre_parent.cfg -side left
   menu $fenetre_parent.cfg.mnu -tearoff 0 -bd 1 \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $fenetre_parent.cfg.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 35]..." -command "Opt_conf"
   $fenetre_parent.cfg.mnu add separator
   $fenetre_parent.cfg.mnu add cascade -menu $fenetre_parent.cfg.mnu.pref -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 377]
   $fenetre_parent.cfg.mnu add cascade -menu $fenetre_parent.cfg.mnu.theme -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 324]
   $fenetre_parent.cfg.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 331]..." -command {change_font}
   $fenetre_parent.cfg.mnu add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 347] -command {ashare::save_geom}

# cascade menu preferences
   set men $fenetre_parent.cfg.mnu.pref
   menu $men -tearoff 0 -bd 1 \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $fenetre_parent.cfg.mnu.pref add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 37] -command "Opt_prefs"
   $fenetre_parent.cfg.mnu.pref add command -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 378] -command "Opt_prefs_net"

# cascade menu theme/color
   set men $fenetre_parent.cfg.mnu.theme
   menu $men -tearoff 0 -bd 1 \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $men add radiobutton -label [ashare::mess ihm 325] -command {change_couleur std } -variable astk::ihm(style,couleur_var)
   $men add radiobutton -label [ashare::mess ihm 292] -command {change_couleur crystal } -variable astk::ihm(style,couleur_var)
   $men add radiobutton -label [ashare::mess ihm 326] -command {change_couleur nostalgique } -variable astk::ihm(style,couleur_var)
   $men add radiobutton -label [ashare::mess ihm 391] -command {change_couleur lucid_amb } -variable astk::ihm(style,couleur_var)
   $men add radiobutton -label [ashare::mess ihm 327] -command {change_couleur perso} -variable astk::ihm(style,couleur_var)
   $men add separator
   $men add command -label "[ashare::mess ihm 330]..." -command {perso_couleur }
   set ashare::context($fenetre_parent.cfg.mnu,0) 129
   set ashare::context($fenetre_parent.cfg.mnu,2) 131
   set ashare::context($fenetre_parent.cfg.mnu,3) 328
   set ashare::context($fenetre_parent.cfg.mnu,4) 332
   set ashare::context($fenetre_parent.cfg.mnu,5) 347
# aide contextuelle
   bind $fenetre_parent.cfg.mnu <<MenuSelect>> "ShowStatus menu $fenetre_parent.cfg.mnu astk"
}

# Menu Outils
#################################################################
proc create_menu_outils { fenetre_parent {REINI "INI"} } {
   if { $REINI != "DETR" } {
      menubutton $fenetre_parent.out -font $astk::ihm(font,labmenu) -text [ashare::mess ihm 153] \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background) \
        -underline 0 -menu $fenetre_parent.out.mnu
      pack $fenetre_parent.out -side left
   } else {
      destroy $fenetre_parent.out.mnu
   }
   menu $fenetre_parent.out.mnu -tearoff 0 -bd 1 \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $fenetre_parent.out.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 154]..."  -command {outils_conf}
   set ashare::context($fenetre_parent.out.mnu,0) 155
   liste_outils $fenetre_parent.out.mnu
# aide contextuelle
   bind $fenetre_parent.out.mnu <<MenuSelect>> "ShowStatus menu $fenetre_parent.out.mnu astk"
}

# Menu Options
#################################################################
proc create_menu_options { fenetre_parent {REINI "INI"} } {
   if { $REINI != "DETR" } {
      menubutton $fenetre_parent.opt -font $astk::ihm(font,labmenu) -text [ashare::mess ihm 250] \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background) \
        -underline 1 -menu $fenetre_parent.opt.mnu
      pack $fenetre_parent.opt -side left
   } else {
      destroy $fenetre_parent.opt.mnu
   }
   menu $fenetre_parent.opt.mnu -tearoff 0 -bd 1 \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
#   $fenetre_parent.opt.mnu add cascade -menu $fenetre_parent.opt.mnu.menarg -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 13]
#   set men $fenetre_parent.opt.mnu.menarg
#   menu $men -tearoff 0 -bd 1
   $fenetre_parent.opt.mnu add command -label [ashare::mess ihm 13] -font $astk::ihm(font,labmenuB) -state disabled -foreground $astk::ihm(couleur,background) -background $astk::ihm(couleur,foreground)
   set men $fenetre_parent.opt.mnu
   set narg [expr [llength $astk::ihm(l_arg)] / 3]
   for {set i 0} {$i < $narg} {incr i} {
      set opt  [lindex $astk::ihm(l_arg) [expr $i * 3]]
      set val  [lindex $astk::ihm(l_arg) [expr $i * 3 + 1]]
      set imes [lindex $astk::ihm(l_arg) [expr $i * 3 + 2]]
      $men add checkbutton -font $astk::ihm(font,labmenu) -label "$opt" \
         -variable astk::profil(option,$opt) -command "mod_argument $opt $val"
#      set ashare::context($men,$i) $imes
      set ashare::context($men,[expr 1+$i]) $imes
   }
   $fenetre_parent.opt.mnu add separator
#   $fenetre_parent.opt.mnu add cascade -menu $fenetre_parent.opt.mnu.menpar -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 154]
#   set men $fenetre_parent.opt.mnu.menpar
#   menu $men -tearoff 0 -bd 1
   $fenetre_parent.opt.mnu add command -label [ashare::mess ihm 154] -font $astk::ihm(font,labmenuB) -state disabled -foreground $astk::ihm(couleur,background) -background $astk::ihm(couleur,foreground)
   set men $fenetre_parent.opt.mnu
   set npar [expr [llength $astk::ihm(l_par)] / 3]
   for {set i 0} {$i < $npar} {incr i} {
      set opt  [lindex $astk::ihm(l_par) [expr $i * 3]]
      set imes [lindex $astk::ihm(l_par) [expr $i * 3 + 1]]
      $men add command -font $astk::ihm(font,labmenu) -label "$opt = $astk::profil(opt_val,$opt)" \
         -command "mod_param $men $opt"
#      set ashare::context($men,$i) $imes
      set ashare::context($men,[expr 1+$narg+2+$i]) $imes
   }
   $fenetre_parent.opt.mnu add separator
#   $fenetre_parent.opt.mnu add cascade -menu $fenetre_parent.opt.mnu.menopt -font $astk::ihm(font,labmenu) -label [ashare::mess ihm 333]
#   set men $fenetre_parent.opt.mnu.menopt
#   menu $men -tearoff 0 -bd 1
   $fenetre_parent.opt.mnu add command -label [ashare::mess ihm 333] -font $astk::ihm(font,labmenuB) -state disabled -foreground $astk::ihm(couleur,background) -background $astk::ihm(couleur,foreground)
   set men $fenetre_parent.opt.mnu
   set nopt [expr [llength $astk::ihm(l_opt)] / 3]
   for {set i 0} {$i < $nopt} {incr i} {
      set opt  [lindex $astk::ihm(l_opt) [expr $i * 3]]
      set imes [lindex $astk::ihm(l_opt) [expr $i * 3 + 1]]
      $men add command -font $astk::ihm(font,labmenu) -label "$opt = $astk::profil(opt_val,$opt)" \
         -command "mod_option $men $opt"
#      set ashare::context($men,$i) $imes
      set ashare::context($men,[expr 1+$narg+2+$npar+2+$i]) $imes
   }

# aide contextuelle
   bind $fenetre_parent.opt.mnu <<MenuSelect>> "ShowStatus menu $fenetre_parent.opt.mnu astk"
#    bind $fenetre_parent.opt.mnu.menarg <<MenuSelect>> "ShowStatus menu $fenetre_parent.opt.mnu.menarg astk"
#    bind $fenetre_parent.opt.mnu.menpar <<MenuSelect>> "ShowStatus menu $fenetre_parent.opt.mnu.menpar astk"
#    bind $fenetre_parent.opt.mnu.menopt <<MenuSelect>> "ShowStatus menu $fenetre_parent.opt.mnu.menopt astk"
}

# Menu Aide
#################################################################
proc create_menu_aide { fenetre_parent } {
# Menu Aide
   menubutton $fenetre_parent.help -font $astk::ihm(font,labmenu) -text [ashare::mess ihm 105] \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background) \
        -underline 0 -menu $fenetre_parent.help.mnu
   pack $fenetre_parent.help -side right
   menu $fenetre_parent.help.mnu -tearoff 0 -bd 1 \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   set id_log 8
   $fenetre_parent.help.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 110]..." -command {aff_aide . 110}
   $fenetre_parent.help.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 111]..." -command {aff_aide . 111}
   $fenetre_parent.help.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 190]..." -command {aff_aide . 190}
   $fenetre_parent.help.mnu add separator
   $fenetre_parent.help.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 241]..." -command {aff_aide . 241}
   $fenetre_parent.help.mnu add separator
   if { $astk::agla(num_serv) > -1 } {
      $fenetre_parent.help.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 269]..." -command {contact_ata}
      incr id_log
   }
   $fenetre_parent.help.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 323]..." -command {aff_aide . 323}
   $fenetre_parent.help.mnu add separator
   $fenetre_parent.help.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 196]..." -accelerator "Ctrl+M" -command {show_fen $astk::ihm(log) force}
   $fenetre_parent.help.mnu add command -font $astk::ihm(font,labmenu) -label "[ashare::mess ihm 106]..." -command {a_propos .}
# aide contextuelle
   bind . <Control-Key-m> "$fenetre_parent.help.mnu invoke $id_log"
}
