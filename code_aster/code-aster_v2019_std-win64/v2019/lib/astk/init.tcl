#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: init.tcl 1204 2006-08-04 09:06:22Z mcourtoi $

# default - may be branch dependent - keep consistency with run.py
set ASTKRC ".astkrc"

# namespace astk
#################################################################
namespace eval astk {
   variable astk_version
   variable lang
   variable check_quit
   variable recup
   variable rcdir_mod
   variable rcdir
   variable rcdir_arg
   variable tmpdir
   variable local_server
   variable fic_serveurs
   variable fic_options
   variable fic_hosts
   variable fic_prefs
   variable fic_prof
   variable fic_outils
   variable fic_print
   variable modprof
   # command line (ps, shell_cmd)
   variable cmd
   variable platform

   variable icon

   # Structures de données
   # (voir DOC/sdd_'nom de la sdd'.txt pour le détail)
   variable UL
   variable UL_ref
   variable profil
   variable config
   variable agla
   variable ihm
   variable rex
   variable rex_fiche
   variable sel
   variable msg

   namespace export astk
}

# valeurs par défaut
set astk::rcdir $ASTKRC
set astk::rcdir_mod 0

# pré-traitement des arguments
# pour le moment uniquement pour définir astk::rcdir
#################################################################
proc pre_traite_argv { } {
   global argc argv
   for {set i 0} {$i < $argc} {incr i} {
      switch -exact -- [lindex $argv $i] {
         --rcdir {
            if { $i < [ expr $argc - 1 ] } {
               incr i
               set astk::rcdir [lindex $argv $i]
               set astk::rcdir_mod 1
            }
         }
      }
   }
}

#################################################################
proc init_gene { root } {
   global env tcl_platform

# Initialisation des variables partagées
   # mettre dbg<>0 pour tracer
   set ashare::dbg 0
   set ashare::fen_log 0

   # sortie apres les verifs ?
   set astk::check_quit 0

   # chemin de référence (install)
   set ashare::root $root
   # chemin astkrc_ref
   set prefix [file dirname [file dirname $root]]
   if { $prefix == "/usr" } {
      set prefix "/"
   }
   set ashare::prefix $prefix
   set ashare::astkrc_ref [file join $ashare::prefix "etc" "codeaster" "astkrc"]
   catch { set ashare::astkrc_ref [file join $env(ASTER_ETC) "codeaster" "astkrc"] }

   #  numero de version de astk
   set pkginfo_file [file join $ashare::root __pkginfo__.tcl]
   source $pkginfo_file
   set lv [split $astk::astk_version .]
   set fin [expr [llength $lv] - 1]
   if { $fin >= 3 && [lindex $lv 3] == "final" } {
      set fin 2
   }
   set astk::astk_version [join [lrange $lv 0 $fin] .]

   # definition des frames
   set astk::ihm(menu)        .bar
   set astk::ihm(fenetre)     .fen.fenetre
   set astk::ihm(satellite)   .fen.satellite
   set astk::ihm(popup)       .popup
   set astk::ihm(status)      .fen.status
   set astk::ihm(asjob)       .asjob
   set astk::ihm(log)         .log
   set astk::ihm(interrompre) .interrompre

   # chargement des modules
   set astk::ihm(extensions) { }
   load_modules

# plate-forme
   set astk::platform $tcl_platform(platform)
   if { $astk::platform != "unix" && $astk::platform != "windows" } {
         ashare::log "<ERREUR 000> Unsupported platform : $astk::platform"
         ashare::my_exit 4
   }
   if { $astk::platform == "unix" } {
       set astk::cmd(shell_cmd) "bash -c"
   } else {
       set astk::cmd(shell_cmd) "cmd.exe"
   }

# $DISPLAY
   init_display

# $HOME sur l'interface
   #XXX "~" pose problème car exec ne "glob" pas
   set astk::config(-1,home) "~"
   catch { set astk::config(-1,home) $env(HOME) }

   # nom réservé du serveur hébergeant l'interface
   set astk::local_server "Local"
   set astk::config(-1,nom) $astk::local_server
   set astk::config(-1,islocal) "oui"
   # IP
   init_nom_complet 0
   # dhcp
   #set astk::config(-1,nom_complet) "xxx.xxx.xxx.xxx"
   # login
   set astk::config(-1,login) [whoami]

# machine accessible
   set astk::config(-1,etat) "on"

# chemin des ressources
# rcdir_arg contiendra par exemple .astkrc_salomeXX, c'est
# lui qui est passé à as_run
   set astk::rcdir_arg $astk::rcdir
   set astk::rcdir [file join $astk::config(-1,home) $astk::rcdir]

# répertoire temporaire
   set astk::tmproot "/tmp"
   if { $env(ASTER_TMPDIR) != "" } {
       set astk::tmproot $env(ASTER_TMPDIR)
   }
   set astk::tmpdir "$astk::tmproot/astk_$astk::config(-1,login)"

   file mkdir "$astk::rcdir"
   catch { file delete [file join "$astk::tmpdir" *] }
   file mkdir "$astk::tmpdir"

   #  dit si on récupère ou pas les infos de config
   set astk::recup 1

# fichiers
   # liste de la configuration des serveurs
   set astk::fic_serveurs [file join $astk::rcdir config_serveurs]

   # fichiers contenant les préférences de la machine interface
   set astk::fic_prefs [file join $astk::rcdir prefs]

   # fichier des derniers profils ouverts
   set astk::fic_prof [file join $astk::rcdir lastprof]

   # fichier de configuration des outils
   set astk::fic_outils [file join $astk::rcdir outils]

   # fichier de configuration des imprimantes
   set astk::fic_print [file join $astk::rcdir printers]

   # fichier des préférences des couleurs
   set astk::fic_color [file join $astk::rcdir colors]

   # numéro de l'onglet agla pour faciliter les choses si on le bouge
   set astk::ihm(nongl_agla) 4

   # profil initial
   set astk::ihm(profil_ini) ""
   set astk::ihm(export_ini) ""
   set astk::ihm(serv_ini) $astk::local_server
   set astk::ihm(etat_satellite) 0

   # styles
   set astk::ihm(style,couleur) std
   set astk::ihm(style,fontlist) [list main fixe bsf]
   set astk::ihm(style,fontlist_label) [list 344 345 346]

   # geometrie
   set astk::ihm(style,geomlist) [list astk asjob bsf rex log]
   for {set i 0} {$i < [llength $astk::ihm(style,geomlist)]} {incr i} {
      set fen [lindex $astk::ihm(style,geomlist) $i]
      set astk::ihm(style,Geom_$fen) ""
   }

   # sélection
   init_sel

   # mode de lancement d'une étude
   set astk::ihm(mode,run) "run"
   set astk::ihm(mode,dbg) "dbg"
   set astk::ihm(mode,pre) "pre"

   # pré-initialisation pour la fenetre des logs
   # potentiellement écrasé ensuite
   set ashare::fen_log 1
   set astk::ihm(font,txt)     "-Adobe-Helvetica-Medium-R-Normal--*-100-*-*-*-*-*-*"
   set astk::ihm(font,labbout) "-Adobe-Helvetica-Bold-R-Normal--*-120-*-*-*-*-*-*"
   # fichier des logs
   set ashare::fic_log [file join $astk::rcdir log]
   set ashare::fic_progress [file join $astk::rcdir logprogress]
   catch { file delete $ashare::fic_progress }

   # init des listes arguments, paramètres
   set astk::ihm(l_arg) { dbgjeveux  non 261 \
                          rep_outils oui 262 \
                          rep_dex    oui 263 \
                          rep_mat    oui 268 \
                          ORBInitRef oui 273 \
                        }
   set astk::ihm(l_par) { nbmaxnook    264 {} \
                          cpresok      265 {RESOK RESNOOK} \
                          only_nook    430 {OUI YES oui yes NON NO non no} \
                          facmtps      266 {} \
                          corefilesize 293 {} \
                        }
   # on se sert du "OUI" en premier pour proposer une liste cochable oui/non
   set astk::ihm(l_opt) { ncpus        44 {} \
                          mpi_nbcpu   300 {} \
                          mpi_nbnoeud 370 {} \
                          classe      334 {} \
                          depart      335 {} \
                          after_job   387 {} \
                          distrib     386 {OUI YES oui yes NON NO non no} \
                          exectool    385 {} \
                          multiple    413 {OUI YES oui yes NON NO non no}
                        }
}

# initialise le display
#################################################################
proc init_display { } {
   global env
   set ashare::DISPLAY ""
   catch {set ashare::DISPLAY $env(DISPLAY)}
}

# initialise le nom complet de la machine locale
#################################################################
proc init_nom_complet { {isdhcp 0} } {
   set astk::config(-1,nom_complet) ""
   if { $isdhcp == 0 } {
      # essai avec $HOSTNAME
      catch { set astk::config(-1,nom_complet) $env(HOSTNAME) }
      # essai avec `hostname`
      if { $astk::config(-1,nom_complet) == "" } {
         catch { set astk::config(-1,nom_complet) [exec hostname] }
      }
      # essai avec `hostname`
      if { $astk::config(-1,nom_complet) == "" } {
         catch { set astk::config(-1,nom_complet) [exec uname -n] }
      }
   }
}

# initialise la sélection
#################################################################
proc init_sel { } {
   set astk::sel(filename) [list]
   set astk::sel(servname) ""
   set astk::sel(liste) ""
   set astk::sel(indice) ""
}

#################################################################
proc load_modules { } {
   global env
# Chargement des modules astk
   set modul { init_env unites_logiques \
               init_agla \
               fichier options pref_config aide \
               ihm_main ihm_menu ihm_commun ihm_liste ihm_etude ihm_execution \
               ihm_agla ihm_satellite ihm_status ihm_outils ihm_rex \
               ihm_salome \
               meshtool convbase \
               tools lance_calcul \
               i_serv }
   set nmod [llength $modul]
   for {set i 0} {$i < $nmod} {incr i} {
      set modi [lindex $modul $i]
      append modi ".tcl"
      set fich [file join $ashare::root $modi]
      if { [file exists $fich] } {
         source $fich
      } else {
         puts "<ERREUR 000> External module not found : $fich"
         exit 4
      }
   }

# Chargement des modules partagés
   set modsh { config preferences tkselecteur remote_tools appli outils \
               messages lire_mcles ihm_tools file_funct print }
   set nmod [llength $modsh]
   for {set i 0} {$i < $nmod} {incr i} {
      set modi [lindex $modsh $i]
      append modi ".tcl"
      set fich [file join $ashare::root $modi]
      if { [file exists $fich] } {
         source $fich
      } else {
         ashare::log "<ERREUR 000> External module not found : $fich"
         ashare::my_exit 4
      }
   }

# Chargement du module asjob
   set mod { asjob_main init_asjob listej }
   set nmod [llength $mod]
   for {set i 0} {$i < $nmod} {incr i} {
      set modi [lindex $mod $i]
      append modi ".tcl"
      set fich [file join $ashare::root $modi]
      if { [file exists $fich] } {
         source $fich
      } else {
         ashare::log "<ERREUR 000> External module not found : $fich"
         ashare::my_exit 4
      }
   }

# Chargement des modules externes
# TKPNG_ROOT contient pkgIndex.tcl et la libtkpng(VERS).so correspondante
   set tkpng_root not_defined
   catch { set tkpng_root $env(TKPNG_ROOT) }
   set mod { "BWidget BWidget-1.7.0 1" }
   lappend mod "tkpng $tkpng_root 0"
   set nmod [llength $mod]
   for {set i 0} {$i < $nmod} {incr i} {
      set modi [lindex $mod $i]
      set bdir [file join $ashare::root [lindex $modi 1]]
      lappend ::auto_path $bdir
      set iret 4
      catch {
          namespace inscope :: package require [lindex $modi 0]
          lappend astk::ihm(extensions) [lindex $modi 0]
          set iret 0
      }
      if { $iret != 0 } {
         if { [lindex $modi 2] == 1 } {
            ashare::log "<ERREUR 000> Required module not found : [lindex $modi 0]"
            ashare::log "             searched in $bdir"
            ashare::my_exit 4
         } else {
            ashare::log "<INFO> Optional module not found : [lindex $modi 0]"
            ashare::log "       searched in $bdir"
         }
      }
   }

# icones
   init_icon
}

# icones
#################################################################
proc init_icon { {theme crystal} } {
   set icons { openR openF new delete edit \
               dir file up refresh comp \
               help help_sans salome \
               uparrow downarrow }

   # default icons
   set prefix "crystalsvg_22_"
   set extension ".gif"
   set ficon { folder_open fileopen filenew button_cancel edit \
               folder empty up reload openterm \
               help help salome \
               1uparrow 1downarrow }

   if { $theme == "vaio" } {
      set prefix "vaio_"
      set ficon { openRS openS newS deleteS editS \
                  dirS fileS upS refreshS compS \
                  helpS sans_helpB salome \
                  uparrow downarrow }
   } elseif { $theme == "lucid_amb" } {
      set prefix "ambiance_"
      set ficon { folder_open fileopen filenew button_cancel edit \
                  folder empty up reload openterm \
                  help help salome \
                  uparrow downarrow }
      if { [lsearch $astk::ihm(extensions) tkpng] >= 0 } {
          set extension ".png"
      } else {
          ashare::log "<WARNING> tkpng required to use full colors icons of the theme: lucid_amb"
      }
   }
   # add iconwindow
   append icons "iconwindow"
   append ficon "iconwindow"

   set nmod [llength $icons]
   for {set i 0} {$i < $nmod} {incr i} {
      set iconi [lindex $icons $i]
      set fich $prefix
      append fich [lindex $ficon $i]
      append fich $extension
      set fich [file join $ashare::root "icons" $fich]
      set astk::icon($iconi) $fich
      if { [file exists $fich] == 0 } {
         puts "<ERREUR 000> Icon file not found : $fich"
         exit 4
      }
   }
}

#################################################################
proc init_couleur { {act "setpal"} } {
   ashare::mess info 34 $astk::ihm(style,couleur)
# variable du widget radio du menu
   set astk::ihm(style,couleur_var) [ashare::mess ihm 325]
# initialisation des valeurs prédéfinies
   # avant-plan général
   set astk::ihm(couleur,foreground) black
   # avant/arrière-plan général
      set astk::ihm(couleur,menu_foreground) black
   set astk::ihm(couleur,background) "#E2E2E2"
   # arrière-plan menu
   set astk::ihm(couleur,menu_background) "#E2E2E2"
   # arrière-plan entry
   set astk::ihm(couleur,entry_background) "#FFFFFF"
   # valid
   set astk::ihm(couleur,valid) "#E3D4FF"
   # annulation
   set astk::ihm(couleur,suppr) "#EA6B6B"
   # suppression
   set astk::ihm(couleur,annul) "#E3D4FF"
   # bouton
   set astk::ihm(couleur,onglet_inactif) "#E3D4FF"
   # bouton actif
   set astk::ihm(couleur,onglet_actif) "#AB8EFF"
   # suivi
   set astk::ihm(couleur,suivi) "#E3D4FF"
   # lancement
   set astk::ihm(couleur,lancement) "#E3D4FF"
   # listes
   set astk::ihm(couleur,liste) "#E3D4FF"
   set astk::ihm(couleur,mode_lancement) "#E3D4FF"
   if { $astk::ihm(style,couleur) == "nostalgique" } {
      set astk::ihm(style,couleur_var) [ashare::mess ihm 326]
      # avant-plan général
      set astk::ihm(couleur,foreground) black
      # arrière-plan général
      set astk::ihm(couleur,background) "#FFDEAD"
      # avant/arrière-plan menu
      set astk::ihm(couleur,menu_foreground) black
      set astk::ihm(couleur,menu_background) "#FFDEAD"
      # arrière-plan entry
      set astk::ihm(couleur,entry_background) "#FFFFF0"
      # valid
      set astk::ihm(couleur,valid) "#66CDAA"
      # annulation
      set astk::ihm(couleur,annul) "#FFA500"
      # suppression
      set astk::ihm(couleur,suppr) "#FF0000"
      # bouton
      set astk::ihm(couleur,onglet_inactif) "#FFDAB9"
      # bouton actif
      set astk::ihm(couleur,onglet_actif) "#87CEFF"
      # bouton suivi
      set astk::ihm(couleur,suivi) "#FFD700"
      # lancement
      set astk::ihm(couleur,lancement) "#2F9BCB"
      # listes
      set astk::ihm(couleur,liste) "#FFDEAD"
      set astk::ihm(couleur,mode_lancement) "#7FFFD4"
   } elseif { $astk::ihm(style,couleur) == "crystal" } {
      set astk::ihm(style,couleur_var) [ashare::mess ihm 292]
      # avant-plan général
      set astk::ihm(couleur,foreground) "#404040"
      # arrière-plan général
      set astk::ihm(couleur,background) "#f0f0ff"
      # avant/arrière-plan menu
      set astk::ihm(couleur,menu_foreground) "#404040"
      set astk::ihm(couleur,menu_background) "#f0f0ff"
      # arrière-plan entry
      set astk::ihm(couleur,entry_background) "#ffffff"
      # valid
      set astk::ihm(couleur,valid) "#dee0e5"
      # annulation
      set astk::ihm(couleur,suppr) "#eb4712"
      # suppression
      set astk::ihm(couleur,annul) "#dee0e5"
      # bouton
      set astk::ihm(couleur,onglet_inactif) "#fffe98"
      # bouton actif
      set astk::ihm(couleur,onglet_actif) "#f6cf5f"
      # suivi
      set astk::ihm(couleur,suivi) "#dee0e5"
      # lancement
      set astk::ihm(couleur,lancement) "#4ea9ff"
      # listes
      set astk::ihm(couleur,liste) "#dee0e5"
      set astk::ihm(couleur,mode_lancement) "#dee0e5"
   } elseif { $astk::ihm(style,couleur) == "lucid_amb" } {
      set astk::ihm(style,couleur_var) [ashare::mess ihm 391]
      # avant-plan général
      set astk::ihm(couleur,foreground) "#000000"
      # arrière-plan général
      set astk::ihm(couleur,background) "#f0ebe2"
      # avant/arrière-plan menu
      set astk::ihm(couleur,menu_foreground) "#dfd8c6"
      set astk::ihm(couleur,menu_background) "#3c3b37"
      # arrière-plan entry
      set astk::ihm(couleur,entry_background) "#ffffff"
      # valid
      set astk::ihm(couleur,valid) "#f0ebe2"
      # annulation
      set astk::ihm(couleur,suppr) "#f06e34"
      # suppression
      set astk::ihm(couleur,annul) "#af9f89"
      # bouton
      set astk::ihm(couleur,onglet_inactif) "#cfc3ad"
      # bouton actif
      set astk::ihm(couleur,onglet_actif) "#f9e4da"
      # suivi
      set astk::ihm(couleur,suivi) "#d8cebf"
      # lancement
      set astk::ihm(couleur,lancement) "#c2b4a2"
      # listes
      set astk::ihm(couleur,liste) "#cfc3ad"
      set astk::ihm(couleur,mode_lancement) "#c2b4a2"
   }
# surcharge par couleur perso
   if { $astk::ihm(style,couleur) == "perso" } {
      set astk::ihm(style,couleur_var) [ashare::mess ihm 327]
      if { [file exists $astk::fic_color] } {
         set iret [ashare::lire_mc_val $astk::fic_color mots vale nlu]
         if { $iret == 0 } {
            for {set j 1} {$j <= $nlu} {incr j} {
               if { $mots($j) != "astkrc_version" } {
                  set astk::ihm(couleur,$mots($j)) $vale($j)
               }
            }
         } else {
            ashare::mess erreur 34
         }
      }
   }
   # option tk
   if { $act == "setpal" } {
      tk_setPalette $astk::ihm(couleur,background)
      option add *foreground $astk::ihm(couleur,foreground)
      option add *background $astk::ihm(couleur,background)
      option add *Entry*background $astk::ihm(couleur,entry_background)
      option add *HighlightThickness 0
   }
}

#################################################################
proc init_font { } {
   SelectFont::loadfont
# valeurs par défaut si rien dans les préférences
   set lf $astk::ihm(style,fontlist)
   set df {"helvetica 10" "courier 10" "helvetica 10"}
   for {set i 0} {$i < [llength $lf]} {incr i} {
      set ff [lindex $lf $i]
      if { $astk::ihm(style,font_$ff) == "" } {
         set astk::ihm(style,font_$ff) [lindex $df $i]
      }
      set astk::config(-1,font_$ff) $astk::ihm(style,font_$ff)
      # valeurs individuelles
      set name($ff) [lindex $astk::ihm(style,font_$ff) 0]
      set size($ff) [lindex $astk::ihm(style,font_$ff) 1]
      set weig($ff) ""
      catch {set weig($ff) [lindex $astk::ihm(style,font_$ff) 2]}
   }
# définition des fonts utilisées
   # labels
   set astk::ihm(font,lab)      "{$name(main)} $size(main) $weig(main)"
   # labels "petits"
   set astk::ihm(font,labpt)    "{$name(main)} [expr $size(main)-2] $weig(main)"
   # valeurs texte
   set astk::ihm(font,val)      "{$name(main)} $size(main) $weig(main)"
   # titre
   set astk::ihm(font,tit)      "{$name(main)} [expr $size(main)+2] $weig(main)"
   # texte
   set astk::ihm(font,txt)      "{$name(main)} $size(main) $weig(main)"
   # texte fixed
   set astk::ihm(font,txtfix)   "{$name(fixe)} $size(fixe) $weig(fixe)"
   # zone de texte fixed
   set astk::ihm(font,zonfix)   "{$name(fixe)} $size(fixe) $weig(fixe)"
   # labels des listes
   set astk::ihm(font,lablst)   "{$name(main)} $size(main) $weig(main)"
   # labels menu
   set astk::ihm(font,labmenu)  "{$name(main)} $size(main) $weig(main)"
   # labels menu gras
   set astk::ihm(font,labmenuB) "{$name(main)} $size(main) bold"
   # labels boutons
   set astk::ihm(font,labbout)  "{$name(main)} $size(main) bold"
   # liste de fichiers dans le browser
   set astk::ihm(font,brwlst)   "{$name(bsf)}  $size(bsf)  $weig(bsf)"
# pour les dialog tk
   option add *font $astk::ihm(font,lab)
}

# définition des listes de mots_cles dans les fichiers de config
#################################################################
proc init_mots { } {
# fichier de config
   ashare::init_mots_config
# fichier de messages
   ashare::init_mots_msg
# fichier prefs
   set ashare::mots(MCS_pref) { nom_user email org def_vers xterm editeur
                                nb_reman langue dbglevel freq_actu nb_ligne
                                flag_maj isdhcp ipdhcp nom_domaine bip
                                remote_copy_protocol remote_shell_protocol
                                couleur font_main font_fixe font_bsf
                                Geom_astk Geom_asjob Geom_bsf Geom_rex Geom_log
                                browser theme forced_display
                                listul user_listul
                                }
# fichier lastprof
   set ashare::mots(MCF_prof) { profil }
   set ashare::mots(SSF_prof) { serv }
# fichier outils
   set ashare::mots(MCF_out) { nom }
   set ashare::mots(SSF_out) { cmde ext dist }
# fichier des imprimantes
   set ashare::mots(MCF_pr) { label }
   set ashare::mots(SSF_pr) { serv file cmde }
}

# liste des langues <--> locale
#################################################################
proc init_lang { } {
   set astk::ihm(lang,FR)  "fr"
   set astk::ihm(lang,ENG) "en"
   set astk::ihm(lang,DE)  "de"
}

# liste des plates-formes : correspondance nom "aster" - nom parlant
#################################################################
proc init_pltf { } {
   set astk::ihm(pltf,LINUX) "Linux x86"
   set astk::ihm(pltf,LINUX64) "Linux 64 bits"
   set astk::ihm(pltf,LINUX32) "Linux x86"
   set astk::ihm(pltf,WIN64) "Windows 64 bits"
   set astk::ihm(pltf,WIN32) "Windows"

   set astk::ihm(pltf,P_LINUX) "Linux x86"
   set astk::ihm(pltf,TRU64) "OSF1 TRU64"
   set astk::ihm(pltf,SOLARIS) "Solaris"
   set astk::ihm(pltf,SOLARIS64) "Solaris 64 bits"
   set astk::ihm(pltf,IRIX64) "IRIX 64 bits"
   set astk::ihm(pltf,HPUX) "HP-UX"
   set astk::ihm(pltf,PPRO_NT) "Windows"
}

# initialise les variables de l'ihm
# (fait après le traitement des arguments)
#################################################################
proc init_ihm { } {
   init_couleur
   init_font
   init_pltf
   init_lang

   if { $ashare::noinitwarn == 1 } {
      set ashare::show_dialog 0
   }
}

# fin de la phase d'initialisation
#################################################################
proc end_init { } {
   set ashare::fen_log 2
   set ashare::show_dialog 1
   if { $ashare::dbg < 4 } {
      destroy $astk::ihm(log)
   }
}


#################################################################
proc set_titre { } {
   set titre [ashare::mess ihm 107 $astk::astk_version]
   append titre " - "
   append titre [file tail $astk::profil(nom_profil)]
   if { $astk::modprof } {
      append titre " "
      append titre [ashare::mess ihm 133]
   }
   if { [file tail $astk::profil(nom_profil)] != [ashare::mess ihm 27] } {
      append titre " - "
      append titre [file dirname $astk::profil(nom_profil)]
   }
   wm title . $titre
}

proc set_icon { {win .} } {
   # iconphoto seems not supported on tk 8.3
   catch {
      set ico [image create photo appicon -file $astk::icon(iconwindow)]
      wm iconphoto $win $ico
   }
}

# init_profil : reinitialise la config/options
# appele aussi par Fichier/Nouveau
#################################################################
proc init_profil {} {
# onglets
   for {set i 1} {$i <= $astk::ihm(nbongM)} {incr i} {
      for {set j 1} {$j <= $astk::ihm(nbong,$i) } {incr j} {
         set astk::profil($astk::ihm(ong,$i,$j)) non
      }
   }
   # cocher par défaut le premier onglet (ETUDE)
   set astk::profil($astk::ihm(ong,1,1)) oui
   set astk::profil(M_1) $astk::profil($astk::ihm(ong,1,1))

   set astk::profil(make_etude) $astk::ihm(mode,run)

   set astk::profil(etude,nbfic) 0
   set astk::profil(path_etude) ""
   set astk::profil(serv_etude) -1
   set astk::profil(tests,nbfic) 0
   set astk::profil(path_tests) ""
   set astk::profil(serv_tests) -1
   set astk::profil(sources,nbfic) 0
   set astk::profil(path_sources) ""
   set astk::profil(serv_sources) -1
   set astk::profil(surcharge,nbfic) 0
   set astk::profil(path_surcharge) ""
   set astk::profil(serv_surcharge) -1
   set astk::profil(args) ""
   set astk::profil(special) ""
   set astk::profil(debug) 0
   set astk::profil(serv_profil) -1
   set astk::profil(nom_profil) [ashare::mess ihm 27]
   set astk::profil(serv_fich_export) -1
   set astk::profil(nom_fich_export) "_VIDE"

# arguments/paramètres/options
   set narg [expr [llength $astk::ihm(l_arg)] / 3]
   for {set i 0} {$i < $narg} {incr i} {
      set opt  [lindex $astk::ihm(l_arg) [expr $i * 3]]
      set astk::profil(option,$opt) 0
      set astk::profil(opt_val,$opt) ""
   }
   set npar [expr [llength $astk::ihm(l_par)] / 3]
   for {set i 0} {$i < $npar} {incr i} {
      set opt  [lindex $astk::ihm(l_par) [expr $i * 3]]
      set astk::profil(option,$opt) 1
      switch $opt {
         nbmaxnook    { set val 5 }
         cpresok      { set val RESNOOK }
         facmtps      { set val 1 }
         corefilesize { set val unlimited }
         default      { set val "" }
      }
      set astk::profil(opt_val,$opt) $val
   }
   set nopt [expr [llength $astk::ihm(l_opt)] / 3]
   for {set i 0} {$i < $nopt} {incr i} {
      set opt  [lindex $astk::ihm(l_opt) [expr $i * 3]]
      set astk::profil(option,$opt) 1
      switch $opt {
         ncpus       { set val "" }
         mpi_nbcpu   { set val 1 }
         mpi_nbnoeud { set val 1 }
         default     { set val "" }
      }
      set astk::profil(opt_val,$opt) $val
   }

# initialisation des paramètres pour omniORB
   if { $ashare::origine == "from_salome" } {
        mod_ORBInitRef $ashare::ORBInitRef
   }
# flag de modification du profil
   modprof off
}

# raccourci pour signaler qu'on modifie le profil
#################################################################
proc modprof { {mode on} } {
   if { $mode == "on" } {
      set astk::modprof 1
   } else {
      set astk::modprof 0
   }
   set_titre
   return 1
}

# mise à jour des valeurs autorisées d'une option
#################################################################
proc update_opt { opt auth_value {REINI "APPEND"} } {
   set trouv 0
   set i -1
   set npar [expr [llength $astk::ihm(l_opt)] / 3]
   for {set i 0} {$i < $npar} {incr i} {
      set var [lindex $astk::ihm(l_opt) [expr $i * 3]]
      if { $var == $opt } {
         set prev [lindex $astk::ihm(l_opt) [expr $i * 3 + 2]]
         set new {}
         set trouv 1
         if { $REINI == "APPEND" } {
            # on évite les doublons
            set new $prev
            foreach val $auth_value {
               if { [lsearch $prev $val] < 0 } {
                  set new "$new $val"
               }
            }
         }
         lset astk::ihm(l_opt) [expr $i * 3 + 2] $new
         break
      }
   }
   if { $trouv == 0 } {
      ashare::log "<ERROR> <update_opt> unknown option : $opt"
   } else {
      ashare::mess "info" 48 $opt $auth_value
   }
}
