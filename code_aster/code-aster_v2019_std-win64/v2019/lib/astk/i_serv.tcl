#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: i_serv.tcl 3764 2009-01-16 16:36:40Z courtois $

# Interface pour ASTK_SERV

# lancement en utilisant les services d'un serveur
# code retour : voir lance_calcul
#################################################################
proc lance_astk_serv { batch } {
   set iret 0
   set soumbtc 0
# indices ihm
   set serv $astk::inv(serv,$astk::profil(serveur))
# export du profil
   set astk::profil(special) ""
#
# exceptions AGLA
#
# exception pour asrest :
# - on regarde s'il y en a déjà un
# - on demande confirmation éventuellement
   if { $astk::profil(agla) == "oui" && $astk::profil(asrest) == "oui"  } {
      set cmd "$astk::cmd(shell_cmd) \"if test -d /aster/eda/"
      append cmd "$astk::profil(version)/$astk::config($serv,login)"
      append cmd "; then echo EXIST ; else echo RAS ; fi\""
      change_status "[ashare::mess ihm 253]..."
      set iret [ashare::rexec_cmd $serv astk::config $cmd "" 0 out .]
      if { $iret == 0 } {
         if { [regexp EXIST $out] } {
            set msg [ashare::mess ihm 254 $astk::profil(version) $astk::config($serv,login)]
            set iret [tk_messageBox -message $msg -title [ashare::mess ihm 138] -type yesno -icon question -parent .]
            if { $iret != "yes" } {
               ashare::mess erreur 24
               return -999
            }
         }
      } else {
         # pb lancement de la commande
         ashare::mess "erreur" 3 $cmd $iret $out
         return $iret
      }
   }
# exception pour asdenot, on demande confirmation
   if { $astk::profil(agla) == "oui" && $astk::profil(asdenot) == "oui"  } {
        set msg [ashare::mess ihm 429 $astk::profil(version)]
        set iret [tk_messageBox -message $msg -title [ashare::mess ihm 138] -type yesno -icon question -parent .]
        if { $iret != "yes" } {
           ashare::mess erreur 24
           return -999
        }
   }
# exception pour asdeno, on cree un profil bidon, on garde le bon dans sav_prof
   if { $astk::profil(agla) == "oui" && $astk::profil(asdeno) == "oui"  } {
      array set sav_prof [array get astk::profil]
      # on oublie les fichiers
      set astk::profil(path_etude) ""
      set astk::profil(path_tests) ""
      set astk::profil(path_sources) ""
      set astk::profil(path_surcharge) ""
      set astk::profil(etude,nbfic) 0
      set astk::profil(tests,nbfic) 0
      set astk::profil(sources,nbfic) 0
      set astk::profil(surcharge,nbfic) 0
      set iret [asdeno_ihm]
      if { $iret != 0 } {
         array set astk::profil [array get sav_prof]
         ashare::mess erreur 24
         return -999
      }
      set iret [exporter astk_serv $astk::profil(serv_fich_export) $astk::profil(nom_fich_export) noverif]
   # on rétablit sav_prof dès qu'on n'en a plus besoin
      array set astk::profil [array get sav_prof]
      array unset sav_prof
   } else {
      set iret [exporter astk_serv $astk::profil(serv_fich_export) $astk::profil(nom_fich_export)]
   }
#
# retour export
   if { $iret == 2 } {
   # alarmes emises lors de l'export
   } elseif { $iret == 4 } {
      # exit silencieux, les messages sont émis lors de l'export
      return -999
   } elseif { $iret == -888 } {
      set soumbtc $iret
   } elseif { $iret != 0 } {
      return $iret
   }

# préparation de la ligne de commande à exécuter
   set lcmd ""
   set argu ""
   # as_exec fichier_export
   append lcmd [file join $ashare::prefix "bin" as_run]
   append lcmd " --proxy --serv --schema=[get_schema $serv serv]"
   append lcmd [ashare::get_glob_args]
   if { [is_localhost_serv $astk::profil(serv_fich_export)] == 0 } {
      append argu "$astk::config($astk::profil(serv_fich_export),login)@$astk::config($astk::profil(serv_fich_export),nom_complet):"
   }
   append argu $astk::profil(nom_fich_export)

   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lance_astk_serv) $lcmd + $argu"
   }

# execution
   change_status "[ashare::mess ihm 49]..."
   set iret [ ashare::rexec_cmd -1 astk::config $lcmd $argu 0 out . progress]
   set jret $iret
   if { $iret == 0 } {
      if { $soumbtc == 0 } {
      # traitement du retour
         set nomjob [get_nomjob]
         set jret [retour_as_exec $nomjob $out]
      } else {
         set fname [decode_consbtc_output $out]
         set msg [ashare::mess ihm 412 $astk::config($serv,nom_complet) $fname]
         change_status $msg
         tk_messageBox -message $msg -type ok -icon info
         set jret $soumbtc
      }
   } else {
   # pb lancement de as_exec
      ashare::mess "erreur" 3 $lcmd $jret $out
      return [expr 100+$jret]
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lance_astk_serv) iret=$iret, jret=$jret, soumbtc=$soumbtc, output :\n$out"
      catch { ashare::log "<DEBUG> (lance_astk_serv) jobid=$jobid\nqueue=$queue" }
   }
   return $jret
}

# traitement du retour d'un as_exec
# nom du job et output
#################################################################
proc retour_as_exec { nomjob out } {
   set jret 0
# indices ihm
   set serv $astk::inv(serv,$astk::profil(serveur))
   set studyid "unknown"
   # decodage du retour
   if { [regexp {JOBID[ ]*=[ ]*([0-9a-zA-Z\._\-]+)} $out mat1 jobid] != 1 ||
        [regexp {QUEUE[ ]*=[ ]*([-a-z_A-Z0-9]+)} $out mat1 queue] != 1 } {
      set jret 3
   }
   regexp {STUDYID[ ]*=[ ]*([-a-z_A-Z0-9]+)} $out mat1 studyid
   if { $jret == 0} {
      if { $astk::profil(batch) } {
         set msg [ashare::mess ihm 125 $jobid $queue $studyid]
      } else {
         set msg [ashare::mess ihm 68 $jobid]
      }
      change_status $msg
      update idletasks
   # prévenir asjob
      set date [clock format [clock seconds] -format "%d/%m/%y"]
      set heure [clock format [clock seconds] -format "%H:%M:%S"]
      set user $astk::config($serv,login)
      set nserv $astk::config($serv,nom_complet)
      set interf "ASTK_$astk::astk_version"
      if { $astk::profil(batch) } {
         set mode "batch"
      } else {
         set mode "interactif"
      }
      set kret [ajout_job $jobid $nomjob $date $heure PEND _ $queue _ $user $nserv $astk::profil(noeud) $interf $mode]
      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (retour_as_exec) call ajout_job, kret = $kret"
      }
      if { $kret != 0 } {
         set jret 6
         ashare::mess "erreur" 3 ajout_job $kret " "
      }
   }
   return $jret
}

# exporter pour astk_serv
# tout_dist (voir exporter)
# code retour : 2 = messages d'info émis ; 4 = erreur
# verif_suppl = "noverif" pour ne pas faire les vérifications supplémentaires
#################################################################
proc export_astk_serv { ftmp {tout_dist "non"} {verif_suppl "oui"}} {
   # UL non repetables qui existent dans plusieurs profils
   global ul_nrep flg
   global env
   array unset ul_nrep
   array unset flg

   set iret 0
   # messages d'info et d'erreur par défaut
   set imsg(2) 16
   set imsg(4) 40
   # mise à zéro des flags
   foreach var {etude tests sources surcharge} {
      for {set j 0} { $j < $astk::UL_ref($var,nbre) } {incr j} {
         set flg($astk::UL_ref($var,$j,nom)) ""
      }
   }
   foreach typ {exec cmde ele etude dbg env astout} {
      set flg(make_$typ) "non"
   }
   # indices ihm
   set serv $astk::inv(serv,$astk::profil(serveur))
   set idexp [open $ftmp w]
   set vers $astk::profil(version)
   # paramètres
   puts $idexp "P profastk $astk::config($astk::profil(serv_profil),login)@$astk::config($astk::profil(serv_profil),nom_complet):$astk::profil(nom_profil)"
   write_server_infos $idexp $serv
   puts $idexp "P noeud $astk::profil(noeud)"
   puts $idexp "P mclient $astk::config(-1,nom_complet)"
   puts $idexp "P uclient $astk::config(-1,login)"
   puts $idexp "P version $astk::profil(version)"
   set lang $astk::ihm(lang,$astk::config(-1,langue))
   puts $idexp "P lang $lang"
   if { $astk::profil(debug) } {
      puts $idexp "P debug debug"
   } else {
      puts $idexp "P debug nodebug"
   }
   if { $astk::profil(batch) } {
      puts $idexp "P mode batch"
   } else {
      puts $idexp "P mode interactif"
   }
   # valeurs des options de lancement
   set nopt [expr [llength $astk::ihm(l_opt)] / 3]
   for {set i 0} {$i < $nopt} {incr i} {
      set opt  [lindex $astk::ihm(l_opt) [expr $i * 3]]
      puts $idexp "P $opt $astk::profil(opt_val,$opt)"
   }
   if { [in_yes_values $astk::profil(opt_val,multiple)] } {
      puts $idexp "P multiple_server_list $astk::profil(multiple_server_list)"
      puts $idexp "P multiple_result_on_client $astk::profil(multiple_result_on_client)"
   }
   # nom du job
   set lign "P nomjob "
   append lign [get_nomjob]
   puts $idexp $lign
   puts $idexp "P origine ASTK $astk::astk_version"
   # obsolete si version >= 1.9
   puts $idexp "P display $ashare::DISPLAY"

   # paramètre spécial
   if { $astk::profil(special) != "" } {
      puts $idexp "P special $astk::profil(special)"
   }

   # activer le mode verbose/debug du service
   if { $ashare::dbg >= 4 } {
      puts $idexp "P srv_verb oui"
   }
   if { $ashare::dbg >= 5 } {
      puts $idexp "P srv_dbg oui"
   }

   # arguments utilisateur
   set tmp $astk::profil(args)
   # on identifie les arguments non répétables (valeurs nulles ignorees)
   foreach opt {dbgjeveux rep_outils rep_dex rep_mat ORBInitRef} {
      if { $astk::profil(option,$opt) == 1 } {
         set repl "-$opt"
         if { $astk::profil(opt_val,$opt) != "" } {
            append repl " *$astk::profil(opt_val,$opt)"
         }
         if { $astk::profil(opt_val,$opt) != "" || $opt == "dbgjeveux" } {
            regsub -all -- "$repl" $tmp "" tmp
            puts $idexp "A $opt $astk::profil(opt_val,$opt)"
         }
      }
   }
   puts $idexp "A args $tmp"


   ###########################
   # mémoire et temps
   # mémoire
   set mem_aster $astk::profil(memoire)
   set tmp "A memjeveux "
   if { [ regexp {64$} $astk::config($serv,plate-forme) ] } {
      append tmp [expr $mem_aster / 8.]
   } else {
      append tmp [expr $mem_aster / 4.]
   }
   puts $idexp $tmp
   # temps max (verif faite dans lance_calcul, si exporter direct...)
   set tmp "A tpmax "
   set vtps [ conv_tps $astk::profil(temps) ]
   append tmp $vtps
   puts $idexp $tmp
   # mémoire et temps pour le batch et les infos
   set vmem [expr $astk::profil(memoire) * 1024]
   puts $idexp "P memjob $vmem"
   set vtmin [expr ( $vtps - 1 ) / 60 + 1]
   puts $idexp "P tpsjob $vtmin"
   ###########################
   # suivi interactif
   if { $astk::profil(suivi_interactif) } {
      #if { [is_localhost_serv $serv] == 0 } {
        #set iret 2
        #set imsg($iret) 50
      #}
      puts $idexp "P follow_output yes"
   }
   ###########################
   # valeurs des paramètres
   set npar [expr [llength $astk::ihm(l_par)] / 3]
   for {set i 0} {$i < $npar} {incr i} {
      set opt  [lindex $astk::ihm(l_par) [expr $i * 3]]
      puts $idexp "P $opt $astk::profil(opt_val,$opt)"
   }
   ###########################
   # Onglet surcharge coché
   set var surcharge
   if { $astk::profil($var) == "oui" } {
      # Liste des fichiers
      set jret [liste_astk_serv sources $idexp $vers $tout_dist $verif_suppl]
      if { $jret != 0 && $jret > $iret } {
         set iret $jret
      }
      # c'est dans liste_astk_serv que l'on exclut exec, cmde et ele
      set jret [liste_astk_serv surcharge $idexp $vers $tout_dist $verif_suppl]
      if { $jret != 0 && $jret > $iret } {
         set iret $jret
      }
   }
   ###########################
   # Onglet Etude coché
   set var etude
   if { $astk::profil($var) == "oui" } {
      if { $astk::profil(make_etude) == $astk::ihm(mode,run) } {
      # lancement du calcul
         set flg(make_$var) "oui"
      } elseif { $astk::profil(make_etude) == $astk::ihm(mode,dbg) } {
      # lancement du debuggueur
         set flg(make_dbg) "oui"
      } else {
      # environnement pour debug
         set flg(make_env) "oui"
      }
      # Liste des fichiers
      set jret [liste_astk_serv $var $idexp $vers $tout_dist $verif_suppl]
      if { $jret != 0 && $jret > $iret } {
         set iret $jret
      }
   }
   if { [in_yes_values $astk::profil(opt_val,distrib)] } {
      if { ($flg(distr) == "") || ([regexp "D" $flg(distr)] != 1)
        || ($flg(repe)  == "") || ([regexp "R" $flg(repe)]  != 1) } {
         set iret 4
         set imsg($iret) 53
      }
   }

   # fin onglet etude
   ###########################
   # Bouton tests coché
   set var tests
   if { $astk::profil($var) == "oui" } {
      if { $astk::profil(agla) != "oui" } {
         set flg(make_astout) "oui"
      }
      # Liste des fichiers
      set jret [liste_astk_serv $var $idexp $vers $tout_dist $verif_suppl]
      if { $jret != 0 && $jret > $iret } {
         set iret $jret
      }
   }
   # fin make_test
   ###########################
   # Bouton agla coché
   set var agla
   if { $astk::profil($var) == "oui" } {
      for {set j 2} {$j <= $astk::ihm(nbong,$astk::ihm(nongl_agla)) } {incr j} {
         if { $astk::profil($astk::ihm(ong,$astk::ihm(nongl_agla),$j)) == "oui" } {
            puts $idexp "P actions $astk::ihm(ong,$astk::ihm(nongl_agla),$j)"
            break
         }
      }
   }
   # fin agla
   ###########################
   # présence d'un fichier btc en donnée   seul => consbtc=non
   #                           en resultat seul => soumbtc=non
   if { $flg(btc) != "" } {
      if { [regexp "D" $flg(btc)] } {
         puts $idexp "P soumbtc oui"
      } else {
         puts $idexp "P soumbtc non"
         if { $iret == 0 } {
            set iret -888
         }
      }
      if { [regexp "R" $flg(btc)] } {
         puts $idexp "P consbtc oui"
      } else {
         puts $idexp "P consbtc non"
      }
   } else {
      puts $idexp "P consbtc oui"
      puts $idexp "P soumbtc oui"
   }
   ###########################
   # Actions dont l'ordre est important
   foreach typ {exec cmde ele etude dbg env astout} {
      if { $flg(make_$typ) == "oui" } {
         if { $typ != "astout" } {
            puts $idexp "P actions make_$typ"
         } else {
            puts $idexp "P actions $typ"
         }
      }
   }
   # fin de l'export
   close $idexp

   ###########################
   # Vérifications complémentaires
   if { $verif_suppl != "noverif" } {
      # il devrait y avoir un .comm
      if { $astk::profil(etude) == "oui" && $flg(comm) == "" } {
         set iret 4
         set imsg($iret) 26
      }
      # sur mach_ref
      if { $iret < 4 && $astk::config($serv,batch) == "oui" } {
         # pas d'astout en interactif
         if { $flg(make_astout) == "oui" && $astk::profil(batch) == 0 } {
            set iret 4
            set imsg($iret) 44
         }
         # pas de make_exec en même temps que make_test (astout)
         if { $flg(make_exec) == "oui" && $flg(make_astout) == "oui" && $astk::profil(batch) == 1 } {
            set iret 4
            set imsg($iret) 45
         }
         # pas de make_exec en même temps que make_etude
         if { $flg(make_exec) == "oui" && $flg(make_etude) == "oui" && $astk::profil(batch) == 1 } {
            set iret 4
            set imsg($iret) 52
         }
      }
      # pas base et bhdf en D ou R en même temps
      if { $iret < 4
        && (   ( [regexp "D" $flg(base)] == 1 && [regexp "D" $flg(bhdf)] == 1)
            || ( [regexp "R" $flg(base)] == 1 && [regexp "R" $flg(bhdf)] == 1) )} {
            set iret 4
            set imsg($iret) 46
      }
      # adéquation source/résultat
      if { $iret < 4 && $astk::profil(surcharge) == "oui" && $astk::profil(agla) != "oui" } {
         # exec en résultat si et seulement s'il y a du C/fortran
         if { (($flg(f) != "" || $flg(f90) != "" || $flg(c) != "") && [regexp "R" $flg(exec)] == 0)
           || (($flg(f) == "" && $flg(f90) == "" && $flg(c) == "") && [regexp "R" $flg(exec)] == 1)} {
            set iret 4
            set imsg($iret) 27
         }
         if { ($astk::profil(etude) == "oui" || $astk::profil(tests) == "oui" )
           && (($flg(exec) != "" && [regexp "D" $flg(exec)] == 0)
            || ($flg(cmde) != "" && [regexp "D" $flg(cmde)] == 0)
            || ($flg(ele)  != "" && [regexp "D" $flg(ele) ] == 0)) } {
            set iret 4
            set imsg($iret) 30
         }
         # catalogue de commandes compilé en résultat si et seulement s'il y a des capy
         if { ($flg(capy) != "" && [regexp "R" $flg(cmde)] == 0)
           || ($flg(capy) == "" && [regexp "R" $flg(cmde)] == 1) } {
            set iret 4
            set imsg($iret) 28
         }
         # catalogue d'éléments compilé en résultat si et seulement s'il y a des cata
         if { ($flg(cata) != "" && [regexp "R" $flg(ele)] == 0)
           || ($flg(cata) == "" && [regexp "R" $flg(ele)] == 1) } {
            set iret 4
            set imsg($iret) 29
         }
      }
      # pas base ou bhdf en D et ele en D en même temps
      if { $iret < 4
        && (   ( [regexp "D" $flg(base)] == 1 || [regexp "D" $flg(bhdf)] == 1)
              && [regexp "D" $flg(ele)] == 1 ) } {
            set iret 4
            set imsg($iret) 57
      }
   }
   # message d'info ou d'erreur
   if { $iret > 0 } {
      if { $iret == 2 } {
         set msg [ashare::mess info $imsg($iret)]
      } elseif { $iret == 4 } {
         set msg [ashare::mess erreur $imsg($iret)]
      } else {
         set msg [ashare::mess erreur 33]
      }
      change_status $msg
      tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
   }
#
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (export_astk_serv) iret : $iret"
   }
   return $iret
}

# exporte le contenu d'une "liste de fichiers" pour as_run
# types de liste : etude, tests, sources, surcharge
# code retour : 2 = messages d'info émis ; 4 = erreur
# tout_dist (voir exporter)
#################################################################
proc liste_astk_serv { var idexp vers {tout_dist "non"} {verif_suppl "oui"}} {
# UL non repetables qui existent dans plusieurs profils
   global ul_nrep flg

   set iret 0
# boucle sur les fichiers
   for { set i 0 } { $i < $astk::profil($var,nbfic) } { incr i } {
      set jret 0
      # flag tentative de répéter un fichier non répétable
      set ficrep 0
      # chemin du fichier
      set valf ""
      set sfic $astk::inv(serv,$astk::profil($var,fich,$i,serv))
      set serv $astk::inv(serv,$astk::profil(serveur))
      if { $tout_dist == "oui" || [is_localhost_serv $sfic] == 0 } {
         append valf "$astk::config($sfic,login)@$astk::config($sfic,nom_complet):"
      }
      if { [string index $astk::profil($var,fich,$i,nom) 0] == "/" } {
         append valf "$astk::profil($var,fich,$i,nom)"
      } else {
         append valf [file join $astk::profil(path_$var) $astk::profil($var,fich,$i,nom)]
      }
   # enlever les /./
      regsub -all {/\./} $valf "/" valf
   # il faut être en D ou R pour être exporté
   # test supplémentaire pour ne pas exporter exec, cmde, ele si agla
      if { ($astk::profil($var,fich,$i,donnee) || $astk::profil($var,fich,$i,resultat))
        && (($astk::profil(agla) == "non" || $var != "surcharge")
            || ($var == "surcharge" && $astk::profil(agla) == "oui"
                && $astk::profil($var,fich,$i,type) != "exec"
                && $astk::profil($var,fich,$i,type) != "cmde"
                && $astk::profil($var,fich,$i,type) != "ele")) } {
      # vérif du nom de fichier
         if { [regexp " " $valf mat1] } {
            ashare::mess erreur 38 $valf
            set jret 4
         }
      # vérif de l'UL
         if { $var != "etude" } {
            set astk::profil($var,fich,$i,UL) 0
         }
         regsub -all " " $astk::profil($var,fich,$i,UL) "" astk::profil($var,fich,$i,UL)
         regexp {([0-9]+)} $astk::profil($var,fich,$i,UL) mat1 val_ul
         if { $astk::profil($var,fich,$i,UL) != $val_ul || $val_ul > 99 } {
            ashare::mess erreur 39 $astk::profil($var,fich,$i,UL)
            set jret 4
         }
      # flag
         set flag ""
         if { $astk::profil($var,fich,$i,donnee)   } { append flag "D" }
         if { $astk::profil($var,fich,$i,resultat) } { append flag "R" }
         if { $astk::profil($var,fich,$i,compress) } { append flag "C" }

         set typ $astk::profil($var,fich,$i,type)
         # si comm et UL<>1, on passe à libr: pourquoi ? on supprime

         set lign $astk::profil($var,fich,$i,FR)
         append lign " "
         append lign $typ
         append lign " "
         append lign $valf
         append lign " "
         append lign $flag
         append lign " "
         append lign $astk::profil($var,fich,$i,UL)
      # on regarde quel type on traite
         set flgold $flg($typ)
         for {set j 0} { $j < $astk::UL_ref($var,nbre) } {incr j} {
            if { $typ == $astk::UL_ref($var,$j,nom) } {
               append flg($typ) $flag
               set ityp $j
               break
            }
         }
      # vérifier que la compression est autorisée
         if { $astk::profil($var,fich,$i,compress) && $astk::UL_ref($var,$ityp,cpr) == 0 } {
            ashare::mess erreur 37 $typ
            set jret 4
         }
      # verifications débrayables
         if { $verif_suppl == "noverif" } {
            puts $idexp $lign
         } else {
         # fichier non répétable (et commun à plusieurs profils)
            if { $astk::UL_ref($var,$ityp,rep) < 0 } {
               # s'il existe, on prend sa valeur, sinon on gardera 0
               set ini 0
               catch { set ini $ul_nrep($typ) }
               set ul_nrep($typ) [expr $ini + 1]
               if { $ul_nrep($typ) == 1 } {
                  puts $idexp $lign
               } else {
                  set ficrep 1
               }
            } elseif { $astk::UL_ref($var,$ityp,rep) == 2 } {
         # fichier répétable une fois en donnée, une fois en résultat maximum
               if { ([regexp D $flgold] && [regexp D $flag])
                 || ([regexp R $flgold] && [regexp R $flag]) } {
                  set ficrep 1
               } else {
                  puts $idexp $lign
               }
            } elseif { $astk::UL_ref($var,$ityp,rep) == 1 } {
         # fichier répétable
               puts $idexp $lign
            } elseif { $flgold == "" } {
         # première occurence d'un fichier non répétable
               puts $idexp $lign
            } else {
               set ficrep 1
            }
         }
      # on marque les actions à la première occurence
         if { $var == "surcharge" && $flgold == ""
           && ($typ == "exec" || $typ == "cmde" || $typ == "ele")
           && $astk::profil(agla) != "oui"
           && $astk::profil($var,fich,$i,resultat) } {
            set flg(make_$typ) "oui"
         }
      # si lancement de tests sur la machine de reference,
      # exec, cmde, ele doivent etre sur cette machine
         if { $var == "surcharge" && $astk::profil(tests) == "oui"
           && $astk::config($serv,mach_ref) == "oui"
           && $astk::profil(agla) != "oui"
           && ($typ == "exec" || $typ == "cmde" || $typ == "ele")
           && [ashare::meme_machine $astk::config($serv,nom_complet) $astk::config($sfic,nom_complet)] == 0 } {
            ashare::mess erreur 43 [quel_onglet $var] $typ
            set jret 4
         }
      # si lancement de tests sur la machine de reference,
      # resu_test doit etre sur cette machine
         if { $var == "tests" && $astk::config($serv,mach_ref) == "oui"
           && $typ == "resu_test"
           && [ashare::meme_machine $astk::config($serv,nom_complet) $astk::config($sfic,nom_complet)] == 0 } {
            ashare::mess erreur 43 [quel_onglet $var] $typ
            set jret 4
         }
      }
   # message si tentative de répétition non permise
      if { $ficrep && $jret < 4 } {
         ashare::mess info 15 $typ $valf
         set jret 2
      }
   # on garde le code retour le plus "grave"
      if { $jret != 0 && $jret > $iret } {
         set iret $jret
      }
   }

# Code retour
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (liste_astk_serv) (var=$var), iret : $iret"
   }
   return $iret
}

# retourne le nom du job : nom du profil ou fonction agla
#################################################################
proc get_nomjob { } {
   set nomjob "unknown"
   if { $astk::profil(agla) == "oui" } {
      for {set j 2} {$j <= $astk::ihm(nbong,$astk::ihm(nongl_agla)) } {incr j} {
         if { $astk::profil($astk::ihm(ong,$astk::ihm(nongl_agla),$j)) == "oui" } {
            set nomjob $astk::ihm(ong,$astk::ihm(nongl_agla),$j)
            append nomjob "_"
            append nomjob $astk::profil(version)
            break
         }
      }
   } elseif { $astk::profil(tests) == "oui" } {
      set nomjob astout
   } else {
      set nomjob [file rootname [file tail $astk::profil(nom_profil)]]
   }
   return $nomjob
}

# lors que le calcul n'est pas soumis, l'endroit où a été
# produit le fichier btc est dans l'output (as_run >= 1.9 seulement).
#################################################################
proc decode_consbtc_output { out } {
    set fname "unknown"
    regexp -line {BTCFILE[ ]*=[ ]*(.+?)$} $out mat1 fname
    return $fname
}
