#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# SCRIPT PRINCIPAL : asjob.tcl

# $Id: asjob.tcl 2075 2006-08-31 16:39:31Z courtois $

# on cache la fenêtre en attendant la construction
wm withdraw .

set root [file dirname [info script]]
source [file join $root init_share.tcl]
source [file join $root init.tcl]

set ashare::appli "asjob"
init_gene $root
unset root

# pas de fenetre de log
set ashare::fen_log 0

# initialisation des mots clés des différents fichiers
init_mots

# initialisation des messages erreur/info/ihm
ashare::lire_msg [file join $ashare::root $ashare::fic_msg]

# préférences utilisateur
ashare::init_prefs
set ashare::dbg $astk::config(-1,dbglevel)

set msg [ashare::mess "info" 20 $astk::astk_version]
puts $msg

# initialisation des couleurs, fonts...
init_couleur
init_font

# verifications
check_conf $astk::check_quit

if { $ashare::dbg } {
   ashare::mess "info" 2 $ashare::dbg }

# initialisation des ressources
init_env

# init
init_sel

# lancement asjob
show_fen .asjob force

# modifie les actions quitter
proc quit_asjob { } {
   set asjob::actu(indic) 0
   destroy $asjob::fen_root
   exit 0
}
wm protocol $asjob::fen_root WM_DELETE_WINDOW { quit_asjob }
.asjob.main.fm.file.m entryconfigure [ashare::mess ihm 32] -command { quit_asjob }
