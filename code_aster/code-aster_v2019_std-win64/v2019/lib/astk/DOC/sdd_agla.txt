Données nécessaires à l'AGLA :

astk::agla(status) = on/off pour activer ou non l'onglet

astk::agla(nb_verdev)  = nb de versions gérées
astk::agla(verdev,i)   = version

# info utilisateur - identAster
astk::agla(num_serv)   = numéro du serveur de config(...)
astk::agla(instance)   = instance d'après identAster (as_info_ref)
                         UA par défaut
astk::agla(infoid)     = infos complètes d'identification d'identAster
                         (P.NOM login mail Organisme/Unité)

astk::agla(mail_ata)   = mail de l'assistance téléphonique (as_info_ref)

astk::agla(rex_url)    = URL du rex (Roundup)
astk::agla(rex_rep)    = répertoire où sont stockés les fichiers associés aux fiches
