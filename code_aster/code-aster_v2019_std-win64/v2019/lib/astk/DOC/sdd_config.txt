###########################################################
astk::config(i,home)     = $HOME sur le serveur i

# Préférences utilisateur sur la machine interface
###########################################################
astk::config(-1,nom_user) = nom complet
astk::config(-1,email) = email
astk::config(-1,choix_agla) = active ou non (on/off) les fonctions AGLA
astk::config(-1,def_vers) = version préférée de l'utilisateur
astk::config(-1,editeur) = éditeur
astk::config(-1,browser) = navigateur
astk::config(-1,nb_reman) = nbre de profils rémanents dans le menu Fichier
astk::config(-1,org) = organisme/unité de l'utilisateur
astk::config(-1,langue) = langue de l'interface
astk::config(-1,ipdhcp) = dernière IP utilisée
astk::config(-1,isdhcp) = 0 si IP fixe, 1 si DHCP
astk::config(-1,remote_shell_protocol) = SSH/RSH
astk::config(-1,remote_copy_protocol) = SCP/RCP/RSYNC
   si DHCP, l'adresse IP est dans astk::config(-1,ipdhcp)

# pas lue dans le fichier prefs
astk::config(-1,nom_complet) = machine interface (HOSTNAME)
                              qui sert à savoir si on peut lancer directement
                              une commande sans rsh.
astk::config(-1,login) = login sur la machine interface (whoami)

astk::config(ignprefs,xxxx) : mots clés ignorés mais stockés pour sauvegarde
                              future du fichier 'prefs'

# Configuration Aster
###########################################################
astk::config(0,nom)=Compaq
astk::config(1,nom)=Cluster
astk::config(2,nom)=Local
astk::config(nb_serv)=3

astk::config(0,nom_complet)=clayastr.cla.edfgdf.fr
astk::config(0,etat)=on/off accessible ou non
astk::config(0,login)=mcourtoi
astk::config(0,home)=répertoire par défaut sur le serveur
astk::config(0,rep_serv)=répertoire des services
astk::config(0,asrun_vers)=version asrun
astk::config(0,recup)=auto/manuel/non
                      auto : récupère automatiquement les infos de astk_serv
                      manu : seulement sur demande
                      none : astk_serv n'est pas installé (serveur de fichiers)
astk::config(0,last_recup)=date de la dernière récup des données
astk::config(0,xterm)=/usr/bin/X11/xterm -sb -si -geometry 120x25
astk::config(0,editeur)=/aster/public/nedit-5.2/nedit -display cli70cx.der.edf.fr:0.0

# fournis par as_info
astk::config(0,batch)=oui
astk::config(0,batch_memmax)=12000
astk::config(0,batch_tpsmax)=72:00:00
astk::config(0,batch_nbpmax)=4
astk::config(0,batch_queue_group)="urgent dvp"

astk::config(0,interactif)=oui
astk::config(0,interactif,memmax)=128
astk::config(0,interactif,tpsmax)=0:05:00
astk::config(0,interactif,nbpmax)=1

astk::config(0,vers,0)=NEW6
astk::config(0,vers,1)=STA6
astk::config(0,nb_vers)=2

astk::config(0,noeud,0)=clayastr.cla.edfgdf.fr
astk::config(0,noeud,1)=cla2astr
astk::config(0,noeud,2)=cla3astr
astk::config(0,noeud,3)=cla4astr
astk::config(0,nb_noeud)=4

astk::config(0,mach_ref)=oui/non     machine de référence ?
astk::config(0,islocal)=oui/non      machine locale ?
