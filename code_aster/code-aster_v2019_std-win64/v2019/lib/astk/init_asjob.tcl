#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################
# $Id: init_asjob.tcl 2075 2006-08-31 16:39:31Z courtois $

# définition et initialisation des srtuctures de données utilisées dans asjob
# Initialisations asjob
#
namespace eval asjob {
#
   variable NomSuivi
#
   variable fen_root
   variable window
   variable popup
   variable status ""
   variable Jobs
   variable TextJobs
   variable fmt
   variable maxarg
   variable paraJob
   variable actu
   variable size
}

#### =================
proc init_asjob {} {
#### =================
# initialisations
   set asjob::NomSuivi [file join $astk::rcdir asjob]
#
# géométrie initiale
#
   set asjob::size(h,f_tail) 8
   set asjob::size(w,f_tail) 90
   set zzz -1
   catch {set zzz $asjob::size(root)}
   if { $zzz == -1 } {
      set asjob::size(root) 100x10
   } else {
      set asjob::size(root) $zzz
   }

#
# structure des paramètres affichés dans le suivi d'exécution
#
   set asjob::maxarg 13

   array set asjob::paraJob {
      numero     { 0 "%16s"   }
      nom        { 1 "%-16s" }
       date       { 2 "%8s"  }
       heure      { 3 "%8s"   }
       etat       { 4 "%6s"   }
       diagnostic { 5 "%-18s"  }
       classe     { 6 "%16s"  }
       tcpu       { 7 "%8s"   }
       user       { 8 "%8s"   }
       machine    { 9 "%-24s" }
       noeud      {10 "%10s"  }
       interface  {11 "%16s"  }
       mode       {12 "%10s"  }
    }  
   set asjob::fmt [concat\
      "[lindex $asjob::paraJob(numero)     1]" "[lindex $asjob::paraJob(nom)        1]" \
      "[lindex $asjob::paraJob(date)       1]" "[lindex $asjob::paraJob(heure)      1]" \
      "[lindex $asjob::paraJob(etat)       1]" "[lindex $asjob::paraJob(diagnostic) 1]" \
        "[lindex $asjob::paraJob(classe)     1]" "[lindex $asjob::paraJob(tcpu)       1]" \
        "[lindex $asjob::paraJob(user)       1]" "[lindex $asjob::paraJob(machine)    1]" \
        "[lindex $asjob::paraJob(noeud)      1]" "[lindex $asjob::paraJob(interface)  1]" \
        "[lindex $asjob::paraJob(mode)       1]"  \
            ]
   return 1
}
