#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: pref_config.tcl 3530 2008-09-26 08:45:55Z courtois $

# reaction a Configuration/Preferences/Generales
#################################################################
proc Opt_prefs { } {
   global choix_listul
   bckup_prefs

   set fen .fen_pref
   catch {destroy $fen}
   toplevel $fen
   wm withdraw $fen
   wm title $fen "[ashare::mess ihm 33] - [ashare::mess ihm 37]"
   wm transient $fen .
   grab set $fen

   pack [frame $fen.liste -relief solid -bd 1]
# titre
   pack [frame $fen.liste.tit1 -relief solid -bd 0] -fill x
   label $fen.liste.tit1.lbl -font $astk::ihm(font,labbout) -text [ashare::mess ihm 141] -width 35 -anchor w
   pack $fen.liste.tit1.lbl -pady 3 -side left -fill x
# aide
   button $fen.liste.tit1.help -image [image create photo -file $astk::icon(help)] \
      -command "grab release $fen ; aff_aide . 190" -bd 0 -bg $astk::ihm(couleur,background)
   pack $fen.liste.tit1.help -side right

# infos "perso"
   pack [frame $fen.liste.nom -relief solid -bd 0] -anchor w
   label $fen.liste.nom.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 89] -width 35 -anchor w
   entry $fen.liste.nom.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,nom_user)
   pack $fen.liste.nom.lbl $fen.liste.nom.path -pady 3 -side left

   pack [frame $fen.liste.email -relief solid -bd 0] -anchor w
   label $fen.liste.email.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 90] -width 35 -anchor w
   entry $fen.liste.email.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,email)
   pack $fen.liste.email.lbl $fen.liste.email.path -pady 3 -side left

# organisme / unité
   pack [frame $fen.liste.org -relief solid -bd 0] -anchor w
   if { $astk::agla(num_serv) > -1 && [llength $astk::agla(infoid)] > 0 } {
      set astk::config(-1,org) [lindex $astk::agla(infoid) 3]
      label $fen.liste.org.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 240] -width 35 -anchor w
      label $fen.liste.org.inst -font $astk::ihm(font,lab) -text $astk::config(-1,org)
      pack $fen.liste.org.lbl $fen.liste.org.inst -pady 3 -side left
   } else {
      label $fen.liste.org.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 240] -width 35 -anchor w
      entry $fen.liste.org.inst -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,org)
      pack $fen.liste.org.lbl $fen.liste.org.inst -pady 3 -side left
   }

# instance AGLA
   pack [frame $fen.liste.agla -relief solid -bd 0] -anchor w
   if { $astk::agla(num_serv) > -1 } {
      label $fen.liste.agla.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 91] -width 35 -anchor w
      label $fen.liste.agla.inst -font $astk::ihm(font,lab) -text [join $astk::agla(instance)]
      pack $fen.liste.agla.lbl $fen.liste.agla.inst -pady 3 -side left
   }

# langue
   pack [frame $fen.liste.langue -relief solid -bd 0] -anchor w
   label $fen.liste.langue.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 101] -width 35 -anchor w
   set MenuLang [tk_optionMenu $fen.liste.langue.choix astk::config(-1,langue) $ashare::llang(lang,0)]
   $MenuLang configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $MenuLang entryconfigure 0 -font $astk::ihm(font,labmenu)
   for {set j 1} {$j < $ashare::llang(nb_lang)} {incr j} {
      $MenuLang add radiobutton
      $MenuLang entryconfigure $j -label $ashare::llang(lang,$j) -font $astk::ihm(font,labmenu) -variable astk::config(-1,langue)
   }
   $fen.liste.langue.choix configure -font $astk::ihm(font,labmenu) -bg $astk::ihm(couleur,liste)
   pack $fen.liste.langue.lbl $fen.liste.langue.choix -pady 3 -side left

# préférences - version par défaut
   pack [frame $fen.liste.defvers -relief solid -bd 0] -anchor w
   label $fen.liste.defvers.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 92] -width 35 -anchor w
   entry $fen.liste.defvers.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,def_vers)
   pack $fen.liste.defvers.lbl $fen.liste.defvers.path -pady 3 -side left

# xterm
   pack [frame $fen.liste.xterm -relief solid -bd 0] -anchor w
   label $fen.liste.xterm.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 82] -width 35 -anchor w
   entry $fen.liste.xterm.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,xterm)
   pack $fen.liste.xterm.lbl $fen.liste.xterm.path -pady 3 -side left

# editeur
   pack [frame $fen.liste.editeur -relief solid -bd 0] -anchor w
   label $fen.liste.editeur.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 93] -width 35 -anchor w
   entry $fen.liste.editeur.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,editeur)
   pack $fen.liste.editeur.lbl $fen.liste.editeur.path -pady 3 -side left

# navigateur
   pack [frame $fen.liste.browser -relief solid -bd 0] -anchor w
   label $fen.liste.browser.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 363] -width 35 -anchor w
   entry $fen.liste.browser.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,browser)
   pack $fen.liste.browser.lbl $fen.liste.browser.path -pady 3 -side left

# nbre de fichiers rémanents
   pack [frame $fen.liste.nb_reman -relief solid -bd 0] -anchor w
   label $fen.liste.nb_reman.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 94] -width 35 -anchor w
   scale $fen.liste.nb_reman.scl -orient horizontal -length 200 -from 0 -to 10 -tickinterval 1 \
      -variable astk::config(-1,nb_reman) -font $astk::ihm(font,lab) -showvalue 0
   pack $fen.liste.nb_reman.lbl $fen.liste.nb_reman.scl -pady 3 -side left

# niveau de debug
   pack [frame $fen.liste.dbg -relief solid -bd 0] -anchor w
   label $fen.liste.dbg.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 36] -width 35 -anchor w
   scale $fen.liste.dbg.scl -orient horizontal -length 200 -from 0 -to 5 -tickinterval 1 \
      -variable astk::config(-1,dbglevel) -font $astk::ihm(font,lab) -showvalue 0
   pack $fen.liste.dbg.lbl $fen.liste.dbg.scl -pady 3 -side left

# unites logiques dans ETUDE
   pack [frame $fen.liste.listul -relief solid -bd 0] -anchor w
   label $fen.liste.listul.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 421] -width 35 -anchor w
   set MenuLang [tk_optionMenu $fen.liste.listul.choix choix_listul [ashare::mess ihm 414]]
   $MenuLang configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $MenuLang entryconfigure 0 -font $astk::ihm(font,labmenu)
   for {set j 1} {$j < $astk::ihm(listul,nbre)} {incr j} {
      $MenuLang add radiobutton
      set label [ashare::mess ihm [expr 414 + $j]]
      $MenuLang entryconfigure $j -label $label -font $astk::ihm(font,labmenu) -variable choix_listul
   }
   $fen.liste.listul.choix configure -font $astk::ihm(font,labmenu) -bg $astk::ihm(couleur,liste)
   pack $fen.liste.listul.lbl $fen.liste.listul.choix -pady 3 -side left


# bip
   pack [frame $fen.liste.bip -relief solid -bd 0] -anchor w -padx 20
   checkbutton $fen.liste.bip.cb -variable astk::config(-1,bip) -anchor w
   label  $fen.liste.bip.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 362]"
   pack $fen.liste.bip.cb $fen.liste.bip.lbl -pady 3 -side left

# options asjob
# titre
   pack [frame $fen.liste.tit2 -relief solid -bd 0] -anchor w
   label $fen.liste.tit2.lbl -font $astk::ihm(font,labbout) -text [ashare::mess ihm 142] -width 35 -anchor w
   pack $fen.liste.tit2.lbl -pady 3 -side left

# nbre de ligne
   pack [frame $fen.liste.nb_ligne -relief solid -bd 0] -anchor w
   label $fen.liste.nb_ligne.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 139] -width 35 -anchor w
   scale $fen.liste.nb_ligne.scl -orient horizontal -length 200 -from 50 -to 450 -tickinterval 100 -resolution 10 \
      -variable astk::config(-1,nb_ligne) -font $astk::ihm(font,lab) -showvalue 0
   pack $fen.liste.nb_ligne.lbl $fen.liste.nb_ligne.scl -pady 3 -side left

# frequence d'actualisation
   pack [frame $fen.liste.freq_actu -relief solid -bd 0] -anchor w
   label $fen.liste.freq_actu.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 140] -width 35 -anchor w
   scale $fen.liste.freq_actu.scl -orient horizontal -length 200 -from 1 -to 5 -tickinterval 1 \
      -variable astk::config(-1,freq_actu) -font $astk::ihm(font,lab) -showvalue 0
   pack $fen.liste.freq_actu.lbl $fen.liste.freq_actu.scl -pady 3 -side left

# ok
   pack [frame $fen.valid -relief solid -bd 0]
   button $fen.valid.annuler -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
       -bg $astk::ihm(couleur,annul) \
      -command "restaure_prefs ; destroy $fen ; grab release $fen"
   button $fen.valid.ok -font $astk::ihm(font,labbout) -text "Ok" \
       -bg $astk::ihm(couleur,valid) \
      -command "accept_pref $ashare::lang ; destroy $fen ; grab release $fen"
   pack $fen.valid.ok $fen.valid.annuler -side left -padx 10 -pady 5
   
   wm deiconify $fen
}

# reaction a Configuration/Preferences/Reseau
#################################################################
proc Opt_prefs_net { } {
   bckup_prefs

   set fen .fen_prefnet
   catch {destroy $fen}
   toplevel $fen
   wm withdraw $fen
   wm title $fen "[ashare::mess ihm 33] - [ashare::mess ihm 378]"
   wm transient $fen .
   grab set $fen

   pack [frame $fen.liste -relief solid -bd 1]

# paramètres réseau
# titre
   pack [frame $fen.liste.tit3 -relief solid -bd 0] -anchor w
   label $fen.liste.tit3.lbl -font $astk::ihm(font,labbout) -text [ashare::mess ihm 356] -width 35 -anchor w
   pack $fen.liste.tit3.lbl -pady 3 -side left

# ipdhcp ou nom du client
   pack [frame $fen.liste.ipdhcp -relief solid -bd 0] -anchor w
   if { $astk::config(-1,isdhcp) == 1} {
      label $fen.liste.ipdhcp.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 348]" -width 35 -anchor w
      entry $fen.liste.ipdhcp.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,ipdhcp)
   } else {
      label $fen.liste.ipdhcp.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 349]" -width 35 -anchor w
      entry $fen.liste.ipdhcp.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,nom_complet) -state disabled
   }
   pack $fen.liste.ipdhcp.lbl $fen.liste.ipdhcp.path -pady 3 -side left

# switch mode DHCP / hostname
   pack [frame $fen.liste.mode -relief solid -bd 0] -anchor w -padx 20
   checkbutton $fen.liste.mode.cb -variable astk::config(-1,isdhcp) -anchor w
   label  $fen.liste.mode.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 350]"
   pack $fen.liste.mode.cb $fen.liste.mode.lbl -pady 3 -side left

# domain name
   pack [frame $fen.liste.domain -relief solid -bd 0] -anchor w
   label $fen.liste.domain.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 322] (ex.: domain.org)" -width 35 -anchor w
   entry $fen.liste.domain.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,nom_domaine)
   pack $fen.liste.domain.lbl $fen.liste.domain.path -pady 3 -side left

# forced display variable
   pack [frame $fen.liste.displ -relief solid -bd 0] -anchor w
   label $fen.liste.displ.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 376]" -width 35 -anchor w
   entry $fen.liste.displ.path -width 30 -font $astk::ihm(font,val) -textvariable astk::config(-1,forced_display)
   pack $fen.liste.displ.lbl $fen.liste.displ.path -pady 3 -side left

# remote shell protocol
   pack [frame $fen.liste.proto1 -relief solid -bd 0] -anchor w
   label $fen.liste.proto1.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 354]" -width 35 -anchor w
   radiobutton $fen.liste.proto1.rsh -font $astk::ihm(font,val) -text rsh -value RSH -variable astk::config(-1,remote_shell_protocol)
   radiobutton $fen.liste.proto1.ssh -font $astk::ihm(font,val) -text ssh -value SSH -variable astk::config(-1,remote_shell_protocol)
   pack $fen.liste.proto1.lbl $fen.liste.proto1.rsh $fen.liste.proto1.ssh -pady 3 -side left

# remote copy protocol
   pack [frame $fen.liste.proto2 -relief solid -bd 0] -anchor w
   label $fen.liste.proto2.lbl -font $astk::ihm(font,lab) -text "[ashare::mess ihm 355]" -width 35 -anchor w
   radiobutton $fen.liste.proto2.rcp -font $astk::ihm(font,val) -text rcp -value RCP -variable astk::config(-1,remote_copy_protocol)
   radiobutton $fen.liste.proto2.scp -font $astk::ihm(font,val) -text scp -value SCP -variable astk::config(-1,remote_copy_protocol)
   pack $fen.liste.proto2.lbl $fen.liste.proto2.rcp $fen.liste.proto2.scp -pady 3 -side left

# ok
   pack [frame $fen.valid -relief solid -bd 0]
   button $fen.valid.annuler -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
       -bg $astk::ihm(couleur,annul) \
      -command "restaure_prefs ; destroy $fen ; grab release $fen"
   button $fen.valid.ok -font $astk::ihm(font,labbout) -text "Ok" \
       -bg $astk::ihm(couleur,valid) \
      -command "accept_pref $ashare::lang ; destroy $fen ; grab release $fen"
   pack $fen.valid.ok $fen.valid.annuler -side left -padx 10 -pady 5
   
   wm deiconify $fen
}

# Acceptation des préférences
#################################################################
proc accept_pref { old_lang } {
   global old_pref
   global choix_listul

# nom de machine / domaine
   init_nom_complet $astk::config(-1,isdhcp)

   if { $astk::config(-1,ipdhcp) != $old_pref(-1,ipdhcp)
     || $astk::config(-1,isdhcp) != $old_pref(-1,isdhcp)
     || $astk::config(-1,nom_complet) != $old_pref(-1,nom_complet) } {
      check_nom_complet
   }
# display
   init_display
   check_display
# niveau debug
   set ashare::dbg $astk::config(-1,dbglevel)
# filtre les types d'unité logique
   set astk::config(-1,listul) $astk::ihm(listul,$choix_listul)
   affiche_onglet $astk::profil(onglet_actif)
# info en cas de changement de la langue
   if { $astk::config(-1,langue) != $old_lang } {
      set ashare::lang $astk::config(-1,langue)
      raffr_princ
   }
   # repositionne les labels de choix
   init_choix_ul
# mise à jour de la liste des profils (nb_reman)
   maj_prof
# sauvegarde des préférences
   ashare::save_prefs
}

# backup des préférences avant modif + init variables globales
#################################################################
proc bckup_prefs {} {
   global old_pref
   global choix_listul

   set choix_listul $astk::ihm(listul,reverse,default)
   catch { set choix_listul $astk::ihm(listul,reverse,$astk::config(-1,listul)) }

# valeurs actuelles dans old_pref
   set mots_cles $ashare::mots(MCS_pref)
   lappend mots_cles "nom_complet"
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set old_pref(-1,$mcs) $astk::config(-1,$mcs)
   }
}

#################################################################
proc restaure_prefs {} {
   global old_pref

# récupère depuis old_pref
   set mots_cles $ashare::mots(MCS_pref)
   lappend mots_cles "nom_complet"
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set astk::config(-1,$mcs) $old_pref(-1,$mcs)
   }
}
