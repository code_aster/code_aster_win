#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: preferences.tcl 753 2004-12-06 08:48:48Z mcourtoi $

#################################################################
proc ashare::init_prefs {} {
# lecture des préférences utilisateur
   set iret [ ashare::lire_prefs ]
   if { $ashare::dbg >= 4 } {
      ashare::log "lire_prefs : Code retour = $iret" }
   if { $iret != 0 } {
      if { $iret == "FICH_NON_TROUVE" } {
         ashare::mess "info" 4 $astk::fic_prefs
         ashare::lire_prefs CREE_ASTKRC
      } elseif { $iret == "PB_astkrc_version" } {
         ashare::lire_prefs PB_astkrc_version
      } else {
         ashare::mess "erreur" 1 "lire_prefs"
         ashare::my_exit 4
      }
      ashare::save_prefs
   }
# on positionne la couleur et la taille de font
   set astk::ihm(style,couleur) $astk::config(-1,couleur)
   set lf $astk::ihm(style,fontlist)
   for {set i 0} {$i < [llength $lf]} {incr i} {
      set ff [lindex $lf $i]
      set astk::ihm(style,font_$ff) $astk::config(-1,font_$ff)
   }
# restaure les tailles de fenetres
   for {set i 0} {$i < [llength $astk::ihm(style,geomlist)]} {incr i} {
      set fen [lindex $astk::ihm(style,geomlist) $i]
      set astk::ihm(style,Geom_$fen) $astk::config(-1,Geom_$fen)
   }

# vérifie que la langue choisie est connue
   for {set i 0} {$i < $ashare::llang(nb_lang)} {incr i} {
      if { $astk::config(-1,langue) == $ashare::llang(lang,$i) } {
         set ashare::lang $astk::config(-1,langue)
         break
      }
   }

# valeurs par défaut
   if { $astk::config(-1,isdhcp) == "" } {
      set astk::config(-1,isdhcp) 0
   }
   if { $astk::config(-1,listul) == "" } {
      set astk::config(-1,listul) default
   }
   if { [lsearch -exact {RSH SSH} $astk::config(-1,remote_shell_protocol)] < 0 } {
      set astk::config(-1,remote_shell_protocol) SSH
   }
   if { [lsearch -exact {RCP SCP RSYNC} $astk::config(-1,remote_copy_protocol)] < 0 } {
      set astk::config(-1,remote_copy_protocol) SCP
   }

# initialisation des unités logiques, filtre des types
   init_choix_ul
   def_ul
   # tout remplir au moins une fois
   filtre_listul default
   filtre_listul
}

# Lecture des préférences utilisateur
#################################################################
proc ashare::lire_prefs { { ini NON } } {
# ini=CREE_ASTKRC, on lit dans [..]/astkrc/ : initialisation de l'environnement
# ini=NON, on lit dans ~/.astkrc/
   if { $ini == "PB_astkrc_version" } {
      ashare::mess "erreur" 4 $astk::fic_prefs.old
      file rename -force $astk::fic_prefs $astk::fic_prefs.old
      set ini CREE_ASTKRC
   }
   if { $ini == "CREE_ASTKRC" } {
      set fich [file join $ashare::astkrc_ref prefs]
   } else {
        set ini NON
      set fich $astk::fic_prefs
   }
   if { $ashare::dbg >= 3 } {
      ashare::mess "info" 9 "Lecture" $fich }

   # phase de vérif
   # mots-clés reconnus
   set mots_cles $ashare::mots(MCS_pref)
   set pmcs [llength $mots_cles]

   # les mots-clés sont facultatifs, on initialise à _VIDE
   for {set j 0} {$j < $pmcs} {incr j} {
      set mm [lindex $ashare::mots(MCS_pref) $j]
      set astk::config(-1,$mm) ""
   }
   ashare::init_default_prefs

   set iret [ashare::lire_mc_val $fich mots vale nlu ]
   if { $iret != 0 } {
      return $iret
   }

   set ashare::mots(MCS_pref_ign) {}
   set nbmcs 0
   set nberr 0
   for {set j 1} {$j <= $nlu} {incr j} {
      #astkrc_version
      if { $mots($j) == "astkrc_version" } {
         if { $vale($j) != "$ashare::astkrc_version" } {
            ashare::mess "erreur" 2 $fich
            return PB_astkrc_version
         }
      } else {
         set iv [lsearch -exact $mots_cles $mots($j)]
         if { $iv < 0 } {
            # mot clé inconnu : ignoré
            #ashare::mess "erreur" 5 $mots($j)
            #incr nberr
            # save keywords/values not used by astk (but by as_run)
            # to be able to save them later.
            set ind [llength $ashare::mots(MCS_pref_ign)]
            lappend ashare::mots(MCS_pref_ign) $mots($j)
            set astk::config(ignprefs,$mots($j),$ind) $vale($j)
         } else {
            incr nbmcs
            set astk::config(-1,$mots($j)) $vale($j)
         }
      }
   }
   if { $nberr > 0 } {
      ashare::mess "erreur" 8 $fich
      ashare::my_exit 4
   }
   return 0
}

# Sauvegarde des préférences utilisateur
#################################################################
proc ashare::save_prefs { } {
   set id [open $astk::fic_prefs w]
   puts $id "# AUTOMATICALLY GENERATED"
   puts $id "# You can edit this file but comments are lost when it is saved by astk."
   puts $id "astkrc_version : $ashare::astkrc_version"
   puts $id "#"
   set mots_cles $ashare::mots(MCS_pref)
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set val ""
      catch { set val $astk::config(-1,$mcs) }
      if { $val == "" } {
         set val "_VIDE"
      }
      set val [unexpandvars $val]
      puts $id "$mcs : $val"
   }
   puts $id "#"
   puts $id "# Following values are not used by codeaster-gui (astk)"
   puts $id "# but may be useful to as_run."
   puts $id "# (this replace the old ~/.astkrc/config file)"
   puts $id "#"
   set mots_cles $ashare::mots(MCS_pref_ign)
   for { set k 0 } { $k < [ llength $mots_cles ] } { incr k } {
      set mcs [lindex $mots_cles $k]
      set val ""
      catch { set val $astk::config(ignprefs,$mcs,$k) }
      if { $val == "" } {
         set val "_VIDE"
      }
      set val [unexpandvars $val]
      puts $id "$mcs : $val"
   }
   close $id
   return 0
}

# initialisation des valeurs par défaut (non vides) des préférences
#################################################################
proc ashare::init_default_prefs { } {
   set astk::config(-1,dbglevel) 1
   set astk::config(-1,freq_actu) 1
   set astk::config(-1,nb_ligne) 50
   set astk::config(-1,nb_reman) 5
}

# ajoute l'event pour capter la géométrie d'une fenetre
# force sa taille (appelé à la construction)
# après un deiconify
#################################################################
proc ashare::trace_geom { nom fen } {
   update idletasks
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> Geom_$nom : $astk::ihm(style,Geom_$nom)"
   }
    # force la taille de la fenetre
   if { $astk::ihm(style,Geom_$nom) != "" } {
      wm withdraw $fen
      wm geometry $fen $astk::ihm(style,Geom_$nom)
      wm deiconify $fen
   }
   bind $fen <Configure> "ashare::update_geom $nom $fen"
}

# Récupère la géométrie d'une fenêtre
#################################################################
proc ashare::update_geom { nom fen } {
   set inf [winfo geometry $fen]
   # special pour asjob : il faut la taille "en caractères"
   # sinon la fenêtre se maximise...
   if { $nom == "asjob" && \
        [regexp -- {^([0-9]+x[0-9]+)([\+\-][0-9]+[\+\-][0-9]+)} \
                $inf mat1 taille pos] } {
      set inf "$asjob::size(root)$pos"
   }
   if { $inf != $astk::ihm(style,Geom_$nom) } {
      set astk::ihm(style,Geom_$nom) $inf
      if { $ashare::dbg >= 5 } {
         ashare::log "<DEBUG> Info geom de $fen : $inf"
      }
   }
}

# Sauvegarde la géométrie des fenêtres
#################################################################
proc ashare::save_geom { } {
   for {set i 0} {$i < [llength $astk::ihm(style,geomlist)]} {incr i} {
      set fen [lindex $astk::ihm(style,geomlist) $i]
      set astk::config(-1,Geom_$fen) $astk::ihm(style,Geom_$fen)
   }
   ashare::save_prefs
}
