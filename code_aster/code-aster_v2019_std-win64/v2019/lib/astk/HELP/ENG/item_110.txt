--------------------------------------------------------------------------------
Introduction to astk

astk is the interface for launching Code_Aster.  It is written in Tcl/Tk, from where its name (Aster + Tk) was derived.

It is a study manager, which can launch Aster calculations on a local machine or a remote server in interactive or batch mode.


--------------------------------------------------------------------------------
Multi-machines?

That means the files used for the studies can be on the local machine or on a remote server.

- the machine called "Local" corresponds to the machine from which astk was started. Its properties are defined among the preferences of the user (Menu Configuration/Interface). If Code_Aster is installed on this machine, a server should be added having the IP address of this machine.

- "servers" are file servers as well as servers of Aster calculations.  It is necessary to add the servers on which you wish to carry out Aster (the server part of astk must be installed there, directory ASTK_SERV), and the servers on which your files are located.

The protocols of exchange supported between the servers and the local machine are:

    - rsh or ssh (default) for the execution of commands,
    - rcp or scp (default) (default) for copying files.
   The use of rsh & rcp are not recommended on an open network for security reasons.

See Help/File/Configuration/Tools menus for more details on the configuration of servers.


--------------------------------------------------------------------------------
Operational logic

The files necessary for launching Aster calculations are provided under the tab "STUDY".  Using the icons located on this line, one can insert lines in the list, add a new file, remove it or publish it. The type corresponding to the Aster type (with its associated logical unit).

"D" must be selected if the file/directory is data,
"R" if it is a result.
"C" to specify that the file (or files contained in the directory) must be de/compressed with gzip (this is useful for the databases or bulky result files).

To carry out an Aster code run, it is necessary to provide the sources under the tab "SURCHARGE/OVERLOADS" (FORTRAN, C, catalogs...) in the upper part, and the products of this overlay in the lower part (executable, catalogs compiled...).

The tab "TESTS" provides a list of tests to launch, by possibly providing additional tests.

Lastly, so that the contents of a button are acted upon, it should be selected (pressed in)!


Contributors : JMB (2007-2008)
