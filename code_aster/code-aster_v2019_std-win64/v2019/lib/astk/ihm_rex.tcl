#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_rex.tcl 3577 2008-10-24 12:03:28Z courtois $

# choix d'un item du menu rex
#################################################################
proc choix_rex { ssongl } {
   global rex_liste
   switch -exact -- $ssongl {
      rex { }
      emis_sans {
         # emission d'une fiche sans prendre en compte le profil
         if { [rex_verif_identite] } {
            rex_saisie_fiche 0
         }
      }
      emis_prof {
         # emission d'une fiche avec export du profil et récupération des fichiers
         if { [rex_verif_identite 1] } {
            rex_saisie_fiche 1
         }
      }
      consult {
         # adresse du REX
         if { $astk::agla(rex_url) == "" } {
            set msg [ashare::mess info 42]
            tk_messageBox -message $msg -type ok -icon info
            change_status $msg
         }
         ashare::file_open_url $astk::agla(rex_url)
      }
      default {
         ashare::mess erreur 5 $ssongl
      }
   }
}

# verifie que l'identité a été renseignée
# retourne 1 si ok, 0 sinon
#################################################################
proc rex_verif_identite { {fich_asso 0} } {
   if { $astk::config(-1,nom_user) == "" || [regexp {Firstname} $astk::config(-1,nom_user)]
     || $astk::config(-1,email) == "" || $astk::config(-1,email) == "address@mail.com" } {
      tk_messageBox -message [ashare::mess ihm 248] -type ok -icon info
      return 0
   }
   if { $fich_asso == 1 } {
      if { $astk::profil(surcharge) == "oui" } {
         tk_messageBox -message [ashare::mess ihm 299] -type ok -icon error
         return 0
      }
   }
   return 1
}

# saisie d'une fiche (fich_asso=1 si fichiers associés,  0 sinon)
#################################################################
proc rex_saisie_fiche { fich_asso } {
   global rex_ftype rex_fcmde bid_typfic
   set fen .fen_rex_fiche
   catch {destroy $fen}
   toplevel $fen
   wm withdraw $fen
   wm title $fen "[ashare::mess ihm 11]"
   wm transient $fen .

   # fiche
   pack [frame $fen.f -relief flat -bd 0] -fill x -padx 5 -pady 5
   # entete
   label $fen.f.em -font $astk::ihm(font,labbout) -text [ashare::mess ihm 216] -anchor w
   pack $fen.f.em -fill x -expand yes

   pack [frame $fen.f.f1a -relief flat -bd 0] -anchor w -padx 5
   label $fen.f.f1a.lab -font $astk::ihm(font,lab) -text "[ashare::mess ihm 89]*" -width 30 -anchor w
   label $fen.f.f1a.val -font $astk::ihm(font,lab) -text $astk::config(-1,nom_user)
   pack $fen.f.f1a.lab $fen.f.f1a.val -side left -padx 10

   pack [frame $fen.f.f1b -relief flat -bd 0] -anchor w -padx 5
   label $fen.f.f1b.lab -font $astk::ihm(font,lab) -text "[ashare::mess ihm 90]*" -width 30 -anchor w
   label $fen.f.f1b.val -font $astk::ihm(font,lab) -text $astk::config(-1,email)
   pack $fen.f.f1b.lab $fen.f.f1b.val -side left -padx 10

   if { $astk::config(-1,org) == "" } {
      set astk::config(-1,org) [ashare::mess ihm 239]
   }
   pack [frame $fen.f.f1f -relief flat -bd 0] -anchor w -padx 5
   label $fen.f.f1f.lab -font $astk::ihm(font,lab) -text "[ashare::mess ihm 240]*" -width 30 -anchor w
   label $fen.f.f1f.val -font $astk::ihm(font,lab) -text $astk::config(-1,org)
   pack $fen.f.f1f.lab $fen.f.f1f.val -side left -padx 10

   label $fen.f.mod -font $astk::ihm(font,labpt) -text "(*) [ashare::mess ihm 235]"
   pack $fen.f.mod -anchor w -padx 45

   # paramètres
   label $fen.f.pa -font $astk::ihm(font,labbout) -text [ashare::mess ihm 221] -anchor w
   pack $fen.f.pa -fill x -expand yes

   pack [frame $fen.f.f1c -relief flat -bd 0] -anchor w -padx 5
   # type de fiche
   label $fen.f.f1c.lab -font $astk::ihm(font,lab) -text [ashare::mess ihm 217] -width 30 -anchor w
   set ltyp {218 219 236 237 220 238}
   set labb {AL EL AO EO AOM ED}
   set nb [llength $ltyp]
   set rex_ftype ""
   set lab [ashare::mess ihm 227]
   set bid_typfic $lab
   set MenuCor [tk_optionMenu $fen.f.f1c.val bid_typfic $lab]
   $MenuCor configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $MenuCor entryconfigure 0 -font $astk::ihm(font,labmenu) -command {set rex_ftype ""}
   for {set j 0} {$j < $nb} {incr j} {
      set lab [ashare::mess ihm [lindex $ltyp $j]]
      $MenuCor add radiobutton
      $MenuCor entryconfigure [expr $j+1] -label $lab -font $astk::ihm(font,labmenu) -variable bid_typfic -command "set rex_ftype [lindex $labb $j]"
   }
   pack $fen.f.f1c.lab $fen.f.f1c.val -side left -padx 10 -pady 1

   set rex_fcmde ""
   pack [frame $fen.f.f1d -relief flat -bd 0] -anchor w -padx 5
   label $fen.f.f1d.lab -font $astk::ihm(font,lab) -text [ashare::mess ihm 222] -width 30 -anchor w
   entry $fen.f.f1d.val -font $astk::ihm(font,lab) -textvariable rex_fcmde -width 55
   pack $fen.f.f1d.lab $fen.f.f1d.val -side left -padx 10 -pady 1

   pack [frame $fen.f.f1h -relief flat -bd 0] -anchor w -padx 5
   label $fen.f.f1h.lab -font $astk::ihm(font,lab) -text [ashare::mess ihm 46] -width 30 -anchor w
   label $fen.f.f1h.val -font $astk::ihm(font,lab) -text $astk::profil(version)
   pack $fen.f.f1h.lab $fen.f.f1h.val -side left -padx 10 -pady 1

   # fichiers associés
   pack [frame $fen.f.f1e -relief flat -bd 0] -anchor w -padx 5
   if { $fich_asso } {
      append val [ashare::mess ihm 224]
   } else {
      append val [ashare::mess ihm 225]
   }
   label $fen.f.f1e.lab -font $astk::ihm(font,lab) -text [ashare::mess ihm 223] -width 30 -anchor w
   label $fen.f.f1e.val -font $astk::ihm(font,lab) -text $val
   pack $fen.f.f1e.lab $fen.f.f1e.val -side left -padx 10 -pady 1

   # texte
   pack [frame $fen.f2 -relief flat -bd 0] -fill both -expand yes -padx 5 -pady 5

   text $fen.f2.tx -xscrollcommand "$fen.f2.scrollx set" -yscrollcommand "$fen.f2.scrolly set" \
   -height 20 -width 80 -font $astk::ihm(font,zonfix) -bg $astk::ihm(couleur,entry_background) -wrap word
   scrollbar $fen.f2.scrolly -command "$fen.f2.tx yview"
   scrollbar $fen.f2.scrollx -command "$fen.f2.tx xview" -orient h
   pack $fen.f2.scrolly -side right  -fill y
   pack $fen.f2.scrollx -side bottom -fill x
   pack $fen.f2.tx -expand 1 -fill both

   # ok / annuler
   pack [frame $fen.valid -relief solid -bd 0] -pady 15
      button $fen.valid.annuler -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
         -command "destroy $fen" -bg $astk::ihm(couleur,annul)
      button $fen.valid.ok -font $astk::ihm(font,labbout) -text [ashare::mess ihm 215] \
         -command "rex_envoi $fen $fich_asso" -bg $astk::ihm(couleur,valid)
      pack $fen.valid.ok $fen.valid.annuler -side left -padx 10 -pady 5

   wm deiconify $fen
}

# envoi de la fiche
#  parent : fenetre parent
#  fich_asso=1 si fichiers associés, 0 sinon
#################################################################
proc rex_envoi { parent fich_asso } {
   global rex_ftype rex_fcmde
   # phase de vérification
   if { $rex_ftype == "" } {
      set msg [ashare::mess ihm 228]
      change_status $msg
      tk_messageBox -message $msg -type ok -icon info -parent $parent
      return
   }

   set fexp [get_tmpname .file_rex_export]
   # l'extension est obligatoire au moment de l'export
   append fexp ".export"
   set serv $astk::agla(num_serv)
   if { $fich_asso } {
       set astk::profil(special) ""
       # on force la sélection du serveur AGLA pour l'export
       adapt_satellite 2
       set iret [exporter astk_serv -1 $fexp "verif" "oui"]
       adapt_satellite 0
       if { $iret != 0 } {
          set msg [ashare::mess erreur 40]
          change_status $msg
          tk_messageBox -message $msg -type ok -icon info -parent $parent
          return
       }
   } else {
      set idexp [open $fexp w]
      write_server_infos $idexp $serv
      close $idexp
   }

   set frex [get_tmpname .file_rex_fiche]
   set idrex [open $frex w]
   puts $idrex "@NOMUSER@"
   regsub -all " " $astk::config(-1,nom_user) "\." tmp
   puts $idrex $tmp
   puts $idrex "@FINNOMUSER@"
   puts $idrex "@MAILUSER@"
   puts $idrex "$astk::config(-1,email)"
   puts $idrex "@FINMAILUSER@"
   puts $idrex "@UNIUSER@"
   regsub -all " " $astk::config(-1,org) "\." tmp
   puts $idrex $tmp
   puts $idrex "@FINUNIUSER@"
   puts $idrex "@TITRE@"
   puts $idrex "$rex_fcmde"
   puts $idrex "@FINTITRE@"
   puts $idrex "@DATE1@"
   puts $idrex [clock format [clock seconds] -format "%d/%m/%Y"]
   puts $idrex "@FINDATE1@"
   puts $idrex "@VERSION@"
   puts $idrex "$astk::profil(version)"
   puts $idrex "@FINVERSION@"
   puts $idrex "@TYPFIC@"
   puts $idrex "$rex_ftype"
   puts $idrex "@FINTYPFIC@"
   puts $idrex "@TEXTE@"
   puts $idrex [$parent.f2.tx get 1.0 end]
   puts $idrex "@FINTEXTE@"
   if { $fich_asso } {
      puts $idrex "@FICASS@"
      puts $idrex "F"
      puts $idrex "@FINFICASS@"
   }
   close $idrex

# préparation de la ligne de commande à exécuter
   set lcmd ""
   set argu ""
   append lcmd [file join $ashare::prefix "bin" as_run]
   append lcmd " --proxy --create_issue --schema=[get_schema $serv create_issue]"
   append lcmd [ashare::get_glob_args]
   append argu $frex
   append argu " "
   append argu $fexp
#  execution
   set iret [ ashare::rexec_cmd -1 astk::config $lcmd $argu 0 out $parent progress]
   if { $iret == 0 } {
      if { [regexp {INDEX[ ]*=[ ]*([0-9]+)} $out mat1 index] } {
         set msg [ashare::mess ihm 226 $index]
         change_status $msg
      } else {
         ashare::mess "erreur" 3 "as_rex_creer" 99 $out
      }
   } else {
      ashare::mess "erreur" 3 "as_rex_creer" $iret $out
      set msg [ashare::mess ihm 60 $iret]
      change_status $msg
      tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info -parent $parent
   }

   # ferme la fenetre d'emission
   destroy $parent
}
