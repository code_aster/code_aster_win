#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# SCRIPT PRINCIPAL : bsf.tcl

# $Id: bsf.tcl 2308 2006-11-02 13:54:50Z courtois $

# on cache la fenêtre en attendant la construction
wm withdraw .

set root [file dirname [info script]]
source [file join $root init_share.tcl]
source [file join $root init.tcl]

set ashare::appli "bsf"
init_gene $root
unset root

# pointeur off sert surtout à faire quelque chose sur "." pour qu'elle
# apparaisse avant la fenetre de log
ashare::pointeur off
ashare::init_log ".bsf"

# initialisation des mots clés des différents fichiers
init_mots

# initialisation des messages erreur/info/ihm
ashare::lire_msg [file join $ashare::root $ashare::fic_msg]

# préférences utilisateur
ashare::init_prefs
init_icon $astk::config(-1,theme)
set ashare::dbg $astk::config(-1,dbglevel)

set msg [ashare::mess "info" 21 $astk::astk_version]
puts $msg

# initialisation des couleurs, fonts...
init_couleur
init_font

# verifications
check_conf $astk::check_quit

if { $ashare::dbg } {
   ashare::mess "info" 2 $ashare::dbg }

# initialisation des ressources
init_env

# arguments
set args "-bsf"

# init
init_sel

# serveur et répertoire initial
set inisrv -1
set inidir [pwd]

# initialisations finies
set ashare::fen_log 2

# traitement des arguments
#################################################################
for {set i 0} {$i < $argc} {incr i} {
    switch -exact -- [lindex $argv $i] {
        -h -
      --help {
         set ashare::fen_log 0
         ashare::mess "info" 22
         ashare::my_exit 1
        }
        -g -
      --debug {
            append args " -debug"
         set ashare::dbg 1
            if { $i < [ expr $argc - 1 ] } {
                incr i
                if { [string equal [string index [lindex $argv $i] 0] - ] } {
                    incr i -1
                } else {
                    set ashare::dbg [lindex $argv $i]
                }

            }
        }
       --version {
          ashare::my_exit 0
       }
       --inisrv {
            if { $i < [ expr $argc - 1 ] } {
                incr i
            set inisrv [lindex $argv $i]
            if { [valid_serveur $inisrv "" nom] != -999 } {
               set inisrv $astk::inv(serv,$inisrv)
            }
            }
       }
       --inidir {
            if { $i < [ expr $argc - 1 ] } {
                incr i
            set inidir [lindex $argv $i]
            }
       }
        --stdout {
            set ashare::fen_log 0
         destroy $astk::ihm(log)
        }
      default {
         if { $i == [ expr $argc - 1 ] } {
            set inidir [lindex $argv $i]
            if { [regexp {(.+):(.*)} $inidir mat1 srv dir] } {
               set inisrv $srv
               set inidir $dir
               if { [valid_serveur $inisrv "" nom] != -999 } {
                  set inisrv $astk::inv(serv,$inisrv)
               }
            }
         } else {
            ashare::mess "erreur" 11 [lindex $argv $i]
         }
      }
    }
}

catch {destroy .fen_about}

# lancement bsf
ashare::selecteur srv fich typ $inisrv $inidir "FR" $args

ashare::my_exit 0
