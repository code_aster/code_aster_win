#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: convbase.tcl 3255 2008-04-10 17:13:17Z courtois $


# Vérifie que les données nécessaires sont présentes
# puis appel le service "as_run --serv"
# numfich pour compatibilité avec les run_$outil
#################################################################
proc run_ConvBase { {numfich -1} } {
   global convbase_para
   
   # base IN sélectionnée
   set lfich $astk::sel(filename)
   if { [llength $lfich] != 1 } {
      return 50
   } else {
      set indice $astk::sel(indice)
      set var $astk::sel(liste)
   }
   set typ $astk::profil($var,fich,$indice,type)
   if { $typ != "base" } {
      return 51
   }
   
   set nom [abspath $var $astk::profil($var,fich,$indice,nom)]
   set basout [file join [file dirname $nom] "bhdf"]
   set servbase $astk::profil($var,fich,$indice,serv)
   
   set convbase_para(base_in)         $nom
   set convbase_para(compress_result) 1
   set convbase_para(base_out)        $basout
      
   # on renseigne les deux lignes du .export : base IN et bhdf OUT
   set astk::profil(special) "convbase%NEXT%"

   # chemin des bases IN et OUT
   set valf ""
   set valout ""
   set sfic $astk::inv(serv,$astk::profil($var,fich,$indice,serv))
   set serv $astk::inv(serv,$astk::profil(serveur))
   if { [ashare::meme_machine $astk::config($sfic,nom_complet) $astk::config($serv,nom_complet)] != 1
     || $astk::config($sfic,login) != $astk::config($serv,login) } {
      append valf   "$astk::config($sfic,login)@$astk::config($sfic,nom_complet):"
      append valout "$astk::config($sfic,login)@$astk::config($sfic,nom_complet):"
   }
   append valf   $nom
   append valout $basout
   # enlever les /./
   regsub -all {/\./} $valf   "/" valf
   regsub -all {/\./} $valout "/" valout

   set flagIN " D"
   if { $astk::profil($var,fich,$indice,compress) } {
      append flagIN "C"
   }
   set flagOUT " R"
   if { $convbase_para(compress_result) == 1 } {
      append flagOUT "C"
   }
   
   # remplit special
   append astk::profil(special) "R $typ $valf $flagIN 0"
   append astk::profil(special) "%NEXT%"
   append astk::profil(special) "R bhdf $valout $flagOUT 0"
      
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (run_ConvBase) special : $astk::profil(special)"
   }
# on force le suivi interactif
   set astk::profil(suivi_interactif) 1
# enregistrement du profil
   set iret [enregistrer $astk::profil(serv_profil) $astk::profil(nom_profil)]
   if { $iret != 0 } {
      return 5
   }
# indices ihm
   set serv $astk::inv(serv,$astk::profil(serveur))
# nom du fichier export
   set serv_export $astk::profil(serv_profil)
   set nom_export [file rootname $astk::profil(nom_profil)]
   append nom_export "_convbase.export"
# export du profil sans les vérifications supplémentaires
   set iret [exporter astk_serv $serv_export $nom_export "noverif" "non"]
   if { $iret == 4 } {
      return $iret
   } elseif { $iret == 2 } {
   # alarmes emises
      change_status [ashare::mess info 16]
      tk_messageBox -message [ashare::mess info 16] -type ok -icon info
   }
   set astk::profil(special) ""

# préparation de la ligne de commande à exécuter
   set lcmd ""
   set argu ""
# profil
   append lcmd [file join $ashare::prefix "bin" as_run]
   append lcmd " --proxy --serv --schema=[get_schema $serv serv]"
   append lcmd [ashare::get_glob_args]
   if { [is_localhost_serv $serv_export] == 0 } {
      append argu "$astk::config($serv_export,login)@$astk::config($serv_export,nom_complet):"
   }
   append argu $nom_export

#  execution
   set iret [ ashare::rexec_cmd -1 astk::config $lcmd $argu 0 out . progress]
   set jret $iret
   if { $iret == 0 } {
   # ajouter la bhdf résultat dans le profil si pas déjà présente
      set trouv 0
      for { set i 0 } {$i < $astk::profil($var,nbfic)} {incr i} {
         if { [abspath $var $basout] == [abspath $var $astk::profil($var,fich,$i,nom)] } {
            set trouv 1
            break
         }
      }
      if { $trouv == 0 } {
         $astk::ihm(fenetre).active.princ.icone.nouveau invoke
         set i [expr $astk::profil($var,nbfic) - 1]
         
         set astk::profil($var,fich,$i,nom)  [relpath $var $basout]
         set astk::profil($var,fich,$i,type) "bhdf"
         set astk::profil($var,fich,$i,FR)   "R"
         set astk::profil($var,fich,$i,UL)   0
         # ne doit pas être pris en compte dans l'export sans intervention de l'utilisateur
         set astk::profil($var,fich,$i,donnee)   0
         set astk::profil($var,fich,$i,resultat) 0
         set astk::profil($var,fich,$i,compress) $convbase_para(compress_result)
      }
      # nouvellement inséré ou pas, on met le serveur à jour
      set astk::profil($var,fich,$i,serv) $servbase
   
   # traitement du retour
      set nomjob [get_nomjob]
      append nomjob "_convbase"
      set jret [retour_as_exec $nomjob $out]
#      tk_messageBox -message $msg -type ok -icon info
      show_fen $astk::ihm(asjob)
   
   } else {
   # pb lancement
      ashare::mess "erreur" 3 $lcmd $jret $out
      return "ConvBase"
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (run_ConvBase) iret : $iret, output :\n$out"
      catch { ashare::log "<DEBUG> (run_ConvBase) jobid=$jobid\nqueue=$queue" }
   }
   
   return $jret
}
