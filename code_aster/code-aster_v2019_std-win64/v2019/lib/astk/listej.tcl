#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################
# $Id: listej.tcl 2075 2006-08-31 16:39:31Z courtois $

# Gestion les listes de paramètres associés aux jobs

#### ==========
proc lire_job {a ficsauv} {
#### ==========
   upvar 1 $a listej
#
   set i 1
   array unset listej
   if [file exist $ficsauv] {
     set idin [open $ficsauv r]   
     while { [eof $idin] != 1 } {
       gets $idin line
       if {[llength $line] > 0} {
         set num [lindex $line 0]
         if { [llength $line] != $asjob::maxarg } {
            ashare::mess erreur 11
            for {set j [llength $line]} {$j < $asjob::maxarg} {incr j} {
               append line " UNKNOWN"
            }
         }
         array set listej [list $num [concat $line $i]]
         incr i
       }
     }
     close $idin
   }
   return [array size listej]
}
#### ==========
proc sauv_job {a ficsauv} {
#### ==========
   upvar 1 $a listej
#
   set i -1
   if { [array size listej] >= 0 } {
     set id [array startsearch listej]
     set l_val " "
     for {set i 0} {$i < [array size listej]} {incr i} {
        set val [lindex [array get listej [array nextelement listej $id]] 1]
        if {$i == 0} { set l_val [list $val]
        } else {       set l_val [linsert $l_val end $val]
        }
     }
     array donesearch listej $id
     set l_val [lsort -integer -index $asjob::maxarg $l_val]   
     set idout [open $ficsauv w+]    
     for {set i 0} {$i < [llength $l_val]} {incr i} { 
       set line [lindex $l_val $i]
       set enrg " "
       for {set j 0} {$j < [llength $line]-1} {incr j} {
          append enrg [format [lindex $asjob::fmt $j] [lindex $line $j]] " "
       }
       puts $idout $enrg
     }
     close $idout
   }
   return $i
}
#### ==========
proc impr_job {a} {
#### ========== 
   upvar 1 $a listej
#
   if { [array size listej] > 0 } {
     set id [array startsearch listej]
     set l_val " "
     for {set i 0} {$i < [array size listej]} {incr i} {
        set val [lindex [array get listej [array nextelement listej $id]] 1]
        if {$i == 0} { set l_val [list $val]
        } else {       set l_val [linsert $l_val end $val]
        }
     }
     array donesearch listej $id
     set l_val [lsort -integer -index $asjob::maxarg $l_val]   
     for {set i 0} {$i < [llength $l_val]} {incr i} { 
       ashare::log "$i --> [lindex $l_val $i]"
     }
   }
}
#### ==========
proc delete_job {a num} {
#### ==========
# 
# Détruit les informations associées à un job (num) dans une liste ordonnée de jobs
   upvar 1 $a listej
#
   if { [llength [array names listej $num]] != 0 } {
     array unset listej $num
   }
   set i [array size listej]
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (delete_job) numero $i"
   }

   return $i
}
#### ==========
proc insert_job {a num args} {
#### ==========
# 
# Insère les informations associées à un job (num) dans une liste ordonnée de jobs
# renvoie le nombre d'éléments contenus dans la liste
# a      :  tableau (array tcl) indexé sur num
# num    :  numéro du job à insérer
# args contient dans cet ordre :
#      nom    :  nom du job
#      date   :  date de lancement
#      time   :  heure de lancement
#      status :  etat du job
#      classe :  classe du job
#      diag   :  diagnostic sur l'exécution
#      user   :  compte de lancement
#      mach   :  machine d'exécution
#      interf :  interface de lancement
#      mode   :  batch/interactif
#      
# clock  :  résultat de la commande "clock seconds" est ajouté lors de l'insertion 
#           pour pouvoir trier la liste
#  
   upvar 1 $a listej
#
   set nbarg [llength $args]
   if {$nbarg != [expr $asjob::maxarg - 1]} {
    error "<ERREUR> nombre d'arguments non conforme ($nbarg/[expr $asjob::maxarg - 1]) lors de l'appel a insert_job"
   }
   if { [llength [array names listej $num]] == 0 } {
     array set listej [list $num [concat $num $args [clock seconds]]]
     set i [array size listej] 
   } else { 
     ashare::log "<ERREUR> l'élément $num existe déjà dans la liste "
     return 0
   }
   return $i  
}
