#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_outils.tcl 3255 2008-04-10 17:13:17Z courtois $

# définition du nom des outils standard
#################################################################
proc def_outils { } {
   set astk::ihm(outils) { Stanley  164  87 \
                           MeshTool 369 213 \
                           ConvBase 272 295 \
                           BSF      174 271 \
                           Eficas   161 209 \
                           Gibi     162 0 \
                           Gmsh     163 0 \
                           Grace    229 0 }
   set astk::ihm(outils_autocfg) { BSF Stanley MeshTool ConvBase }
   # attention pour la suite : tous les outils de outils_autocfg doivent être dans outils
   set astk::ihm(outils_cach) { astk }
   # les outils cachés n'apparaissent nulle part, sauf pour lancer run_$outil
}

# Insert les outils dans le menu, complète le popup
#################################################################
proc liste_outils { wmen } {
   $wmen add separator
# tableau inverse
   for {set j 0} {$j < $astk::outils(nb)} {incr j} {
      set astk::inv(outil,$astk::outils($j,nom)) $j
      if { $ashare::dbg >= 5 } {
         ashare::log "<DEBUG> (liste_outils) inv(outil,$astk::outils($j,nom)) = $j"
      }
   }
# outils standard
   set nb [expr [llength $astk::ihm(outils)] / 3]
   set nn 0
   for {set i 0} {$i < $nb} {incr i} {
      set out  [lindex $astk::ihm(outils) [expr $i * 3]]
      set ctxt [lindex $astk::ihm(outils) [expr $i * 3 + 1]]
      set ilab [lindex $astk::ihm(outils) [expr $i * 3 + 2]]
      if { $ilab != 0 } {
         set lab [ashare::mess ihm $ilab]
      } else {
         set lab $out
      }
      set test -1
      catch {set test $astk::inv(outil,$out)}
      # si l'outil n'est pas configuré : disabled
      set etat "disabled"
      if { $test >= 0 || [lsearch $astk::ihm(outils_autocfg) $out] > -1 } {
         set etat "normal"
         incr nn
      }
      $wmen add command -font $astk::ihm(font,labmenu) -label $lab \
         -command "run_tool $out" -state $etat
      set ashare::context($wmen,[expr $i + 2]) $ctxt
   }
# outils perso
   # combien ?
   set nbp [expr $astk::outils(nb) - $nn + [llength $astk::ihm(outils_autocfg)]]
   if { $nbp > 0 } {
      $wmen add separator
      for {set j 0} {$j < $astk::outils(nb)} {incr j} {
         if { [lsearch $astk::ihm(outils) $astk::outils($j,nom)] == -1} {
            $wmen add command -font $astk::ihm(font,labmenu) -label $astk::outils($j,nom) \
               -command "run_tool $astk::outils($j,nom)"
         }
      }
   }
   # complète le popup
   complete_popup $astk::ihm(popup)
}

# Lancement d'un outil
# numfich permet de lancer l'outil sur le "numfich"ième fichier sélectionné
# si numfich<0, on lance sur tous les fichiers
#################################################################
proc run_tool { outil {numfich -1} } {
   set iret 0
   set msg [ashare::mess ihm 165 $outil]
   change_status $msg
   update idletasks
   if { [lsearch $astk::ihm(outils_autocfg) $outil] > -1 || [lsearch $astk::ihm(outils_cach) $outil] > -1} {
      set iret [run_$outil $numfich]
   } else {
      # execution
      set num $astk::inv(outil,$outil)
      set cmd $astk::outils($num,cmde)

      # y-a-t'il des arguments facultatifs ?
      set arg_fac [regexp {\(@[fFR]+\)} $cmd]
      # y-a-t'il des arguments obligatoires ?
      regsub -all {\(@[fFR]+\)} $cmd {} cmd2
      set arg_obl [regexp {@[fFR]+} $cmd2]
      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (run_tool) arg_obl : $arg_obl  arg_fac : $arg_fac "
      }

      set lfich $astk::sel(filename)
      set maxfich [llength $lfich]
      if { $maxfich > 0 || $arg_obl == 0 } {
      # enlever les () autour des args facultatifs
         regsub -all {\(@f\)} $cmd {@f} cmd
         regsub -all {\(@F\)} $cmd {@F} cmd
         regsub -all {\(@R\)} $cmd {@R} cmd
         regsub -all {\(@D\)} $cmd {@D} cmd
      # un fichier sélectionné OU pas d'argument obligatoire
         set serv -1
         if { $maxfich == 0 } {
         # pas de sélection : lancement sans argument
            set cmd $cmd2
            set iret [ashare::rexec_cmd $serv astk::config $cmd "" 1 out]
         } elseif { $arg_fac || $arg_obl } {
         # sélection ET un argument
            set srv $astk::sel(servname)
            set nsrv $astk::inv(serv,$srv)
            if { $nsrv != -1 } {
               if { $astk::outils($num,dist) == "off" && $srv != $astk::config(-1,nom_complet) } {
                  set msg [ashare::mess ihm 191 $outil]
                  change_status $msg
                  tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
                  set iret 0
               } else {
               # exécution sur fichier distant
                  if { $numfich == -1 } {
                     set numfich 0
                  } else {
                     set maxfich [expr $numfich + 1]
                  }
                  for {set ind $numfich} {$ind < $maxfich} {incr ind} {
                     set fich [lindex $lfich $ind]
                     set ftmp $astk::sel(servname)
                     append ftmp "_"
                     regsub -all "/" $fich "_" ftmp2
                     append ftmp $ftmp2
                     set ftmp [file join $astk::tmpdir $ftmp]
                     # recopie le fichier distant en local
                     set macrcmd "$astk::cmd(shell_cmd) \""
                     #set cmdcpy "rcp $astk::config($nsrv,login)@$astk::config($nsrv,nom_complet):$fich $ftmp"
                     set cmdcpy [ashare::rcp_cmd astk::config $nsrv $fich -1 $ftmp "commande"]
                     append macrcmd $cmdcpy
                     append macrcmd ";"
                     # exécute la commande
                     append macrcmd $cmd
                     # recopie le fichier temporaire au cas où modif
                     append macrcmd ";"
                     #append macrcmd "rcp $ftmp $astk::config($nsrv,login)@$astk::config($nsrv,nom_complet):$fich"
                     append macrcmd [ashare::rcp_cmd astk::config -1 $ftmp $nsrv $fich "commande"]
                     # efface le temporaire
                     append macrcmd ";/bin/rm -rf $ftmp"
                     append macrcmd "\""
                     # "sélection" du fichier temporaire (pour subst_var)
                     set astk::sel(filename) $ftmp
                     # exécution
                     set iret [ashare::rexec_cmd $serv astk::config $macrcmd "" 1 out]
                     if { $ashare::dbg >= 4 } {
                        ashare::log "<DEBUG> Lancement d'un outil distant, commande :\n$macrcmd"
                     }
                  }
                  # retour sélection normale
                  set astk::sel(filename) $lfich
               }
            } else {
               set iret [ashare::rexec_cmd $serv astk::config $cmd "" 1 out]
            }
         } else {
         # pas d'argument
            set iret [ashare::rexec_cmd $serv astk::config $cmd "" 1 out]
         }
         if { $iret != 0 } {
            set msg [ashare::mess ihm 60 $iret]
            change_status $msg
            tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
         }
      } else {
         set msg [ashare::mess ihm 25]
         change_status $msg
         tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
      }
      update idletasks
      return $iret
   }
   if { $iret == 0 } {
   # ok
   } elseif { $iret == 4 } {
   # export failed
      change_status [ashare::mess erreur 40]
      tk_messageBox -message [ashare::mess erreur 40] -type ok -icon info
   } elseif { $iret == 5 } {
   # il faut sauver le profil
      set msg [ashare::mess erreur 13]
      change_status $msg
      tk_messageBox -message $msg -type ok -icon info
   } elseif { [lsearch {14 50 51} $iret] > -1 } {
   # données insuffisantes (message affiché dans run_$outil)
      set msg [ashare::mess erreur $iret]
      change_status $msg
      tk_messageBox -message $msg -type ok -icon info
   } else {
   # probleme lancement de l'outil
      change_status [ashare::mess ihm 60 "$outil, code retour=$iret"]
      tk_messageBox -message [ashare::mess ihm 60 "$outil, code retour=$iret"] -type ok -icon info
   }
   update idletasks
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (run_tool) outil : $outil, iret = $iret"
   }
}


# Vérifie que les données nécessaires sont présentes
# puis appel le service "tool_stanley"
# numfich pour compatibilité avec les run_$outil
#################################################################
proc run_Stanley { {numfich -1} } {
# il faut une base, ordre du choix :
#   0. la base sélectionnée
#   1. la première base en R
#   2. la première bhdf en R
#   3. la première base en D
#   4. la première bhdf en D
   set nbR -1
   set nbD -1
   set typ ""
   set var etude
   for {set i 0} {$i < $astk::profil($var,nbfic)} {incr i} {
      if { $astk::profil($var,fich,$i,type) == "base"
        || $astk::profil($var,fich,$i,type) == "bhdf" } {
         if { $nbR < 0 && $astk::profil($var,fich,$i,resultat) } {
            set nbR $i
            set typ $astk::profil($var,fich,$i,type)
         }
         if { $nbD < 0 && $astk::profil($var,fich,$i,donnee) } {
            set nbD $i
            set typ $astk::profil($var,fich,$i,type)
         }
      }
   }
   if { [expr $nbR + $nbD] == -2 } {
   # il faut une base !
      return 14
   } elseif { $nbR != -1 } {
      set indice $nbR
   } elseif { $nbD != -1 } {
      set indice $nbD
   }

   # une base est-elle dans la sélection ?
   set lfich $astk::sel(filename)
   if { [llength $lfich] > 0 } {
      # on ne s'intéresse qu'au premier indice de la sélection (on n'est pas dans la bsf !)
      set v $astk::sel(liste)
      set i $astk::sel(indice)
      if { $astk::profil($v,fich,$i,type) == "base"
        || $astk::profil($v,fich,$i,type) == "bhdf" } {
         set indice $i
         set var $v
         set typ $astk::profil($v,fich,$i,type)
      }
   }

   # paramètres
   set astk::profil(special) "stanley%NEXT%"
   
   # chemin du fichier
   append astk::profil(special) "R $typ "
   set valf ""
   set sfic $astk::inv(serv,$astk::profil($var,fich,$indice,serv))
   set serv $astk::inv(serv,$astk::profil(serveur))
   if { [is_localhost_serv $sfic] == 0 } {
      append valf "$astk::config($sfic,login)@$astk::config($sfic,nom_complet):"
   }
   if { [string index $astk::profil($var,fich,$indice,nom) 0] == "/" } {
      append valf $astk::profil($var,fich,$indice,nom)
   } else {
      append valf [file join $astk::profil(path_$var) $astk::profil($var,fich,$indice,nom)]
   }
   # enlever les /./
   regsub -all {/\./} $valf "/" valf

   append astk::profil(special) $valf
   ashare::mess info 19 $valf
   append astk::profil(special) " D"
   if { $astk::profil($var,fich,$indice,compress) } {
      append astk::profil(special) "C"
   }
   append astk::profil(special) " 0"
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (run_Stanley) ligne base : $astk::profil(special)"
   }
# on force le suivi interactif
   set astk::profil(suivi_interactif) 1
# enregistrement du profil
   set iret [enregistrer $astk::profil(serv_profil) $astk::profil(nom_profil)]
   if { $iret != 0 } {
      return 5
   }
# indices ihm
   set serv $astk::inv(serv,$astk::profil(serveur))
# nom du fichier export
   set serv_export $astk::profil(serv_profil)
   set nom_export [file rootname $astk::profil(nom_profil)]
   append nom_export "_post.export"
# export du profil sans les vérifications supplémentaires
   set iret [exporter astk_serv $serv_export $nom_export "noverif" "non"]
   if { $iret == 4 } {
      return $iret
   } elseif { $iret == 2 } {
   # alarmes emises
      change_status [ashare::mess info 16]
      tk_messageBox -message [ashare::mess info 16] -type ok -icon info
   }
   set astk::profil(special) ""

# préparation de la ligne de commande à exécuter
   set lcmd ""
   set argu ""
# tool_stanley profil
   #XXX assert serv_export == -1 !
   append lcmd [file join $ashare::prefix "bin" as_run]
   append lcmd " --proxy --serv --schema=[get_schema $serv serv]"
   append lcmd [ashare::get_glob_args]
   if { [is_localhost_serv $serv_export] == 0 } {
      append argu "$astk::config($serv_export,login)@$astk::config($serv_export,nom_complet):"
   }
   append argu $nom_export

#  execution
   set iret [ ashare::rexec_cmd -1 astk::config $lcmd $argu 0 out . progress]
   set jret $iret
   if { $iret == 0 } {
   # traitement du retour
      set nomjob [get_nomjob]
      append nomjob "_post"
      set jret [retour_as_exec $nomjob $out]
#      tk_messageBox -message $msg -type ok -icon info
      show_fen $astk::ihm(asjob)
   } else {
   # pb lancement
      ashare::mess "erreur" 3 $lcmd $jret $out
      return "Stanley"
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (run_Stanley) iret : $iret, output :\n$out"
      catch { ashare::log "<DEBUG> (run_Stanley) jobid=$jobid\nqueue=$queue" }
   }
   return $jret
}

# lancement d'astk (normalement appelé sur sélection de la bsf par run_tool)
#################################################################
proc run_astk { {numfich 0} } {
   ashare::log [ashare::mess ihm 165 astk]
   set cmd [file join $ashare::root bin astk]
   set args "--serv $astk::sel(servname) --profil [lindex $astk::sel(filename) $numfich]"
   set iret [ashare::rexec_cmd -1 astk::config $cmd $args 1 out]
   return $iret
}

# lancement de la bsf
#################################################################
proc run_BSF { {numfich 0} } {
   ashare::log [ashare::mess ihm 165 bsf]
   set cmd [file join $ashare::prefix bin bsf]

   set lfich $astk::sel(filename)
   set maxfich [llength $lfich]
   set s_inisrv ""
   set s_inidir ""
   if { $maxfich > 1 } {
      set lfich [lindex $lfich 0]
   }
   set dir  [file dirname $lfich]
   if { $maxfich != 0 } {
      set var $astk::sel(liste)
      set ind $astk::sel(indice)
      if { $astk::profil($var,fich,$ind,FR) == "F" } {
         set s_inidir "--inidir [file dirname $lfich]"
      } else {
         set s_inidir "--inidir $lfich"
      }
      
      set nsrv $astk::inv(serv,$astk::sel(servname))
      if { $nsrv != -1 } {
         set s_inisrv "--inisrv $astk::sel(servname)"
      }
   }
   set iret [ashare::rexec_cmd -1 astk::config $cmd "$s_inisrv $s_inidir" 1 out]
   return $iret
}

# reaction a Outils/Parametres
#################################################################
proc outils_conf { } {
   global outil_sel
   global old_out old_inv
   set fen .fen_para_outils
   catch {destroy $fen}
   toplevel $fen
   wm withdraw $fen
   wm title $fen "[ashare::mess ihm 153] - [ashare::mess ihm 154]"
   wm transient $fen .
   grab set $fen
   
# sauvegarde le tableau outils avant modif
   copie_tabl astk::outils old_out
   copie_tabl astk::inv old_inv
   if { $astk::outils(nb) > 0 } {
      set outil_sel $astk::outils(0,nom)
   }
   outils_conf_list $fen

   ashare::centre_fen $fen .
   wm deiconify $fen
}

# recréer la liste des outils
#################################################################
proc outils_conf_list { fen } {
   global outil_sel last_sel

   catch {destroy $fen.haut}
   catch {destroy $fen.liste}
   catch {destroy $fen.bas}

   # le 1er coup ET quand on veut recréer la liste, il faut raffraichir
   set last_sel -1

   pack [frame $fen.haut -relief flat -bd 0] -fill x -expand yes
   pack [frame $fen.liste -relief solid -bd 1] -fill x
   pack [frame $fen.bas -relief solid -bd 0] -fill both -expand yes

   pack [frame $fen.haut.choix_out -relief solid -bd 0] -side left -fill x -expand yes
   if { $astk::outils(nb) > 0 } {
      set MenuOut [tk_optionMenu $fen.haut.choix_out.outil outil_sel $astk::outils(0,nom)]
      $MenuOut entryconfigure 0 -font $astk::ihm(font,labmenu) -command "outils_conf_para $fen.liste $astk::outils(0,nom)"
      for {set j 1} {$j < $astk::outils(nb)} {incr j} {
         $MenuOut add radiobutton
         $MenuOut entryconfigure $j -label $astk::outils($j,nom) -font $astk::ihm(font,labmenu) -variable outil_sel -command "outils_conf_para $fen.liste $astk::outils($j,nom)"
      }
      $MenuOut add radiobutton
      $MenuOut entryconfigure $j -label [ashare::mess ihm 160] -font $astk::ihm(font,labmenu) -variable outil_sel -command "add_outil $fen $MenuOut"
   #
      $fen.haut.choix_out.outil configure -font $astk::ihm(font,labmenu) -bg $astk::ihm(couleur,liste)
      pack $fen.haut.choix_out.outil
      outils_conf_para $fen.liste $outil_sel
   } else {
      set MenuOut [tk_optionMenu $fen.haut.choix_out.outil outil_sel [ashare::mess ihm 160]]
      $MenuOut entryconfigure 0 -font $astk::ihm(font,labmenu) -command "add_outil $fen $MenuOut"
      pack $fen.haut.choix_out.outil
   }
   $MenuOut configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   button $fen.haut.help -image [image create photo -file $astk::icon(help)] \
         -bd 0 -command "grab release $fen ; aff_aide . 190"
   pack $fen.haut.help -anchor ne -side right

   pack [frame $fen.bas.valid -relief solid -bd 0] -side left -fill both -expand yes
      pack [frame $fen.bas.valid.m -relief solid -bd 0]
      button $fen.bas.valid.m.annuler -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
         -background $astk::ihm(couleur,annul) \
         -command "annule_mod_outil $fen"
      button $fen.bas.valid.m.ok  -font $astk::ihm(font,labbout) -text "Ok" \
         -background $astk::ihm(couleur,valid) \
         -command "save_outils ; create_menu_outils $astk::ihm(menu) DETR ; destroy $fen ; grab release $fen"
      pack $fen.bas.valid.m.ok $fen.bas.valid.m.annuler -side left -padx 10 -pady 5
   
   pack [frame $fen.bas.del -relief solid -bd 0] -side right
   button $fen.bas.del.bout -font $astk::ihm(font,labbout) -text [ashare::mess ihm 159] \
       -background $astk::ihm(couleur,suppr) \
      -command "del_outil $fen"
   pack $fen.bas.del.bout -side left -padx 10 -pady 5
}

#################################################################
proc outils_conf_para {fen outil} {
   global last_sel
   set nout $astk::inv(outil,$outil)
# si pas de chgt, on sort
   set mod 0
   catch { if { $outil == $last_sel } { set mod 1 } }
   if { $mod } { return }
   set last_sel $outil


   catch {destroy $fen.f}
   pack [frame $fen.f -relief raised -bd 0]

# cmde
   pack [frame $fen.f.cmde -relief solid -bd 0] -anchor w
   label $fen.f.cmde.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 156] -width 35 -anchor w
   entry $fen.f.cmde.path -width 30 -font $astk::ihm(font,val) -textvariable astk::outils($nout,cmde)
   pack $fen.f.cmde.lbl $fen.f.cmde.path -pady 3 -side left

# ext
   pack [frame $fen.f.ext -relief solid -bd 0] -anchor w
   label $fen.f.ext.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 157] -width 35 -anchor w
   entry $fen.f.ext.path -width 30 -font $astk::ihm(font,val) -textvariable astk::outils($nout,ext)
   pack $fen.f.ext.lbl $fen.f.ext.path -pady 3 -side left

# distant
   pack [frame $fen.f.dist -relief solid -bd 0] -anchor w
   label $fen.f.dist.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 158] -width 35 -anchor w
   radiobutton $fen.f.dist.active -font $astk::ihm(font,lab) -text on -value on -variable astk::outils($nout,dist)
   radiobutton $fen.f.dist.inactive -font $astk::ihm(font,lab) -text off -value off -variable astk::outils($nout,dist)
   pack $fen.f.dist.lbl $fen.f.dist.active $fen.f.dist.inactive -pady 3 -side left
}

# annule les modifs des outils
#################################################################
proc annule_mod_outil { fen } {
   global old_out old_inv
   set mod 0
   # vérifie que l'on n'a pas ajouté d'outil
   if { $old_out(nb) != $astk::outils(nb) } {
      set mod 1
   } else {
      set lmv [array get old_out]
      set nbl [expr [llength $lmv] / 2]
      for {set i 0} {$i < $nbl} {incr i} {
         set k  [expr $i * 2]
         set k1 [expr $k + 1]
         if { $astk::outils([lindex $lmv $k]) != [lindex $lmv $k1] } {
            set mod 1
            if { $ashare::dbg >= 5 } {
               ashare::log "<DEBUG> (annule_mod_outil) [lindex $lmv $k] old =[lindex $lmv $k1] ; new=$astk::outils([lindex $lmv $k])"
            } else {
               break
            }
         }
      }
   }
   # est-ce qu'il y a eu des modifs
   if { $mod } {
   # demande confirmation
      set iret [ tk_messageBox -message [ashare::mess ihm 88] \
         -title [ashare::mess ihm 143] -type yesno -icon question ]
      if { $ashare::dbg >= 5 } {
         ashare::log "<DEBUG> (annule_mod_outil) choix : $iret"
      }
      if { $iret == "yes" } {
         set mod 0
      }
   }
   # on sort
   if { $mod == 0 } {
      copie_tabl old_out astk::outils
      copie_tabl old_inv astk::inv
      destroy $fen
      grab release $fen
   } else {
      raise $fen
   }
}

# ajoute un outil
# fen : fenetre parent
#################################################################
proc add_outil { fen men } {
   global outil_sel last_sel
   global getValue_val
   # retire le focus au parent
   grab release $fen

   set new [expr $astk::outils(nb)]
   set valint $astk::ihm(outils_autocfg)
   for {set i 0} {$i<$new} {incr i} {
      lappend valint $astk::outils($i,nom)
   }
   getValue_fen $fen 160 168 $valint "" non_vide
   tkwait window .fen_getv
   grab set $fen
# valide le nom de l'outil
   set valid 1
   if { $getValue_val == "_VIDE" } {
      set valid 0
   } else {
      for {set i 0} {$i<$new} {incr i} {
         if { $getValue_val == $astk::outils($i,nom) } {
            set valid 0
         }
      }
   }
   if { $valid } {
      set astk::outils($new,nom) $getValue_val
      incr astk::outils(nb)
      # initialisation
      # valeurs obligatoires
      set nbm [llength $ashare::mots(SSF_out)]
      for {set i 0} {$i<$nbm} {incr i} {
         set mcle [lindex $ashare::mots(SSF_out) $i]
         set val ""
         # quelques valeurs particulières
         if { $mcle == "dist" } {
            set val "off"
         }
         set astk::outils($new,$mcle) $val
      }
      # en attendant l'appel à liste_outils
      set astk::inv(outil,$astk::outils($new,nom)) $new
      # nom dans le menu
      set outil_sel $astk::outils($new,nom)
      outils_conf_list $fen
      raise $fen
   } else {
      set outil_sel $last_sel
      outils_conf_para $fen $outil_sel
      raise $fen
   }
}

# supprime un outil
#################################################################
proc del_outil { fen } {
   global outil_sel
   set nout $astk::inv(outil,$outil_sel)
# demande confirmation
   set iret [ tk_messageBox -message [ashare::mess ihm 166] \
      -title [ashare::mess ihm 167] -type yesno -icon question ]
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (del_outil) choix : $iret"
   }
   if { $iret != "yes" } {
      raise $fen
      return 1
   }
   suppr_outil_tab $nout
   set outil_sel $astk::outils(0,nom)
   outils_conf_list $fen
   raise $fen
   return 0
}


# supprime l'outil d'indice nout du tableau
#################################################################
proc suppr_outil_tab { nout } {
# liste des mots-clés
   set mots_cles $ashare::mots(MCF_out)
   append mots_cles $ashare::mots(SSF_out)
   set nbm [llength $mots_cles]
# suppression
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (del_outil) Suppr $astk::outils($nout,nom) (numero $nout)" }
   catch {unset astk::inv(outil,$astk::outils($nout,nom))}
# décalage
   incr astk::outils(nb) -1
   for { set j $nout } { $j < $astk::outils(nb) } {incr j} {
      set j1 [expr $j + 1]
      if { $ashare::dbg >= 5 } {
         ashare::log "<DEBUG> (del_outil) Copie $astk::outils($j1,nom) de $j1 en $j"
      }
      # tabl inverse
      set astk::inv(outil,$astk::outils($j1,nom)) $j
      for { set k 0 } { $k < $nbm } { incr k } {
         set mcs [lindex $mots_cles $k]
         set astk::outils($j,$mcs) $astk::outils($j1,$mcs)
      }
   }
   # suppression du dernier outil
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (del_outil) Suppr outil $j"
   }
   for { set k 0 } { $k < $nbm } { incr k } {
      set mcs [lindex $mots_cles $k]
      unset astk::outils($j,$mcs)
   }
}

# retourne l'outil associé à un type de fichier, -1 sinon
#################################################################
proc quel_outil { typ } {
   set out -1
   for {set j 0} {$j < $astk::outils(nb)} {incr j} {
      if { [lsearch $astk::outils($j,ext) $typ] > -1} {
         set out $astk::outils($j,nom)
         break
      }
   }
   if { $out == -1 && $typ == "astk" } {
      set out astk
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (quel_outil) type : $typ --> $out"
   }
   return $out
}
