#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: file_funct.tcl 3469 2008-09-12 07:36:29Z courtois $

# fonctions de manipulations des fichiers

# ouvrir un fichier :
#  "edit" : édition
#  "def" : lance l'appli par défaut en fonction du type
# numfich permet de lancer l'outil sur le "numfich"ième fichier sélectionné
# si numfich<0, on lance sur tous les fichiers
#################################################################
proc ashare::file_open { act {numfich -1}} {
   set lfich $astk::sel(filename)
   set maxfich [llength $lfich]
   if { $maxfich == 0 } {
      set msg [ashare::mess ihm 25]
      tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
      change_status $msg
      return
   }
   if { $numfich == -1 } {
      set numfich 0
   } else {
      set maxfich [expr $numfich + 1]
   }
   for {set ind $numfich} {$ind < $maxfich} {incr ind} {
      set fich [lindex $lfich $ind]
      set serv $astk::inv(serv,$astk::sel(servname))
      switch $act {
         edit {
         # edition
            set cmd $astk::config($serv,editeur)
            if { $cmd != "" } {
               set cmd2 "$astk::cmd(shell_cmd) \"if test -d $fich ; then $cmd $fich/*; else $cmd $fich ; fi\""
               ashare::rexec_cmd $serv astk::config $cmd2 "" 1 out
            } else {
               set msg [ashare::mess erreur 15 [ashare::mess ihm 83] $astk::sel(servname)]
               tk_messageBox -message $msg -type ok -icon info
               change_status $msg
            }
         }
         typ {
         # action par defaut en fonction du type de fichier
            # cherche l'outil à lancer par défaut sur ce type
            set var $astk::sel(liste)
            set i   $astk::sel(indice)
            set outil [quel_outil $astk::profil($var,fich,$i,type)]
            if { $outil != -1 } {
               run_tool $outil $ind
            } else {
               ashare::file_open edit $ind
            }
         }
         ext {
         # action par defaut en fonction de l'extension fichier
            # cherche l'outil à lancer par défaut sur ce type
            set var $astk::sel(liste)
            set i   $astk::sel(indice)
            set ext [file extension $fich]
            regsub "^\." $ext "" ext
            set outil [quel_outil $ext]
            if { $outil != -1 } {
               run_tool $outil $ind
            } else {
               ashare::file_open edit $ind
            }
         }
         default {
         }
      }
   }
}

# action propriétés
#################################################################
proc ashare::file_prop { } {
   set lfich $astk::sel(filename)
   if { [llength $lfich] == 0 } {
      set msg [ashare::mess ihm 25]
      tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
      change_status $msg
      return
   }
   # multiplie les ls
   set cmd "$astk::cmd(shell_cmd) \""
   for {set ind 0} {$ind < [llength $lfich]} {incr ind} {
      set fich [lindex $lfich $ind]
      set dir  [file dirname $fich]
      set base [file tail $fich]
      append cmd "cd $dir ; /bin/ls -la $base ;"
   }
   append cmd "\""
   set serv $astk::inv(serv,$astk::sel(servname))
   set cmd [ashare::subst_var $cmd]
   set iret [ashare::rexec_cmd $serv astk::config $cmd "" 0 retour_ls]
   set msg [ashare::mess ihm 172]
   append msg " :"
   set fen .fen_prop
   ashare::fen_info $fen [ashare::mess ihm 138] $msg $retour_ls
}

# fenetre d'informations avec "scrollbar"
#################################################################
proc ashare::fen_info { fen titre entete contenu } {
   toplevel $fen
   wm title $fen $titre
# parent n'est pas "." pour la bsf
#   wm transient $fen .
   grab set $fen
   label $fen.lbl -font $astk::ihm(font,lab) -text $entete
   pack $fen.lbl -anchor w -side top -padx 5 -pady 3

   pack [frame $fen.f -relief flat -bd 0] -anchor w -side top -padx 5 -pady 3 -fill both -expand yes
   text $fen.f.tx -xscrollcommand "$fen.f.scrollx set" -yscrollcommand "$fen.f.scrolly set" \
      -width 90 -height 8 -font $astk::ihm(font,txtfix) -bg $astk::ihm(couleur,entry_background) -wrap none
   scrollbar $fen.f.scrolly -command "$fen.f.tx yview"
   scrollbar $fen.f.scrollx -command "$fen.f.tx xview" -orient h
   pack $fen.f.scrolly -side right  -fill y
   pack $fen.f.scrollx -side bottom -fill x
   pack $fen.f.tx -expand yes -fill both
   $fen.f.tx insert end $contenu
   $fen.f.tx configure -state disabled

   button $fen.ok -font $astk::ihm(font,labbout) -text "Ok" \
      -background $astk::ihm(couleur,valid) \
      -command "grab release $fen ; destroy $fen"
   pack $fen.ok -side top -pady 10
   tkwait window $fen
}

# imprimer un fichier avec choix de l'imprimante
#################################################################
proc ashare::file_print_to { {parent .} } {
   global select_printer bid_pr iret_print
   #
   set iret_print 0
   set fen .fen_print
   catch {destroy $fen}
   toplevel $fen
   wm title $fen "[ashare::mess ihm 247]"
   wm transient $fen $parent
   wm withdraw $fen
   
   ##############################
   pack [frame $fen.f0  -relief flat -bd 0] -fill x -anchor nw
   pack [frame $fen.f1  -relief flat -bd 0] -fill x
   pack [frame $fen.f99 -relief flat -bd 0]

   ##############################
   # liste
   if { $astk::print(nb) > 0 } {
      set bid_pr ""
      catch {set bid_pr $astk::print($select_printer,label)}
      if { $bid_pr == "" } {
         set select_printer 0
         set bid_pr $astk::print($select_printer,label)
      }
      set MenuPr [tk_optionMenu $fen.f1.lst bid_pr $astk::print(0,label)]
      $MenuPr entryconfigure 0 -font $astk::ihm(font,labmenu) -command "set select_printer 0"
      for {set j 1} {$j < $astk::print(nb)} {incr j} {
         $MenuPr add radiobutton
         $MenuPr entryconfigure $j -label $astk::print($j,label) -font $astk::ihm(font,labmenu) -variable bid_pr -command "set select_printer $j"
      }
   } else {
      label $fen.f1.lst -text "No printer"
   }
   pack $fen.f1.lst -fill x -padx 15 -pady 10 -expand yes

   ##############################
   if { $astk::print(nb) > 0 } {
      button $fen.f99.ok -font $astk::ihm(font,labbout) -text [ashare::mess ihm 251] \
         -command "ashare::file_pre-print $fen $parent"
      pack $fen.f99.ok -padx 10 -pady 3 -side left
   }
   button $fen.f99.can -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
      -background $astk::ihm(couleur,annul) \
      -command "set iret_print 1 ; destroy $fen"
   pack $fen.f99.can -padx 10 -pady 3
   ashare::centre_fen $fen $parent
   wm deiconify $fen
   tkwait window $fen
   return $iret_print
}

#################################################################
proc ashare::file_pre-print { fen parent } {
   global select_printer bid_pr
   set simp $astk::inv(serv,$astk::print($select_printer,serv))
   set file $astk::print($select_printer,file)
   regsub -all "@P" $astk::print($select_printer,cmde) $file cmde
   catch {destroy $fen}
   ashare::file_print $simp $cmde $parent
}

# imprimer un fichier
# simp : numéro du serveur
# cmde : ligne de commande
# on utilise /tmp sur le serveur d'impression
#################################################################
proc ashare::file_print { simp cmde {parent .}} {
   set lfich $astk::sel(filename)
   set maxfich [llength $lfich]
   if { $maxfich == 0 } {
      set msg [ashare::mess ihm 25]
      tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
      change_status $msg
      return
   }
   if { $cmde == "" } {
      set msg [ashare::mess erreur 18]
      tk_messageBox -message $msg -type ok -icon info
      change_status $msg
      return 1
   }
   for {set ind 0} {$ind < $maxfich} {incr ind} {
      set fich [lindex $lfich $ind]
   # fichier sur le serveur d'impression ou non
      set dist 0
      set serv $astk::inv(serv,$astk::sel(servname))
      if { [ashare::meme_machine $astk::config($serv,nom_complet) $astk::config($simp,nom_complet)] != 1
        || $astk::config($serv,login) != $astk::config($simp,login) } {
         set dist 1
      }
      if { $dist } {
         set ftmp $astk::sel(servname)
         append ftmp "_"
         regsub -all "/" $fich "_" ftmp2
         append ftmp $ftmp2
         set ftmp [file join "/tmp" $ftmp]
         # récupère le fichier vers le serveur d'impression
         set macrcmd "$astk::cmd(shell_cmd) \""
         #set cmdcpy "rcp $astk::config($nsrv,login)@$astk::config($nsrv,nom_complet):$fich $ftmp"
         set cmdcpy [ashare::rcp_cmd astk::config $serv $fich $simp $ftmp "commande"]
         append macrcmd $cmdcpy
         append macrcmd ";"
         # commande d'impression
         append macrcmd $cmde
         # efface le temporaire
         append macrcmd ";/bin/rm -f $ftmp"
         append macrcmd "\""
      } else {
         set macrcmd $cmde
         set ftmp $fich
      }
      # "sélection" du fichier temporaire (pour subst_var)
      set astk::sel(filename) $ftmp
   # impression
      ashare::rexec_cmd $simp astk::config $macrcmd "" 0 out $parent
   }
   # retour sélection normale
   set astk::sel(filename) $lfich
   return 0
}

# action Terminal
#################################################################
proc ashare::file_terminal { } {
   set lfich $astk::sel(filename)
   set maxfich [llength $lfich]
   if { $maxfich == 0 } {
      set msg [ashare::mess ihm 175]
      tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
      change_status $msg
      return
   }
   set dir  [file dirname $lfich]
   set serv $astk::inv(serv,$astk::sel(servname))
   set cmd $astk::config($serv,xterm)
   if { $cmd != "" } {
      set cmd2 "$astk::cmd(shell_cmd) \"if test -d $lfich; then { cd $lfich; $cmd; } else { cd $dir; $cmd; } fi\""
      ashare::rexec_cmd $serv astk::config $cmd2 "" 1 out
   } else {
      set msg [ashare::mess erreur 15 [ashare::mess ihm 82] $astk::sel(servname)]
      tk_messageBox -message $msg -type ok -icon info
      change_status $msg
   }
}

# action Ouvrir une URL
#################################################################
proc ashare::file_open_url { url } {
   set cmd $astk::config(-1,browser)
   if { $cmd != "" } {
      append cmd " $url"
      ashare::rexec_cmd -1 astk::config $cmd "" 1 out
   } else {
      set msg [ashare::mess erreur 15 [ashare::mess ihm 363] $astk::local_server]
      tk_messageBox -message $msg -type ok -icon info
      change_status $msg
   }
}
