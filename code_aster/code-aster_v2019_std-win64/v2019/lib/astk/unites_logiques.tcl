#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: unites_logiques.tcl 3642 2008-11-25 16:17:34Z courtois $

# definition de la table de conversion type/UL
# pour en ajouter : rajouter un bloc (incr i...)
# extrait de struc_profil.h de asterix

# nom, numéro d'unité logique, flag par défaut (DRC),
# rep=-1: non répétable y compris entre deux listes
# rep=0 : non répétable dans une liste (étude, surcharge, tests...)
# rep=1 : répétable
# rep=2 : répétable mais une seule fois en D, une seule fois en R
# type accepté : Fichier ou Répertoire (ou les deux)
#     c'est aussi le type par défaut en résultat (donc "les deux" n'est
#     pas compatible avec Résultat).
# cpr=1 : compression autorisée

#########################################################################
proc def_ul {} {
# pour profil etude
   set var "etude"
   set i -1
#  définition des UL
   incr i
   set astk::UL_ref($var,$i,nom) comm
   set astk::UL_ref($var,$i,num) 1
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) mail
   set astk::UL_ref($var,$i,num) 20
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) erre
   set astk::UL_ref($var,$i,num) 9
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) mess
   set astk::UL_ref($var,$i,num) 6
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) resu
   set astk::UL_ref($var,$i,num) 8
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) base
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "DRC"
   set astk::UL_ref($var,$i,rep) 2
   set astk::UL_ref($var,$i,FR)  "R"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) bhdf
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "DRC"
   set astk::UL_ref($var,$i,rep) 2
   set astk::UL_ref($var,$i,FR)  "R"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) cast
   set astk::UL_ref($var,$i,num) 37
   set astk::UL_ref($var,$i,def) "RC"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) mast
   set astk::UL_ref($var,$i,num) 26
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) mgib
   set astk::UL_ref($var,$i,num) 19
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) mmed
   set astk::UL_ref($var,$i,num) 20
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) msh
   set astk::UL_ref($var,$i,num) 19
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) msup
   set astk::UL_ref($var,$i,num) 19
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) datg
   set astk::UL_ref($var,$i,num) 16
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) pos
   set astk::UL_ref($var,$i,num) 37
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) ensi
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 2
   set astk::UL_ref($var,$i,FR)  "R"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) dat
   set astk::UL_ref($var,$i,num) 29
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) ps
   set astk::UL_ref($var,$i,num) 24
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) agraf
   set astk::UL_ref($var,$i,num) 25
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) digr
   set astk::UL_ref($var,$i,num) 26
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) rmed
   set astk::UL_ref($var,$i,num) 80
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) unv
   set astk::UL_ref($var,$i,num) 30
   set astk::UL_ref($var,$i,def) "RC"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) distr
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) hostfile
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) nom
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) para
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) repe
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "DR"
   set astk::UL_ref($var,$i,rep) 2
   set astk::UL_ref($var,$i,FR)  "R"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) libr
   set astk::UL_ref($var,$i,num) 38
   set astk::UL_ref($var,$i,def) ""
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 1
   incr i
   set astk::UL_ref($var,$i,nom) flash
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 2
   set astk::UL_ref($var,$i,FR)  "R"
   set astk::UL_ref($var,$i,cpr) 1
# pour toutes les listes
   incr i
   set astk::UL_ref($var,$i,nom) btc
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) -1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
#  nombre d'unités logiques déclarées ici
   incr i
   set astk::UL_ref($var,nbre) $i

####################################
# pour profil tests
   set var "tests"
   set i -1
#  définition des UL
   incr i
   set astk::UL_ref($var,$i,nom) list
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) rep_test
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "R"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) resu_test
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "R"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) hostfile
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) flash
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "R"
   set astk::UL_ref($var,$i,cpr) 1
# pour toutes les listes
   incr i
   set astk::UL_ref($var,$i,nom) btc
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) -1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
#  nombre d'unités logiques déclarées ici
   incr i
   set astk::UL_ref($var,nbre) $i

####################################
# pour les sources
   set var "sources"
   set i -1
#  définition des UL
   incr i
   set astk::UL_ref($var,$i,nom) f
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "FR"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) f90
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "FR"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) c
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "FR"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) py
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "FR"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) capy
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "FR"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) cata
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "FR"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) hist
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) conf
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) unig
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) datg
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "FR"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) cmat
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "D"
   set astk::UL_ref($var,$i,rep) 1
   set astk::UL_ref($var,$i,FR)  "FR"
   set astk::UL_ref($var,$i,cpr) 0
#  nombre d'unités logiques déclarées ici
   incr i
   set astk::UL_ref($var,nbre) $i

####################################
# pour surcharge
   set var "surcharge"
   set i -1
#  définition des UL
   incr i
   set astk::UL_ref($var,$i,nom) exec
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "DR"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) cmde
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "DR"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "R"
   set astk::UL_ref($var,$i,cpr) 0
   incr i
   set astk::UL_ref($var,$i,nom) ele
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "DR"
   set astk::UL_ref($var,$i,rep) 0
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
# pour toutes les listes
   incr i
   set astk::UL_ref($var,$i,nom) btc
   set astk::UL_ref($var,$i,num) 0
   set astk::UL_ref($var,$i,def) "R"
   set astk::UL_ref($var,$i,rep) -1
   set astk::UL_ref($var,$i,FR)  "F"
   set astk::UL_ref($var,$i,cpr) 0
#  nombre d'unités logiques déclarées ici
   incr i
   set astk::UL_ref($var,nbre) $i
}

# filtre la liste des unités logiques à utiliser
#################################################################
proc filtre_listul { { filtre "prefs" } } {
    if { $filtre == "prefs" } {
        set filtre $astk::config(-1,listul)
    }
    foreach var { "etude" "tests" "sources" "surcharge" } {
        set keep {}
        if { $filtre == "common" || $filtre == "comsort" } {
            set keep { comm mail erre mess resu base bhdf mgib mmed msh
                       datg pos dat rmed unv libr repe nom distr
                       list rep_test resu_test hostfile
                       f f90 c py capy cata hist conf unig
                       exec cmde ele }
        }
        if { $filtre == "perso" || $filtre == "persosort" } {
            set keep $astk::config(-1,user_listul)
        }
        if { [llength $keep] == 0 } {
            set j 0
            for {set i 0} {$i < $astk::UL_ref($var,nbre)} {incr i} {
                lappend keep $astk::UL_ref($var,$i,nom)
            }
        }
        if { $filtre == "sorted" || $filtre == "comsort" || $filtre == "persosort" } {
            set keep [lsort $keep]
        }
        # si une entrée du profil utilise un type qui n'est pas dans la
        # nouvelle liste, on ne modifie pas la liste
        set prof_init 0
        catch {
            set test $astk::profil($var,nbfic)
            set prof_init 1
        }
        if { $prof_init == 1 } {
            for {set j 0} {$j < $astk::profil($var,nbfic)} {incr j} {
                set typf $astk::profil($var,fich,$j,type)
                set found 0
                foreach typ $keep {
                    if { $typ == $typf } {
                        set found 1
                        break
                    }
                }
                if { $found == 0 } {
                    ashare::mess info 51 $typf
                    lappend keep $typf
                }
            }
        }
        # remplit la nouvelle liste
        set ic 0
        for {set j 0} {$j < [llength $keep]} {incr j} {
            set typ [lindex $keep $j]
            for {set i 0} {$i < $astk::UL_ref($var,nbre)} {incr i} {
                if { $typ == $astk::UL_ref($var,$i,nom) } {
                    set astk::UL($var,$ic,nom) $astk::UL_ref($var,$i,nom)
                    set astk::UL($var,$ic,num) $astk::UL_ref($var,$i,num)
                    set astk::UL($var,$ic,def) $astk::UL_ref($var,$i,def)
                    set astk::UL($var,$ic,rep) $astk::UL_ref($var,$i,rep)
                    set astk::UL($var,$ic,FR)  $astk::UL_ref($var,$i,FR)
                    set astk::UL($var,$ic,cpr) $astk::UL_ref($var,$i,cpr)
                    #ashare::log "#XXX type $typ : $ic / $i"
                    incr ic
                    break
                }
            }
        }
        set astk::UL($var,nbre) $ic
    }
}


# initialise les labels de choix des ul
#################################################################
proc init_choix_ul { } {
    # choix de liste des unités logiques
    set astk::ihm(listul,0) {default    414}
    set astk::ihm(listul,1) {sorted     415}
    set astk::ihm(listul,2) {common     416}
    set astk::ihm(listul,3) {comsort    417}
    set astk::ihm(listul,4) {perso      418}
    set astk::ihm(listul,5) {persosort  419}
    set astk::ihm(listul,nbre) 6
    for {set i 0} {$i < $astk::ihm(listul,nbre)} {incr i} {
        set name [lindex $astk::ihm(listul,$i) 0]
        set label [ashare::mess ihm [lindex $astk::ihm(listul,$i) 1]]
        set astk::ihm(listul,$label) $name
        set astk::ihm(listul,reverse,$name) $label
    }
}
