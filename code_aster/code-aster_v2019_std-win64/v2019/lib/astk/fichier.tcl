#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: fichier.tcl 3750 2009-01-15 15:50:06Z courtois $

# reaction a File/New
# aff!=OUI pour ne pas afficher d'onglet
#################################################################
proc nouveau {{ aff "OUI" }} {
   if { $astk::modprof } {
      set ich [ tk_messageBox -message [ashare::mess ihm 132] -type yesnocancel -icon question ]
      if { $ich != "cancel" } {
         if { $ich == "yes" } {
            set iret [enregistrer $astk::profil(serv_profil) $astk::profil(nom_profil)]
            if { $iret != 0 } {
               return
            }
         }
      } else {
         return
      }
   }
   ashare::pointeur off
   affiche_onglet none
   array unset astk::profil *
   init_profil
   reinit_satellite
   set astk::profil(onglet_actif) $astk::ihm(ong,1,1)
   if { $aff == "OUI" } {
      affiche_onglet $astk::profil(onglet_actif)
      create_menu_options $astk::ihm(menu) DETR
   }
   ashare::pointeur on
}

# reaction a File/Open
#################################################################
proc ouvrir {srv fich} {
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (ouvrir) Fichier profil : $srv $fich"
   }
   set iret 0
   if { $fich == "_VIDE" } {
      set iret [ashare::selecteur srv fich typ "" "" "F" "-initialfilt *.astk"]
   }
   if { $iret == 0 } {
      ashare::pointeur off
      set ftmp [get_tmpname .file_open]
      file delete -force $ftmp
      set iret [ashare::rcp_cmd astk::config $srv $fich -1 $ftmp action .]
      if { $iret == 0 && [file exists $ftmp] } {
         nouveau NON
         set idop [open $ftmp r]
         while  { [eof $idop] != 1 } {
            gets $idop line
            if { [regexp {([^ ]*)[ ]+(.*)} $line mat1 mot1 mot2] == 1 } {
               if { $mot2 != "_VIDE" } {
                  set astk::profil($mot1) $mot2
                  if { $ashare::dbg > 5 } {
                     ashare::log "<DEBUG> (ouvrir) profil($mot1) = $mot2"
                  }
               } else {
                  set astk::profil($mot1) ""
               }
            }
         }
         close $idop
         file delete -force $ftmp
         set astk::profil(serv_profil) $srv
         set astk::profil(nom_profil) $fich
# gérer la compatibilité ascendante
         # avec vers < 1.0.17
         if { $astk::profil(make_etude) == "oui" } {
            set astk::profil(make_etude) $astk::ihm(mode,run)
         } elseif { $astk::profil(make_etude) == "non" } {
            set astk::profil(make_etude) $astk::ihm(mode,pre)
         }
# fin glut compatibilité !
      # raffraichissement des derniers profils
         maj_prof $astk::profil(serv_profil) $astk::profil(nom_profil)
         modprof off
      # cohérence des serveurs du profil
         set iret [cohe_fich]
         if { $iret != 0 } {
            modprof on
            change_status [ashare::mess info 16]
            tk_messageBox -message [ashare::mess info 16] -type ok -icon info
         }
      } else {
         change_status [ashare::mess ihm 136]
         tk_messageBox -message [ashare::mess ihm 136] -type ok -icon info
      }
   # raffraichissement de l'ihm
      affiche_onglet $astk::profil(onglet_actif)
      verif_ongletM
      maj_sat
      create_menu_options $astk::ihm(menu) DETR
   # raffraichissement si agla
      raffr_ong_agla
   #
      ashare::pointeur on
   }
}

# reaction a File/Save
#################################################################
proc enregistrer {srv fich} {
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (enregistrer) Fichier profil : $srv $fich"
   }
   set iret 0
   if { $fich == "_VIDE" || $fich == [ashare::mess ihm 27] } {
      if { $fich != "_VIDE" } {
      # on n'est pas dans le cas Enregistrer sous...
         set ich [tk_messageBox -title [ashare::mess ihm 143] -message [ashare::mess ihm 132] -type yesno -icon question]
         if { $ich != "yes" } {
            return 1
         }
      }
      set iret [ashare::selecteur srv fich typ "" "" "F" "-initialfilt *.astk"]
   }
   if { $iret == 0} {
      change_status [ashare::mess ihm 117]
      ashare::pointeur off
      # impose l'extension .astk (vérifie et ajoute si besoin)
      if { [regexp {\.astk$} $fich] != 1 } {
         append fich ".astk"
      }
      set astk::profil(serv_profil) $srv
      set astk::profil(nom_profil) $fich
      # ecrire le fichier
      set ftmp [get_tmpname .file_save]
      set idsav [open $ftmp w]
      set ind [array get astk::profil]
      set n [expr [llength $ind] / 2]
      for {set i 0} {$i < $n} {incr i} {
         set k  [expr $i * 2]
         set k1 [expr $k + 1]
         set mot [lindex $ind $k]
         set val [lindex $ind $k1]
         if { [string length $val] == 0 } {
            set val "_VIDE"
         }
         puts $idsav "$mot $val"
         if { $ashare::dbg >= 5 } {
            ashare::log "<DEBUG> (enregistrer) profil($mot) = $val"
         }
      }
      close $idsav
      set iret [ashare::rcp_cmd astk::config -1 $ftmp $srv $fich action .]
      file delete -force $ftmp
# raffraichissement des derniers profils
      maj_prof $astk::profil(serv_profil) $astk::profil(nom_profil)
      ashare::pointeur on
      modprof off
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (enregistrer) iret=$iret ; srv # fich : $srv # $fich"
   }
   return $iret
}

# reaction a File/Copy as : copie d'un profil et de ses fichiers
#################################################################
proc copie_prof { } {
   # enregistrement du profil actuel si modifié ou si Nouveau ?
   if { $astk::modprof || $astk::profil(nom_profil) == [ashare::mess ihm 27] } {
      set ich [ tk_messageBox -message [ashare::mess ihm 132] -type yesnocancel -icon question ]
      if { $ich != "cancel" } {
         if { $ich == "yes" } {
            # pour ne pas poser deux fois la question
            if { $astk::profil(nom_profil) == [ashare::mess ihm 27] } {
               set astk::profil(nom_profil) "_VIDE"
            }
            set iret [enregistrer $astk::profil(serv_profil) $astk::profil(nom_profil)]
            if { $iret != 0 } {
               return 2
            }
         }
      } else {
         return 1
      }
   }
   # choix du répertoire destination
   set iret [ashare::selecteur srv dir typ "" "" "R"]
   if { $iret == 0} {
      ashare::pointeur off
      set new [file join $dir [file tail $astk::profil(nom_profil)]]
      # pour chaque fichier, on demande si on doit copier
      # si c'est le cas, on modifie le profil
      set yesall 0
      foreach var {etude tests sources surcharge} {
         set nbcop 0
         add_chbase $var
         for {set i 0} {$i<$astk::profil($var,nbfic)} {incr i} {
         # il faut être en D ou R pour être copié
            if { $astk::profil($var,fich,$i,donnee) || $astk::profil($var,fich,$i,resultat) } {
               set valf $astk::profil($var,fich,$i,nom)
               if { $yesall == 0 } {
                  set rep [ashare::message_yesall [ashare::mess ihm 143] [ashare::mess ihm 277 $valf] .]
                  if { $rep == "yes" } {
                     set yes 1
                  } elseif { $rep == "all" } {
                     set yesall 1
                  } else {
                     set yes 0
                  }
               }
               if { $yesall || $yes } {
                  incr nbcop
                  # copier le fichier/rép
                  set sfic $astk::inv(serv,$astk::profil($var,fich,$i,serv))
                  set iret [ashare::rcp_cmd astk::config $sfic $valf $srv $dir action .]
                  # modifier le profil
                  set astk::profil($var,fich,$i,nom)  [file join $dir [file tail $valf]]
                  set astk::profil($var,fich,$i,serv) $astk::config($srv,nom)
               }
            }
         }
         # si on a copié au moins un fichier, on modifie le chemin de base
         if { $nbcop > 0 } {
            set astk::profil(path_$var) $dir
         }
         sub_chbase $var
      }
      # "enregistrer sous" du profil modifié
      set new [file join $dir [file tail $astk::profil(nom_profil)]]
      set iret [enregistrer $srv $new]
      if { $iret != 0 } {
         set msg [ashare::mess erreur 31]
         tk_messageBox -message $msg -type ok -icon info
         return 5
      }
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (copie_prof) iret=$iret ; srv # dir : $srv # $dir"
   }
   return $iret
}

# importer : reaction a File/Import
#   relit un fichier .export
#################################################################
proc importer { type { append "non"}} {
   global getValue_val
   set typin $type
   switch -exact -- $type {
      astk_serv -
      rex -
      test {
         set fil "*.export"
      }
      default {
         set fil "*"
         tk_messageBox -message [ashare::mess ihm 1] -type ok
         return
      }
   }
   set nouveau "oui"
   # sélection du fichier ou saisie du numéro de la fiche à importer ou du cas-test
   if { $type == "rex" } {
      set type astk_serv
      if { $astk::agla(num_serv) < 0 } {
         tk_messageBox -message [ashare::mess ihm 1] -type ok
         return
      }
      getValue_fen . 223 214 {} "" vide
      tkwait window .fen_getv
      set num ""
      catch { set num [format "%06d" $getValue_val] }
      if { $num == "" } {
         return
      }
      set iret 0
      set srv $astk::agla(num_serv)
      set fich [file join $astk::agla(rep_rex) "emise" $num "$num.export"]
      ashare::mess info 43 $fich
   } elseif { $type == "test" } {
      set type astk_serv
      set nouveau "non"
      getValue_fen . 380 381 {} "" vide
      tkwait window .fen_getv
      set tst $getValue_val
      if { $tst == "" } {
         return
      }
      set fich "$tst.para"
      # indices ihm
      set srv $astk::inv(serv,$astk::profil(serveur))
      # préparation de la ligne de commande à exécuter
      set lcmd ""
      set argu ""
      set ftmp [get_tmpname .get_export]
      set idexp [open $ftmp w]
      write_server_infos $idexp $srv
      close $idexp
      # as_run --get_export testcase
      append lcmd [file join $ashare::prefix "bin" as_run]
      append lcmd " --proxy --get_export --schema=[get_schema $srv get_export] $ftmp"
      append lcmd " --vers=$astk::profil(version)"
      append lcmd " $tst"
      append lcmd [ashare::get_glob_args]
      # execution
      change_status "[ashare::mess ihm 49]..."
      set iret [ ashare::rexec_cmd -1 astk::config $lcmd $argu 0 out . progress]
      if { $iret == 0 } {
         set srv -1
         set fich [get_tmpname .file_tst_import]
         set idsav [open $fich w]
         puts $idsav $out
         close $idsav
      } else {
         # pb lancement de as_run
         ashare::mess "erreur" 3 $lcmd $iret $out
         set iret 4
      }
      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (get_export) iret=$iret, output :\n$out"
      }
      change_status [ashare::mess ihm 12]

   } else {
      set iret [ashare::selecteur srv fich typ "" "" "F" "-initialfilt \"$fil\""]
   }
   if { $iret == 0 } {
   # import
      set iret [import_from $type $srv $fich $append $nouveau]
      if { $typin == "test" } {
         for {set i 0} { $i < $astk::profil(etude,nbfic) } {incr i} {
             set astk::profil(etude,fich,$i,serv) $astk::profil(serveur)
         }
      }
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (importer) retour : $iret"
   }
   if       { $iret == 2 } {
      tk_messageBox -message [ashare::mess info 16] -type ok -icon info
   } elseif { $iret == 4 } {
      tk_messageBox -message [ashare::mess ihm 360] -type ok -icon info
   } elseif { $iret != 0 } {
      tk_messageBox -message [ashare::mess ihm 136] -type ok -icon info
   }
   return $iret
}

# import_from : import d'un fichier
#################################################################
proc import_from { type srv fich { append "non" } {nouveau "oui"}} {
   ashare::pointeur off
   if { $append == "non" } {
      nouveau NON
   }
#
   set ftmp [get_tmpname .file_import]
   set iret [ashare::rcp_cmd astk::config $srv $fich -1 $ftmp action .]
   if { $iret == 0 && [file exists $ftmp] } {
      if { $append == "non" && $nouveau == "oui" } {
         set astk::profil(nom_profil) $fich
         set astk::profil(serv_profil) $srv
      }
      set iret [import_$type $ftmp $append]
      file delete -force $ftmp
   } else {
      set iret 4
   }
# raffraichissement de l'ihm
   affiche_onglet $astk::profil(onglet_actif)
   verif_ongletM
   maj_sat
   create_menu_options $astk::ihm(menu) DETR
# raffraichissement si agla
   raffr_ong_agla
   modprof on
#
   ashare::pointeur on
   return $iret
}

# exporter_sous : reaction a File/Export as
#################################################################
proc exporter_sous { type } {
   set iret [exporter $type -1 _VIDE]
   if { $iret == 2 } {
   # alarmes emises
      change_status [ashare::mess info 16]
      tk_messageBox -message [ashare::mess info 16] -type ok -icon info
   } elseif { $iret != 0 } {
      tk_messageBox -message [ashare::mess ihm 136] -type ok -icon info
   }
   change_status [ashare::mess ihm 12]
   update idletasks
}

# exporter : export d'un profil
# verif : on vérifie si les fichiers en données existent ou pas
# tout_dist : pour forcer l'export avec user@mach:/...
#################################################################
proc exporter { type srv fich {verif "oui"} {tout_dist "non"}} {
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (exporter) Fichier export : $srv $fich"
   }
   # extension par défaut
   switch -exact -- $type {
      run_aster { set ext run }
      astk_serv { set ext export }
      default   { set ext * }
   }
   # récupérer les types (F)ichier ou (R)épertoire
   set iret 0
   if { $fich == "_VIDE" } {
      set iret [ashare::selecteur srv fich typ "" "" "F" "-initialfilt *.$ext"]
   }
   if { $iret == 0 } {
      ashare::pointeur off
      # impose l'extension (vérifie et ajoute si besoin)
      if { [regexp "\.$ext\$" $fich] != 1 } {
         append fich ".$ext"
      }
      set jret 0
      if { $verif != "noverif" } {
         set jret [verif_nomfich]
         if { $jret != 0 && $jret > $iret } {
            set iret $jret
         }
         if { $iret < 4 } {
            set jret [check_exist_type]
            if { $jret != 0 && $jret > $iret } {
               set iret $jret
            }
         }
      }
      if { $iret < 4 } {
         change_status "[ashare::mess ihm 123]..."
         set ftmp [get_tmpname .file_export]
         set iret [export_$type $ftmp $tout_dist $verif]
         if { $iret != 4 } {
            set kret [ashare::rcp_cmd astk::config -1 $ftmp $srv $fich action .]
            file delete -force $ftmp
            if { $kret != 0 && $kret > $iret } {
               set iret $kret
            }
         }
      } else {
         set msg [ashare::mess erreur 40]
         change_status $msg
         tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
      }
      ashare::pointeur on
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (exporter) retour : $iret"
   }
   return $iret
}

# Fichier/Enregistre dans la base d'études
# L'accès à la machine AGLA est verrouillé dans le menu
#################################################################
proc insert_in_db {} {
   # on prend un .export bidon
   set astk::profil(serv_fich_export) -1
   set fexp [get_tmpname insert_in_db]
   # l'extension est obligatoire au moment de l'export
   append fexp ".export"
   set astk::profil(nom_fich_export) $fexp
   set iret [exporter astk_serv -1 $astk::profil(nom_fich_export)]
   if { $iret == 0 } {
   # indices ihm
      set serv $astk::inv(serv,$astk::profil(serveur))
   # préparation de la ligne de commande à exécuter
      set lcmd ""
      set argu ""
      append lcmd [file join $ashare::prefix "bin" as_run]
      append lcmd " --proxy --insert_in_db --schema=[get_schema $serv insert_in_db]"
      append lcmd [ashare::get_glob_args]
      append argu $astk::profil(nom_fich_export)

      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (insert_in_db) $lcmd + $argu"
      }

   # execution
      change_status "[ashare::mess ihm 49]..."
      set jret [ ashare::rexec_cmd -1 astk::config $lcmd $argu 0 out . progress]
      if { $jret != 0 } {
      # pb lancement de as_run
         ashare::mess "erreur" 3 $lcmd $jret $out
         set iret 4
      }
      set iret $jret
      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (insert_in_db) iret=$iret, jret=$jret, output :\n$out"
      }
   }
   if { $iret == 0 } {
      # pass
   } elseif { $iret == 2 } {
   # alarmes emises
      change_status [ashare::mess info 16]
      tk_messageBox -message [ashare::mess info 16] -type ok -icon info
   } elseif { $iret == 4 } {
      tk_messageBox -message [ashare::mess ihm 375] -type ok -icon info
   } else {
      tk_messageBox -message [ashare::mess ihm 136] -type ok -icon info
   }
   change_status [ashare::mess ihm 12]
   update idletasks
}

#################################################################
proc quitter {} {
   if { $astk::modprof } {
      set ich [ tk_messageBox -message [ashare::mess ihm 132] -type yesnocancel -icon question ]
      if { $ich != "cancel" } {
         if { $ich == "yes" } {
            # pour ne pas poser deux fois la question
            if { $astk::profil(nom_profil) == [ashare::mess ihm 27] } {
               set astk::profil(nom_profil) "_VIDE"
            }
            set iret [enregistrer $astk::profil(serv_profil) $astk::profil(nom_profil)]
            if { $iret != 0 } {
               return
            }
         }
      } else {
         return
      }
   } else {
      set ich [ tk_messageBox -message [ashare::mess ihm 2] -type yesno -icon question ]
      if { $ich == "no" } {
         return
      }
   }
# exit
   ashare::my_exit 0
}

# verifier que les items en données existent et récupérer leur type
# retourne : O si ok
#################################################################
proc check_exist_type { } {
   set iret 0
   # tabf : tableau temporaire qui contient les noms de fichiers par serveur
   # tabf(num_serv,nbfic)
   # tabf(num_serv,indice=1...nbfic) = etude indice chemin_du_fichier num_type_UL
   for {set srv -1} {$srv < $astk::config(nb_serv)} {incr srv} {
      set tabf($srv,nbfic) 0
   }

   change_status [ashare::mess ihm 244]
   # regroupe les fichiers par serveur
   foreach var { etude tests sources surcharge } {
      set var2 $var
      if { $var == "sources" } {
         # on ne "coche" pas sources
         set var2 surcharge
      }
      if { $astk::profil($var2) == "oui" } {
         for {set i 0} { $i < $astk::profil($var,nbfic) } {incr i} {
            # numéro du type dans astk::UL_ref
            set typdef "X"
            set nul 0
            for {set nul 0} {$nul < $astk::UL_ref($var,nbre)} {incr nul} {
               if { $astk::profil($var,fich,$i,type) == $astk::UL_ref($var,$nul,nom) } {
                  set typdef $astk::UL_ref($var,$nul,FR)
                  break
               }
            }
            # si fichier source
            # ou ( surcharge (exec/cmde/ele/btc) en D mais pas R
            #      et pas agla
            #      et pas multiple car relocalisé )
            if { ( $var != "surcharge" && $astk::profil($var,fich,$i,donnee) )
              || ( $var == "surcharge" && $astk::profil($var,fich,$i,donnee)
                   && $astk::profil($var,fich,$i,resultat) == 0
                   && $astk::profil(agla) != "oui"
                   && [in_no_values $astk::profil(opt_val,multiple)] ) } {
               # serveur
               set srv $astk::inv(serv,$astk::profil($var,fich,$i,serv))
               # chemin du fichier
               set valf $astk::profil($var,fich,$i,nom)
               if { [string index $valf 0] != "/" } {
                  set valf [file join $astk::profil(path_$var) $valf]
               }
               # stocke le chemin
               incr tabf($srv,nbfic)
               set tabf($srv,$tabf($srv,nbfic)) [list $var $i $valf $nul]
            } else {
               # on met le type par défaut si on est en résultat
               set astk::profil($var,fich,$i,FR) $typdef
            }
         }
      }
   }
   # interrogation de chaque serveur
   set typdef "X"
   for {set srv -1} {$srv < $astk::config(nb_serv)} {incr srv} {
      if { $tabf($srv,nbfic) > 0 } {
         # cf. setup.py (aster-full-src) pour calculer la longueur limite d'une commande shell
         set ilast 0
         set Lmaxi 1024
         while { $ilast < $tabf($srv,nbfic) } {
            set ideb [expr $ilast + 1]
            set ilast $tabf($srv,nbfic)
            #
            set cmd "$astk::cmd(shell_cmd) \""
            for {set nf $ideb} {$nf <= $ilast} {incr nf} {
               set vtab $tabf($srv,$nf)
               set var  [lindex $vtab 0]
               set i    [lindex $vtab 1]
               set valf [lindex $vtab 2]
            # longueur de la commande commande ci dessous = 85 + 2 $valf
               set iadd [expr 85 + 2*[string length $valf]]
               if { [expr [string length $cmd] + $iadd] > $Lmaxi } {
                  set ilast [expr $nf - 1]
               } else {
                  append cmd "if test -f $valf ; then echo F ; else if test -d $valf ; then echo R ; else echo $typdef ; fi ; fi ; "
               }
            }
            append cmd "\""
            ashare::rexec_cmd $srv astk::config $cmd "" 0 out .
            set ltyp [split $out]
            set ntyp [llength $ltyp]
            set inb [expr $ilast - $ideb + 1]
            if { $ntyp != $inb } {
               set iret 4
               ashare::mess erreur 3 check_exist_type $iret $out
            } else {
               for {set nf $ideb} {$nf <= $ilast} {incr nf} {
                  set vtab $tabf($srv,$nf)
                  set typ  [lindex $ltyp [expr $nf - $ideb]]
                  set var  [lindex $vtab 0]
                  set i    [lindex $vtab 1]
                  set valf [lindex $vtab 2]
                  set nul  [lindex $vtab 3]
                  if { $ashare::dbg > 0 } {
                     ashare::log "<INFO> Type $typ : $valf"
                  }
                  set astk::profil($var,fich,$i,FR) $typ
                  if { $typ == "X" } {
                  # fichier inexistant
                     set msg [ashare::mess info 4 $valf]
                     change_status $msg
                     tk_messageBox -message $msg -type ok -icon info
                     set iret 4
                  } elseif { [regexp $typ $astk::UL_ref($var,$nul,FR)] == 0 } {
                  # pas du type requis
                     if { $astk::UL_ref($var,$nul,FR) == "F" } {
                        set msg [ashare::mess erreur 35 $astk::UL_ref($var,$nul,nom)]
                     } else {
                        set msg [ashare::mess erreur 36 $astk::UL_ref($var,$nul,nom)]
                     }
                     change_status $msg
                     tk_messageBox -message $msg -type ok -icon info
                     set iret 4
                  }
               }
            }
         }
      }
   }
   change_status [ashare::mess ihm 12]
   return $iret
}

# import un profil au format astk_serv (.export)
#################################################################
proc import_astk_serv { fich append } {
   set iret 0
   regsub {\.export$} $astk::profil(nom_profil) ".astk" astk::profil(nom_profil)
   # compteurs de fichier etude, tests, source, surcharge
   set idp [open $fich r]
   set perc 100
   # voir si la valeur change
   set ini_serv $astk::profil(serveur)
   set ini_node $astk::profil(noeud)
   while { [eof $idp] != 1 } {
      gets $idp line
      if { [regexp {^[ ]*A[ ]+([-a-z_A-Z0-9]*)[ ]+(.*[^ $])} $line bid mot val] == 1 } {
      # arguments
         switch -exact -- $mot {
            args  { set astk::profil($mot) $val }
            tpmax { set astk::profil(temps) $val }
            memjeveux_stat {
               set astk::profil(args) [string trim "$astk::profil(args) -memjeveux_stat $val"]
            }
            memjeveux { }
            default {
            # comportement par defaut
               set trouv 0
               # valeurs des arguments
               set narg [expr [llength $astk::ihm(l_arg)] / 3]
               for {set i 0} {$i < $narg} {incr i} {
                  set arg  [lindex $astk::ihm(l_arg) [expr $i * 3]]
                  if { $mot == $arg } {
                     set astk::profil(option,$arg) 1
                     set astk::profil(opt_val,$arg) $val
                     set trouv 1
                     break
                  }
               }
               if { $trouv == 0 } {
                  ashare::log "<INFO> Argument $mot inconnu (valeur=$val)"
                  set iret 2
               }
            }
         }
      } elseif { [regexp {^[ ]*P[ ]+([-a-z_A-Z0-9]*)[ ]+(.*[^ $])} $line bid mot val] == 1 } {
      # paramètres
         switch -exact -- $mot {
            actions {
               switch -exact -- $val {
                  make_etude -
                  make_dbg -
                  make_env {
                     set astk::profil(etude) "oui"
                     switch -exact -- $val {
                        make_etude { set astk::profil(make_etude) $astk::ihm(mode,run) }
                        make_dbg   { set astk::profil(make_etude) $astk::ihm(mode,dbg) }
                        make_env   { set astk::profil(make_etude) $astk::ihm(mode,pre) }
                     }
                  }
                  make_exec -
                  make_cmde -
                  make_ele {
                     set astk::profil(surcharge) "oui"
                  }
                  astout {
                     set astk::profil(tests) "oui"
                  }
                  default {
                     set n $astk::ihm(nongl_agla)
                     set trouv 0
                     for {set ia 1} {$ia<=$astk::ihm(nbong,$n)} {incr ia} {
                        if { $mot == $astk::ihm(ong,$n,$ia) } {
                           set astk::profil(agla) "oui"
                           set astk::profil($mot) "oui"
                           set trouv 1
                           break
                        }
                     }
                     if { $trouv == 0 } {
                        ashare::log "<INFO> Action $mot inconnue"
                        set iret 2
                     }
                  }
               }
            }
            serveur {
               set is [valid_serveur $val "" nom_complet]
               if { $is == -999 } {
                  ashare::log "<INFO> Serveur $val inconnu, on positionne sur le premier"
                  set iret 2
                  set is 0
               }
               set astk::profil($mot) $astk::config($is,nom)
            }
            noeud {
               if { $val == "AGLA" } {
                  set astk::profil($mot) $val
                  set astk::profil(serveur) $astk::config($astk::agla(num_serv),nom)
               } else {
                  set in [quel_serveur $val]
                  if { $in == "-" } {
                     ashare::log "<INFO> Noeud $val inconnu"
                     set iret 2
                     set val "-"
                  }
                  set astk::profil($mot) $val
               }
            }
            debug {
               if { $val == "debug" } {
                  set astk::profil($mot) 1
               } else {
                  set astk::profil($mot) 0
               }
            }
            mode {
               if { $val == "batch" } {
                  set astk::profil(batch) 1
               } else {
                  set astk::profil(batch) 0
               }
            }
            memjob {
               set astk::profil(memoire) 64
               catch {set astk::profil(memoire) [expr $val / 1024]}
            }
            tpsjob {
               set astk::profil(temps) $val
               catch {set astk::profil(temps) [expr $val * 60]}
            }
            version {
               set astk::profil($mot) $val
            }
            lang -
            display -
            profastk -
            origine -
            username -
            nomjob -
            soumbtc -
            consbtc -
            special -
            xterm -
            mclient -
            follow_output -
            liste_test -
            testlist -
            memory_limit -
            mem_aster -
            time_limit -
            platform -
            studyid -
            aster_root -
            proxy_dir -
            protocol_exec -
            protocol_copyfrom -
            protocol_copyto -
            uclient {
            # valeurs ignorees
            }
            default {
            # comportement par defaut
               set trouv 0
               # valeurs des paramètres
               set npar [expr [llength $astk::ihm(l_par)] / 3]
               for {set i 0} {$i < $npar} {incr i} {
                  set opt  [lindex $astk::ihm(l_par) [expr $i * 3]]
                  if { $mot == $opt } {
                     set astk::profil(opt_val,$opt) $val
                     set trouv 1
                     break
                  }
               }
               # valeurs des options
               set nopt [expr [llength $astk::ihm(l_opt)] / 3]
               for {set i 0} {$i < $nopt} {incr i} {
                  set opt  [lindex $astk::ihm(l_opt) [expr $i * 3]]
                  if { $mot == $opt } {
                     set astk::profil(opt_val,$opt) $val
                     set trouv 1
                     break
                  }
               }
               if { $trouv == 0 } {
                  ashare::log "<INFO> Paramètre $mot inconnu"
                  set iret 2
               }
            }
         }
         if { $astk::profil(serveur) != $ini_serv \
           && $astk::profil(noeud) == $ini_node } {
            set is $astk::inv(serv,$astk::profil(serveur))
            if { $is > 0 } {
               set astk::profil(noeud) $astk::config($is,noeud,0)
            }
         }

      } elseif {[regexp {^[ ]*([FR]+)[ ]+([-a-z_A-Z0-9]*)[ ]+(.*)[ ]+([DRC]+)[ ]*([0-9]*)} $line bid cFR ctyp val flag ul] == 1} {
         # nom de fichiers ou répertoires sans blanc
         regsub -all " " $val "" val
         # dans quelle liste
         set ist [is_type $ctyp]
         set var [lindex $ist 0]
         set ind [lindex $ist 1]
         if { $var == 0 } {
            ashare::log "<INFO> (import_astk_serv) On ne sait pas dans quelle liste mettre ce fichier :\n$ctyp ! $val ! $flag ! $ul"
            set var etude
            set ctyp libr
            set iret 2
         }
         if { $ind != -1 } {
            if { $ctyp != "libr" && $astk::UL_ref($var,$ind,num) != $ul } {
               ashare::log "<INFO> (import_astk_serv) Type et UL ne correspondent pas pour ce fichier :\n$ctyp ! $val ! $flag ! $ul"
               set ctyp libr
               set iret 2
            }
         }
         set i $astk::profil($var,nbfic)
         set srv $astk::local_server
         set cfic $val
         if { [regexp {(.+)@(.+):(.+)} $val bid user srv cfic] } {
            set is [valid_serveur $srv "" nom_complet]
            if { $is == -999 } {
               ashare::log "<INFO> (import_astk_serv) On met ce fichier sur $astk::local_server :\n$ctyp ! $val ! $flag ! $ul"
               set iret 2
               set is -1
            }
            set srv $astk::config($is,nom)
         }
         set astk::profil($var,fich,$i,nom) $cfic
         set astk::profil($var,fich,$i,serv) $srv
         set astk::profil($var,fich,$i,type) $ctyp
         if { $var != "etude" } {
            set ul 0
         }
         set astk::profil($var,fich,$i,UL) $ul
         set astk::profil($var,fich,$i,donnee) [regexp D+ $flag]
         set astk::profil($var,fich,$i,resultat) [regexp R+ $flag]
         set astk::profil($var,fich,$i,compress) [regexp C+ $flag]
         set astk::profil($var,fich,$i,FR) $cFR
         incr astk::profil($var,nbfic)
      }
   }
   close $idp
   # chemin de base
   if { [file dirname $astk::profil(nom_profil)] != "." } {
       foreach var { etude tests sources surcharge } {
           if { $astk::profil(path_$var) == "" } {
              set astk::profil(path_$var) [file dirname $astk::profil(nom_profil)]
           }
       }
   }
   return $iret
}

# dit si le type est dans etude, tests, sources ou surcharge... ou 0
#################################################################
proc is_type { typ } {
   set res 0
   set ind -1
   set ll { etude tests sources surcharge }
   set nl [llength $ll]
   for {set k 0} {$k < $nl} {incr k} {
      set var [lindex $ll $k]
      if { $res != 0 } {
         break
      } else {
         for {set i 0} {$i < $astk::UL_ref($var,nbre)} {incr i} {
            if { $typ == $astk::UL_ref($var,$i,nom) } {
               set res $var
               set ind $i
               break
            }
         }
      }
   }
   set lret [list $res $ind]
   return $lret
}

# retourne 0 si les noms de fichiers du profil sont corrects
# NB : on ne vérifie pas l'existence
#################################################################
proc verif_nomfich { {diag "oui"} } {
   set iret 0
   # messages d'info et d'erreur par défaut
   set imsg(2) 16
   set iarg(2) ""
   set imsg(4) 40
   set iarg(4) ""
   foreach var { etude tests sources surcharge } {
      set var2 $var
      if { $var == "sources" } {
         # on ne "coche" pas sources
         set var2 surcharge
      }
      if { $astk::profil($var2) == "oui" } {
      # vérif du chemin de base
         set chbas $astk::profil(path_$var)
         if { $chbas != "" && [string index $chbas 0] != "/" } {
            set iret 4
            set imsg($iret) 41
            set iarg($iret) $chbas
            break
         }
      # vérif des fichiers
         for {set i 0} { $i < $astk::profil($var,nbfic) } {incr i} {
            set valf $astk::profil($var,fich,$i,nom)
            # retire les espaces au début et à la fin
            regsub {^\ +} $valf "" valf
            regsub {\ +$} $valf "" valf
            set astk::profil($var,fich,$i,nom) $valf
         # ligne vide
            if { $valf == "" } {
               set iret 4
               set imsg($iret) 42
               set iarg($iret) [list [quel_onglet $var2] $astk::profil($var,fich,$i,type)]
               break
            }
            # ajoute le chemin de base
            if { [string index $valf 0] != "/" } {
               set valf [file join $astk::profil(path_$var) $valf]
            }
         # fichier contenant des espaces
            if { [regexp {[ \|\?\*\\:@]+} $valf] } {
               set iret 4
               set imsg($iret) 38
               set iarg($iret) [list [quel_onglet $var2] $valf]
               break
            }
         # alarme nom en relatif
         # exception pour forlib
            if { [string index $valf 0] != "/"
             && $astk::profil($var,fich,$i,type) != "forlib" } {
               set msg [ashare::mess info 24 $valf]
               if { 2 > $iret } {
                  set iret 2
                  set imsg($iret) 24
                  set iarg($iret) $valf
               }
            }
         }
      }
   }
   if { $diag == "oui" } {
   # message d'info ou d'erreur
      if { $iret > 0 } {
         if { $iret == 2 } {
            set msg [eval "ashare::mess info $imsg($iret) $iarg($iret)"]
         } elseif { $iret == 4 } {
            set msg [eval "ashare::mess erreur $imsg($iret) $iarg($iret)"]
         } else {
            set msg [ashare::mess erreur 33]
         }
         change_status $msg
         tk_messageBox -title [ashare::mess ihm 138] -message $msg -type ok -icon info
      }
   }
   return $iret
}
