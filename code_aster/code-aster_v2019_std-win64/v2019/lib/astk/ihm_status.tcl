#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_status.tcl 568 2003-12-19 14:36:31Z mcourtoi $

# affiche la ligne d'état
#################################################################
proc affiche_status { } {
   pack [frame $astk::ihm(status).st -relief raised -bd 0] -fill both -expand 1
   pack [frame $astk::ihm(status).st.img -relief flat -bd 0] -side left
   pack [frame $astk::ihm(status).st.txt -relief flat -bd 0] -side left -fill x -expand 1
   pack [frame $astk::ihm(status).st.cancel -relief flat -bd 0] -side left
#
   change_status [ashare::mess ihm 12]
#
   image create photo nimg -file $astk::icon(help_sans)
   button $astk::ihm(status).st.img.w -image nimg -bd 0 -relief flat
   pack $astk::ihm(status).st.img.w -side left -padx 1
# texte du message
   label $astk::ihm(status).st.txt.etat -font $astk::ihm(font,val) -textvariable astk::ihm(val_status) -anchor w -relief sunken
   pack $astk::ihm(status).st.txt.etat -fill x -anchor w -expand 1
}

# modifie le message affiché dans la barre d'état
#################################################################
proc change_status { msg } {
   switch -exact -- $ashare::appli {
      astk {
         # on enlève le type
         if { [regexp {^ *<([A-Z]+)[0-9 ]*>} $msg mat1 typ] } {
            regsub {^ *<([A-Z]+)[0-9 ]*> +} $msg "" msg
         }
         set astk::ihm(val_status) $msg
         # message "vide"
         if { $msg == "" || $msg == [ashare::mess ihm 12] } {
            catch {destroy $astk::ihm(status).st.img.w}
         } else {
            catch {destroy $astk::ihm(status).st.img.w}
            image create photo nimg -file $astk::icon(help_sans)
            button $astk::ihm(status).st.img.w -image nimg -bd 0 -relief flat
            pack $astk::ihm(status).st.img.w -side left
         }
      }
      asjob {
         set asjob::status $msg
      }
      bsf {
         set tkgetdir::status $msg
      }
   }
}
