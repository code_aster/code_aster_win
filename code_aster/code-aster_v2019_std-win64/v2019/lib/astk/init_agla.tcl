#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: init_agla.tcl 1030 2005-10-28 11:07:49Z mcourtoi $

# initialisation des variables de l'agla
#################################################################
proc init_agla { } {
   set astk::agla(status) off
   set astk::agla(instance) "UA"
   set astk::agla(mail_ata) ""
   set astk::agla(infoid) {}
   set astk::agla(num_serv) -1
   set astk::agla(nb_verdev) 0
   set astk::agla(verdev,0) ""
   set astk::agla(rex_url) ""
   set astk::agla(rep_rex) ""
}

# mise à jour des données de l'agla
#################################################################
proc raffr_agla { {parent "."} {REINI "DETR"} } {
   ashare::mess "info" 5
   init_agla

#  vérifie que mach_dev est parmi les serveurs configurés
   set pastrouve 1
   for {set i 0} {$i < $astk::config(nb_serv)} {incr i} {
      if { $astk::config($i,mach_ref) == "oui" } {
         set astk::agla(num_serv) $i
         set pastrouve 0
         break
      }
   }
# machine de référence pas dans les serveurs
#ou on ne veut pas récupérer les infos
   if { $pastrouve } {
      ashare::mess "info" 7
   } elseif { $astk::recup == 0 } {
      ashare::mess "info" 40
   } else {
# machine de référence trouvée
      set serv $astk::agla(num_serv)
   # instance de l'utilisateur
      set astk::agla(status) off
      recup_info $parent $serv
      # issue17523: always enable agla functions
      # may 2013: never!
      if { 0 && [regexp "EDA" $astk::agla(instance)] } {
         set astk::agla(status) on
      }

   # faire la liste des versions (les NEW de $serv)
      set astk::agla(nb_verdev) 0
      for {set i 0} {$i < $astk::config($serv,nb_vers)} {incr i} {
         if { [regexp {^NEW} $astk::config($serv,vers,$i)] || \
              $astk::config($serv,vers,$i) == "unstable" || \
              $astk::config($serv,vers,$i) == "stable-updates" } {
            set astk::agla(verdev,$astk::agla(nb_verdev)) $astk::config($serv,vers,$i)
            incr astk::agla(nb_verdev)
         }
      }
      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (raffr_agla) status:$astk::agla(status) num_serv:$astk::agla(num_serv) instance:$astk::agla(instance)"
      }
   }
# mise à jour des onglets
   if { $REINI != "INI" } {
      init_onglet "DETR"
   }
}
