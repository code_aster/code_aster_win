#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: lire_mcles.tcl 308 2003-04-22 15:22:07Z mcourtoi $

# Lecture d'un fichier et renvoie la liste des mots-clés
# et des valeurs associées
# format  "mot_cle : valeur"
# tout ce qui ne commence pas par une lettre ou un chiffre = commentaire
#################################################################
proc ashare::lire_mc_val { fich lmots lvale nblu } {
   upvar $lmots mots
   upvar $lvale vale
   upvar $nblu nlu

   # lecture du fichier
   #    mots-clés : valeur
   if { [ file exists $fich ] == 0 } {
        ashare::mess "info" 4 $fich
      return FICH_NON_TROUVE
   }
   set idconf [open $fich r]

   # lecture du fichier
   set nlu 0
   while  { [eof $idconf] != 1 } {
      gets $idconf line
      if {[regexp -nocase {^[ ]*([-a-z_0-9\.]*)[ ]*:} $line gch1 mot] == 1} {
         if {[regexp -nocase {:[ ]+(.*[^ $])} $line gch2 val] == 1} {
            incr nlu
            set mots($nlu) $mot
            if { $val != "_VIDE" } {
               set vale($nlu) [expandvars $val]
            } else {
               set vale($nlu) ""
            }
            if { $ashare::dbg >= 4 } {
               ashare::log "<DEBUG> (lire_mc_val) $mot = $val"
            }
         }
      }
   }
   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lire_mc_val) nbre valeurs lues : $nlu"
   }
   close $idconf
   return 0
}
