#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: ihm_main.tcl 2881 2007-03-20 12:30:18Z courtois $

# construction de la fenetre principale de l'interface
#################################################################
proc astk_princ { { REINI "INI" } } {
# message de mise à jour
   set client_vers "00.00.00"
   if { [regexp {([0-9]+)\.([0-9]+)\.([0-9]+)} $astk::astk_version mat1 i1 i2 i3] } {
      set client_vers [format "%02d.%02d.%02d" $i1 $i2 $i3]
   }
   if { $astk::config(-1,flag_maj) != $client_vers } {
      if { $ashare::origine != "from_salome" } {
         tk_messageBox -title [ashare::mess ihm 138] -message [ashare::mess ihm 242] -type ok -icon info
      }
      set astk::config(-1,flag_maj) $client_vers
      ashare::save_prefs
   }

# fenetre principale
   ashare::pointeur off

    # Titre de la fenetre + menu + creation de la frame complete + icon
    set_titre
    set_icon
    # on fixe la taille de la fenetre
#if_not_resizable   wm resizable . 0 0
   wm protocol . WM_DELETE_WINDOW { quitter }

    pack [frame $astk::ihm(menu) -relief raised -bd 0 -background $astk::ihm(couleur,menu_background)] -fill x -side top -anchor nw
    pack [frame .fen -relief raised -bd 0] -fill both -expand 1 -anchor nw
       grid [frame $astk::ihm(fenetre) -relief raised -bd 1] -row 0 -column 0 -sticky nsew
       grid [frame $astk::ihm(satellite) -relief raised -bd 1] -row 0 -column 1 -sticky nsw
       grid [frame $astk::ihm(status) -relief raised -bd 0] -row 1 -columnspan 2 -sticky nsew
   grid columnconfigure .fen 0 -weight 1
   grid rowconfigure .fen 0 -weight 1

#      pack [frame $astk::ihm(fenetre) -relief raised -bd 1] -side left -fill both -expand 1 -anchor nw
#      pack [frame $astk::ihm(satellite) -relief raised -bd 1] -fill y -expand 1 -anchor nw
#   pack [frame $astk::ihm(status) -relief raised -bd 0] -fill x -anchor s -expand 1
#    pack [panedwindow .fen -orient horizontal] -expand 1 -fill both -anchor nw
#      .fen add [frame $astk::ihm(fenetre) -relief raised -bd 0]
#       .fen add [frame $astk::ihm(satellite) -relief raised -bd 0]

    # barre de menu
   create_popup $astk::ihm(popup)
    create_menu $astk::ihm(menu)

    # frame "satellite"
    affiche_satellite

   # etat, aide...
   affiche_status

    # il est nécessaire d'afficher l'onglet en dernier car c'est lui qui bride
    # la taille de la fenetre
    pack [frame $astk::ihm(fenetre).onglet] -anchor w
    # creation et initialisation des onglets
    init_onglet

    # lecture du profil initial ou export
    if { $astk::ihm(profil_ini) != "" } {
        ouvrir $astk::ihm(serv_ini) $astk::ihm(profil_ini)
      set astk::ihm(profil_ini) ""
    } elseif { $astk::ihm(export_ini) != "" } {
        import_from "astk_serv" -1 $astk::ihm(export_ini) "non" "oui"
      set astk::ihm(export_ini) ""
    } else {
       # pour le demarrage
       affiche_onglet $astk::profil(onglet_actif)
    }

   # cree la liste des noeuds, des versions, choix batch/interactif...
   maj_sat

   # fenetre init
   catch {destroy .fen_about}
   catch {destroy $astk::ihm(asjob)}
   show_fen $astk::ihm(asjob)
   
   # affichage
   ashare::pointeur on
   update idletasks
   wm deiconify .
   ashare::trace_geom astk .
}

# raffraichir la fenetre principale de l'interface
#################################################################
proc raffr_princ { } {
   global ongsel
# sous-onglets sélectionnés
   for {set i 1} {$i <= $astk::ihm(nbongM)} {incr i} {
      for {set j 1} {$j <= $astk::ihm(nbong,$i)} {incr j} {
         if { $ongsel($i) == $astk::ihm(tit,$i,$j) } {
            set ongsel($i) $j
         }
      }
   }
# réinitialisation du nom des onglets
   def_onglet
   for {set i 1} {$i <= $astk::ihm(nbongM)} {incr i} {
      set ongsel($i) $astk::ihm(tit,$i,$ongsel($i))
   }
# détruit les fenêtres filles et ouvre de nouveau asjob s'il existait
   set iasj [winfo exists $astk::ihm(asjob)]
# ne pas détruire les classes BWidgets
#   eval destroy [winfo children .]
   set lw [winfo children .]
   for {set i 0} {$i < [llength $lw]} {incr i} {
      set ww [lindex $lw $i]
      if { [string match ".#BWidget*" $ww] == 0 } {
         destroy $ww
      }
   }
# réaffichage
   astk_princ DETR
   if { $iasj } {
      show_fen $astk::ihm(asjob) force
   }
   raise .
}

# change le thème de couleur (voir init_couleur pour les thèmes)
#################################################################
proc change_couleur { theme } {
   set astk::ihm(style,couleur) $theme
   set astk::config(-1,couleur) $theme
   set astk::config(-1,theme) $theme
   ashare::save_prefs
   init_couleur
   init_icon $theme
   raffr_princ
}

# permet de changer les couleurs à la main
#################################################################
proc perso_couleur { } {
   global tmp_coul
# récupère les couleurs actuelles
   set old_coul $astk::ihm(style,couleur)
   set astk::ihm(style,couleur) perso
   init_couleur "ras"
   set lmv [array get astk::ihm]
   set nbl [expr [llength $lmv] / 2]
   set lmcle [list]
   set lcoul [list]
   set nn 0
   for {set i 0} {$i < $nbl} {incr i} {
      set k  [expr $i * 2]
      set k1 [expr $k + 1]
      if { [regexp {^couleur,(.*)} [lindex $lmv $k] mat1 mcle] } {
         incr nn
         lappend lmcle $mcle
         lappend lcoul [lindex $lmv $k1]
      }
   }
   set lmcle [lsort $lmcle]
# fenetre
   set fen .fen_coul
   catch {destroy $fen}
   toplevel $fen
   wm withdraw $fen
   wm transient $fen .
   wm title $fen [ashare::mess ihm 328]

   pack [frame $fen.liste -relief solid -bd 1]

# boucle sur les couleurs
   for {set i 0} {$i < $nn} {incr i} {
      set mcle [lindex $lmcle $i]
      set tmp_coul($mcle) $astk::ihm(couleur,$mcle)
      if { $mcle != "foreground" } {
         set fgc $astk::ihm(couleur,foreground)
      } else {
         set fgc $astk::ihm(couleur,entry_background)
      }
      pack [frame $fen.liste.l_$i -relief solid -bd 0] -anchor w
      label $fen.liste.l_$i.lbl -font $astk::ihm(font,lab) -text $mcle -width 35 -anchor w
      button $fen.liste.l_$i.butt -width 30 -font $astk::ihm(font,val) -text [ashare::mess ihm 329] \
         -fg $fgc \
         -bg $astk::ihm(couleur,$mcle) \
         -command "choix_couleur $mcle $fen $fen.liste.l_$i.butt"
      pack $fen.liste.l_$i.lbl $fen.liste.l_$i.butt -pady 3 -side left
   }

# ok
   pack [frame $fen.valid -relief solid -bd 0]
   button $fen.valid.annuler -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
      -bg $astk::ihm(couleur,annul) \
      -command "set astk::ihm(style,couleur) $old_coul ; init_couleur ras ; destroy $fen"
   button $fen.valid.ok -font $astk::ihm(font,labbout) -text "Ok" \
      -bg $astk::ihm(couleur,valid) \
      -command "set astk::ihm(style,couleur) $old_coul ; init_couleur ras ; accept_couleur ; destroy $fen"
   pack $fen.valid.ok $fen.valid.annuler -side left -padx 10 -pady 5
   
   wm deiconify $fen
}

# choisit une couleur
#################################################################
proc choix_couleur { mcle fen w } {
   global tmp_coul
   set tmp_coul($mcle) [tk_chooseColor -title "Choose a color" -parent $fen \
       -initialcolor $astk::ihm(couleur,$mcle)]
   if { $tmp_coul($mcle) != "" } {
      $w configure -bg $tmp_coul($mcle)
   }
}

# accepte les couleurs
#################################################################
proc accept_couleur { } {
   global tmp_coul
   set id [open $astk::fic_color w]
   puts $id "# AUTOMATICALLY GENERATED - DO NOT EDIT !"
   puts $id "astkrc_version : $ashare::astkrc_version"
   puts $id "#"
   set lmv [array get tmp_coul]
   set nbl [expr [llength $lmv] / 2]
   for {set i 0} {$i < $nbl} {incr i} {
      set k  [expr $i * 2]
      set k1 [expr $k + 1]
      set mcle [lindex $lmv $k]
      set astk::ihm(couleur,$mcle) $tmp_coul($mcle)
      puts $id "$mcle : $tmp_coul($mcle)"
   }
   close $id
   if { $astk::ihm(style,couleur) == "perso" } {
      change_couleur perso
   }
}

# choix des polices
#################################################################
proc change_font { } {
# fenetre
   set fen .fen_font
   catch {destroy $fen}
   toplevel $fen
   wm withdraw $fen
   wm transient $fen .
   wm title $fen [ashare::mess ihm 331]

   pack [frame $fen.liste -relief solid -bd 1] -padx 10 -pady 10

   set lf $astk::ihm(style,fontlist)
   set lm $astk::ihm(style,fontlist_label)
   for {set i 0} {$i < [llength $lf]} {incr i} {
      set ff [lindex $lf $i]
      set msg [ashare::mess ihm [lindex $lm $i]]
      button $fen.liste.$ff -font $astk::ihm(style,font_$ff) -text $msg \
         -command "choix_font $fen $ff"
      grid $fen.liste.$ff -row $i -column 0 -sticky ew -padx 5 -pady 5
   }

# ok
   pack [frame $fen.valid -relief solid -bd 0]
   button $fen.valid.ok -font $astk::ihm(font,labbout) -text "Ok" \
      -bg $astk::ihm(couleur,valid) \
      -command "destroy $fen"
   pack $fen.valid.ok -side left -padx 10 -pady 5

   ashare::centre_fen $fen .
   wm deiconify $fen
}

# sélection d'une police
# groupe = main, fixe
#################################################################
proc choix_font { parent groupe } {
   set newfont [SelectFont .fontdlg -parent $parent -font $astk::ihm(style,font_$groupe)]
   if { $newfont != "" } {
      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> Font : $newfont"
      }
      set astk::ihm(style,font_$groupe) $newfont
      init_font
      ashare::save_prefs
      destroy $parent
      raffr_princ
   }
}
