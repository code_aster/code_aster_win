#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: lance_calcul.tcl 3649 2008-11-28 16:01:22Z courtois $

# GO
#################################################################
proc go_calcul { } {
   set nb [quoi_lancer recap]
   if { $nb > 0 } {
      ashare::log "====================================\n$recap\n===================================="

   # exception pour asdeno/asquit, on n'enregistre pas le profil bidon
      if { $astk::profil(agla) != "oui" ||
           ($astk::profil(asdeno) != "oui" && $astk::profil(asquit) != "oui") } {
      # enregistrement du profil
         set iret [enregistrer $astk::profil(serv_profil) $astk::profil(nom_profil)]
         if { $iret != 0 } {
            set msg [ashare::mess erreur 13]
            tk_messageBox -message $msg -type ok -icon info
            return 5
         }
      }

      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (go_calcul) nom serveur = $astk::profil(serveur)"
         ashare::log "<DEBUG> (go_calcul) nom noeud   = $astk::profil(noeud)"
      }

   # soumet le calcul
      show_fen $astk::ihm(asjob)
      set iret [lance_calcul $astk::profil(batch)]

      if { $iret == 0 } {
         if { $astk::profil(batch) } {
            set msg [ashare::mess ihm 53 [ashare::mess ihm 51]]
         } else {
            set msg [ashare::mess ihm 53 [ashare::mess ihm 52]]
         }
      } elseif { $iret == 2 } {
      # alarmes emises
         set icod 16
      } elseif { $iret == 4 } {
      # export failed
         set icod 40
      } elseif { $iret == 6 } {
      # ajout_job failed
         set icod 134

# code as_exec (+100)
      } elseif { $iret == 102 || $iret == 106 } {
      # pb rcp / rsh
         set icod 96
      } elseif { $iret == 103 } {
      # pb conversion export - run_aster
         set icod 104
      } elseif { $iret == 107 } {
      # pb xterm
         set icod 173

      } elseif { $iret == -888 } {
# exit silencieux
      # code pour exit silencieux car pas de soumission
         set icod 246
      } elseif { $iret == -999 } {
      # code pour exit silencieux
         set iret 0
         #set icod 12

      } else {
# autre probleme de lancement de la commande (as_exec)
         set icod 60
      }
      if { $iret != 0 } {
         set msg [ashare::mess ihm $icod $iret]
         change_status $msg
         tk_messageBox -message $msg -type ok -icon info
      }
   } else {
      tk_messageBox -message [ashare::mess ihm 61] -type ok -icon info
      change_status [ashare::mess ihm 12]
   }
}

# message récapitulatif des actions à lancer
#################################################################
proc quoi_lancer { var } {
   upvar $var chaine
   set nb 0
   set chaine "[ashare::mess ihm 63] :\n"
# fonctions agla
   if { $astk::profil(agla) == "oui" } {
      for {set j 2} {$j <= $astk::ihm(nbong,$astk::ihm(nongl_agla)) } {incr j} {
         if { $astk::profil($astk::ihm(ong,$astk::ihm(nongl_agla),$j)) == "oui" } {
            append chaine "  - [ashare::mess ihm 150 $astk::ihm(tit,$astk::ihm(nongl_agla),$j)]\n"
            incr nb
         }
      }
   } else {
# autres fonctions
      if { $astk::profil(etude) == "oui" } {
         append chaine "  - [ashare::mess ihm 64]\n"
         incr nb
      }
      if { $astk::profil(tests) == "oui" } {
         append chaine "  - [ashare::mess ihm 65]\n"
         incr nb
      }
      if { $astk::profil(surcharge) == "oui" } {
         append chaine "  - "
         if { $astk::profil(etude) == "oui" || $astk::profil(tests) == "oui" } {
            append chaine [ashare::mess ihm 66]
         } else {
            append chaine [ashare::mess ihm 67]
         }
         append chaine "\n"
         incr nb
      }
   }
   return $nb
}

# lancement d'un calcul via le service adéquat
#################################################################
proc lance_calcul { batch } {
   # batch = 1 : batch
   # batch = 0 : interactif
   global srv_list_on
   set serv $astk::inv(serv,$astk::profil(serveur))
   # avertir si des options supplémentaires sont activées
   set isuppl 0
   set msg ""
   foreach opt {classe depart after_job exectool} {
      if { $astk::profil(opt_val,$opt) != "" } {
         set isuppl 1
         append msg "$opt=$astk::profil(opt_val,$opt)\n"
      }
   }
   set opt "distrib"
   if { [in_no_values $astk::profil(opt_val,$opt)] == 0 } {
      set isuppl 1
      append msg "$opt=$astk::profil(opt_val,$opt)\n"
   }
   set opt "multiple"
   if { [in_no_values $astk::profil(opt_val,$opt)] == 0 } {
      set isuppl [expr $isuppl + 2]
   }
   set ich "yes"
   if { $isuppl == 1 || $isuppl == 3 } {
      set ich [tk_messageBox -message [ashare::mess ihm 340 $msg] -type yesno -icon question ]
      if { $ich == "no" } {
         return -999
      }
   }
   if { $isuppl >= 2 } {
      select_server_list
      tkwait window .fen_srv_list
      set ich $srv_list_on
      if { $ich == "no" } {
         return -999
      }
   }

   # verif syntaxe de temps, memoire, nbproc
   if { [ conv_tps $astk::profil(temps) ] == 0 } {
      return [ashare::mess ihm 56]
   }
   if { [regexp {^[ ]*([0-9]+)[ ]*} $astk::profil(memoire)] == 0 } {
      return [ashare::mess ihm 58]
   }
   if { $astk::profil(opt_val,ncpus) != ""
     && [regexp {^[ ]*([0-9]+)[ ]*} $astk::profil(opt_val,ncpus)] == 0 } {
      return [ashare::mess ihm 54 ncpus]
   }
   if { [regexp {^[ ]*([0-9]+)[ ]*} $astk::profil(opt_val,mpi_nbcpu)] == 0 } {
      return [ashare::mess ihm 54 mpi_nbcpu]
   }
   if { [regexp {^[ ]*([0-9]+)[ ]*} $astk::profil(opt_val,mpi_nbnoeud)] == 0 } {
      return [ashare::mess ihm 54 mpi_nbnoeud]
   }
   # verif des limites
   if { $batch != "" && $batch } {
      set ch "batch"
   } else {
      set ch "interactif"
   }
   set nb_err 0
   # verif temps
   set val $ch
   append val "_tpsmax"
   set val $astk::config($serv,$val)
   if { [ conv_tps $astk::profil(temps)] > $val } {
      set ich "no"
      if { $astk::profil(batch) == 1 } {
         set ich [tk_messageBox -message "[ashare::mess ihm 57 $val]\n[ashare::mess ihm 337]" -type yesno -icon question ]
      }
      if { $ich == "no" } {
         return [ashare::mess ihm 57 $val]
      }
   }
   # verif memoire
   set val $ch
   append val "_memmax"
   set val $astk::config($serv,$val)
   if { $astk::profil(memoire) > $val } {
      set ich "no"
      if { $astk::profil(batch) == 1 } {
         set ich [ tk_messageBox -message "[ashare::mess ihm 59 $val]\n[ashare::mess ihm 337]" -type yesno -icon question ]
      }
      if { $ich == "no" } {
         return [ashare::mess ihm 59 $val]
      }
   }
   # vérif ncpus
   set key $ch
   append key "_nbpmax"
   set val $astk::config($serv,$key)
   if { $val != "" && $astk::profil(opt_val,ncpus) > $val } {
      return "(OpenMP) [ashare::mess ihm 55 $val]"
   }
   # vérif mpi_nbcpu
   set key $ch
   append key "_mpi_nbpmax"
   set val $astk::config($serv,$key)
   if { $val != "" && $astk::profil(opt_val,mpi_nbcpu) > $val } {
      return "(MPI) [ashare::mess ihm 55 $val]"
   }

   # nom du fichier export
   set astk::profil(serv_fich_export) $astk::profil(serv_profil)
   set astk::profil(nom_fich_export) [file rootname $astk::profil(nom_profil)]
   append astk::profil(nom_fich_export) ".export"
   # exception pour asdeno/asquit, on n'enregistre pas le profil bidon
   if { $astk::profil(agla) == "oui" &&
        ($astk::profil(asdeno) == "oui" || $astk::profil(asquit) == "oui") } {
      set astk::profil(serv_fich_export) -1
      set astk::profil(nom_fich_export) [get_tmpname tmp_agla]
      append astk::profil(nom_fich_export) ".export"
   }

   # lancement de la commande
   set type astk_serv
   set iret [lance_$type $batch]

   if { $ashare::dbg >= 4 } {
      ashare::log "<DEBUG> (lance_calcul) iret : $iret"
   }
   return $iret
}
