#########################################################################
# COPYRIGHT (C) 2003         EDF R&D              WWW.CODE-ASTER.ORG    #
#                                                                       #
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR         #
# MODIFY IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS        #
# PUBLISHED BY THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE    #
# LICENSE, OR (AT YOUR OPTION) ANY LATER VERSION.                       #
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,       #
# BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF        #
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      #
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              #
#                                                                       #
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     #
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO : EDF R&D CODE_ASTER,       #
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        #
#########################################################################

# $Id: print.tcl 2302 2006-10-26 11:29:20Z courtois $

# reaction a Impression
#################################################################
proc print_conf { } {
   global impr_sel
   global old_impr old_inv
   set fen .fen_impr
   catch {destroy $fen}
   toplevel $fen
   wm withdraw $fen
   wm title $fen [ashare::mess ihm 247]
   wm transient $fen .
   grab set $fen
   
# sauvegarde le tableau outils avant modif
   copie_tabl astk::print old_impr
   if { $astk::print(nb) > 0 } {
      set impr_sel $astk::print(0,label)
   }
   print_conf_list $fen

   wm deiconify $fen
}

# recréer la liste des imprimantes
#################################################################
proc print_conf_list { fen } {
   global impr_sel last_impr_sel

   catch {destroy $fen.haut}
   catch {destroy $fen.liste}
   catch {destroy $fen.bas}

   # le 1er coup ET quand on veut recréer la liste, il faut raffraichir
   set last_impr_sel -1

   pack [frame $fen.haut -relief flat -bd 0] -fill x -expand yes
   pack [frame $fen.liste -relief solid -bd 1] -fill x
   pack [frame $fen.bas -relief solid -bd 0] -fill both -expand yes

   pack [frame $fen.haut.choix_impr -relief solid -bd 0] -side left -fill x -expand yes
   if { $astk::print(nb) > 0 } {
      set MenuImpr [tk_optionMenu $fen.haut.choix_impr.pr impr_sel $astk::print(0,label)]
      $MenuImpr entryconfigure 0 -font $astk::ihm(font,labmenu) -command "print_conf_para $fen.liste \"$astk::print(0,label)\""
      for {set j 1} {$j < $astk::print(nb)} {incr j} {
         $MenuImpr add radiobutton
         $MenuImpr entryconfigure $j -label $astk::print($j,label) -font $astk::ihm(font,labmenu) -variable impr_sel -command "print_conf_para $fen.liste \"$astk::print($j,label)\""
      }
      $MenuImpr add radiobutton
      $MenuImpr entryconfigure $j -label [ashare::mess ihm 255] -font $astk::ihm(font,labmenu) -variable impr_sel -command "add_print $fen $MenuImpr"
   #
      $fen.haut.choix_impr.pr configure -font $astk::ihm(font,labmenu) -bg $astk::ihm(couleur,liste)
      pack $fen.haut.choix_impr.pr
      print_conf_para $fen.liste "$impr_sel"
   } else {
      set MenuImpr [tk_optionMenu $fen.haut.choix_impr.pr impr_sel [ashare::mess ihm 255]]
      $MenuImpr entryconfigure 0 -font $astk::ihm(font,labmenu) -command "add_print $fen $MenuImpr"
      pack $fen.haut.choix_impr.pr
   }
   $MenuImpr configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   button $fen.haut.help -image [image create photo -file $astk::icon(help)] \
         -bd 0 -command "grab release $fen ; aff_aide . 190"
   pack $fen.haut.help -anchor ne -side right

   pack [frame $fen.bas.valid -relief solid -bd 0] -side left -fill both -expand yes
      pack [frame $fen.bas.valid.m -relief solid -bd 0]
      button $fen.bas.valid.m.annuler -font $astk::ihm(font,labbout) -text [ashare::mess ihm 85] \
         -background $astk::ihm(couleur,annul) \
         -command "annule_mod_print $fen"
      button $fen.bas.valid.m.ok -font $astk::ihm(font,labbout) -text "Ok" \
         -background $astk::ihm(couleur,valid) \
         -command "save_print ; destroy $fen ; grab release $fen"
      pack $fen.bas.valid.m.ok $fen.bas.valid.m.annuler -side left -padx 10 -pady 5
   
   pack [frame $fen.bas.del -relief solid -bd 0] -side right
   button $fen.bas.del.bout -font $astk::ihm(font,labbout) -text [ashare::mess ihm 256] \
      -background $astk::ihm(couleur,suppr) \
      -command "del_print $fen"
   pack $fen.bas.del.bout -side left -padx 10 -pady 5
}

#################################################################
proc print_conf_para {fen impr} {
   global last_impr_sel
   set nimpr -1
   for {set i 0} {$i < $astk::print(nb)} {incr i} {
      if { $astk::print($i,label) == $impr } {
         set nimpr $i
         break
      }
   }
# si pas de chgt, on sort
   set mod 0
   catch { if { $impr == $last_impr_sel } { set mod 1 } }
   if { $mod } { return }
   set last_impr_sel $impr

   catch {destroy $fen.f}
   pack [frame $fen.f -relief raised -bd 0]

# serv
   pack [frame $fen.f.serv -relief solid -bd 0] -anchor w
   label $fen.f.serv.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 15] -width 25 -anchor w
   #entry $fen.f.serv.path -width 30 -font $astk::ihm(font,val) -textvariable astk::print($nimpr,serv)
   set m2 [tk_optionMenu $fen.f.serv.path astk::print($nimpr,serv) $astk::config(-1,nom)]
   $m2 configure \
        -foreground $astk::ihm(couleur,menu_foreground) \
        -background $astk::ihm(couleur,menu_background)
   $m2 entryconfigure 0 -font $astk::ihm(font,lablst)
   for {set j 0} {$j < $astk::config(nb_serv)} {incr j} {
      set j1 [expr $j + 1]
      $m2 add radiobutton
      $m2 entryconfigure $j1 -label $astk::config($j,nom) -font $astk::ihm(font,lablst) -variable astk::print($nimpr,serv)
   }
   pack $fen.f.serv.lbl $fen.f.serv.path -pady 3 -side left

# file
   pack [frame $fen.f.file -relief solid -bd 0] -anchor w
   label $fen.f.file.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 257] -width 25 -anchor w
   entry $fen.f.file.path -width 30 -font $astk::ihm(font,val) -textvariable astk::print($nimpr,file)
   pack $fen.f.file.lbl $fen.f.file.path -pady 3 -side left

# cmde
   pack [frame $fen.f.dist -relief solid -bd 0] -anchor w
   label $fen.f.dist.lbl -font $astk::ihm(font,lab) -text [ashare::mess ihm 156] -width 25 -anchor w
    entry $fen.f.dist.cmde -width 80 -font $astk::ihm(font,val) -textvariable astk::print($nimpr,cmde)
   pack $fen.f.dist.lbl $fen.f.dist.cmde -pady 3 -side left
}

# annule les modifs des imprimantes
#################################################################
proc annule_mod_print { fen } {
   global old_impr
   set mod 0
   # vérifie que l'on n'a pas ajouté d'imprimante
   if { $old_impr(nb) != $astk::print(nb) } {
      set mod 1
   } else {
      set lmv [array get old_impr]
      set nbl [expr [llength $lmv] / 2]
      for {set i 0} {$i < $nbl} {incr i} {
         set k  [expr $i * 2]
         set k1 [expr $k + 1]
         if { $astk::print([lindex $lmv $k]) != [lindex $lmv $k1] } {
            set mod 1
            if { $ashare::dbg >= 5 } {
               ashare::log "<DEBUG> (annule_mod_print) [lindex $lmv $k] old =[lindex $lmv $k1] ; new=$astk::print([lindex $lmv $k])"
            } else {
               break
            }
         }
      }
   }
   # est-ce qu'il y a eu des modifs
   if { $mod } {
   # demande confirmation
      set iret [ tk_messageBox -message [ashare::mess ihm 88] \
         -title [ashare::mess ihm 143] -type yesno -icon question ]
      if { $ashare::dbg >= 4 } {
         ashare::log "<DEBUG> (annule_mod_print) choix : $iret"
      }
      if { $iret == "yes" } {
         set mod 0
      }
   }
   # on sort
   if { $mod == 0 } {
      copie_tabl old_impr astk::print
      destroy $fen
      grab release $fen
   } else {
      raise $fen
   }
}

# ajoute une imprimante
# fen : fenetre parent
#################################################################
proc add_print { fen men } {
   global impr_sel last_impr_sel
   global getValue_val
   # retire le focus au parent
   grab release $fen

   set new [expr $astk::print(nb)]
   set valint [list]
   for {set i 0} {$i<$new} {incr i} {
      lappend valint $astk::print($i,label)
   }
   getValue_fen $fen 255 258 $valint "" non_vide
   tkwait window .fen_getv
   grab set $fen
# valide le nom de l'imprimante
   set valid 1
   if { $getValue_val == "_VIDE" } {
      set valid 0
   }
   if { $valid } {
      set astk::print($new,label) $getValue_val
      incr astk::print(nb)
      # initialisation
      # valeurs obligatoires
      set nbm [llength $ashare::mots(SSF_pr)]
      for {set i 0} {$i<$nbm} {incr i} {
         set mcle [lindex $ashare::mots(SSF_pr) $i]
         set val ""
         # quelques valeurs particulières
         if { $mcle == "serv" } {
            set val $astk::local_server
         }
         set astk::print($new,$mcle) $val
      }
      # nom dans le menu
      set impr_sel $astk::print($new,label)
      print_conf_list $fen
      raise $fen
   } else {
      set impr_sel $last_impr_sel
      print_conf_para $fen "$impr_sel"
      raise $fen
   }
}

# supprime une imprimante
#################################################################
proc del_print { fen } {
   global impr_sel
   set nimpr -1
   for {set i 0} {$i < $astk::print(nb)} {incr i} {
      if { $astk::print($i,label) == $impr_sel } {
         set nimpr $i
         break
      }
   }
# demande confirmation
    set iret [ tk_messageBox -message [ashare::mess ihm 259] \
       -title [ashare::mess ihm 260] -type yesno -icon question ]
    if { $ashare::dbg >= 4 } {
        ashare::log "<DEBUG> (del_print) retour : $iret"
   }
    if { $iret != "yes" } {
      raise $fen
        return 1
   }
# liste des mots-clés
   set mots_cles $ashare::mots(MCF_pr)
   append mots_cles $ashare::mots(SSF_pr)
   set nbm [llength $mots_cles]
# suppression
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (del_print) Suppr $astk::print($nimpr,label) (numero $nimpr)" }
# décalage
   incr astk::print(nb) -1
   for { set j $nimpr } { $j < $astk::print(nb) } {incr j} {
      set j1 [expr $j + 1]
      if { $ashare::dbg >= 5 } {
         ashare::log "<DEBUG> (del_print) Copie $astk::print($j1,label) de $j1 en $j"
      }
      for { set k 0 } { $k < $nbm } { incr k } {
         set mcs [lindex $mots_cles $k]
         set astk::print($j,$mcs) $astk::print($j1,$mcs)
      }
   }
   # suppression du dernier outil
   if { $ashare::dbg >= 5 } {
      ashare::log "<DEBUG> (del_print) Suppr outil $j"
   }
   for { set k 0 } { $k < $nbm } { incr k } {
      set mcs [lindex $mots_cles $k]
      unset astk::print($j,$mcs)
   }
   set impr_sel $astk::print(0,label)
   print_conf_list $fen
   raise $fen
   return 0
}
